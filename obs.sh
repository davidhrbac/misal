dd="2021-02-20"
dd=$1
mkdir -p "docs/api/v1/obs/${dd}/olejnik"
for i in $(jq -r '.misal | keys_unsorted[]' "docs/api/v1/${dd}.json" | grep -v pred)
do
  echo "templates/${i}.html --> ${dd}/${i}.html"
  jinja -d "docs/api/v1/${dd}.json" "templates/${i}.j2" -o "docs/api/v1/obs/${dd}/${i}.html"
done
jinja -d "docs/api/v1/olejnik/${dd}.json" "templates/olejnik/mezizpev.j2" -o "docs/api/v1/obs/${dd}/olejnik/mezizpev.html"
