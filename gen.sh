#/bin/bash
OUT="$(mktemp)"
trap 'rm -f -- "$OUT"' EXIT

#echo $OUT

for i in $(cd docs; ls -1r misal/*.md | head -n 28)
do
  echo "    - $(basename ${i%.*}): $i" >> $OUT
done

echo "$(date --rfc-3339=date) (dnes)"
echo "$(date -d "1 day" --rfc-3339=date) (včera)"
echo "$(date -d "-1 day" --rfc-3339=date) (zítra)"

sed -i "s/\($(date --rfc-3339=date)\):/\1 (dnes):/" $OUT
sed -i "s/\($(date -d '-1 day' --rfc-3339=date)\):/\1 (včera):/" $OUT

grep 'včera' $OUT -A 8 -B 30 > "${OUT}.bak"

sed -n '1,/start/p;/end/,$p' mkdocs.yml > mkdocs.yml.bak
sed "/days start/ r ${OUT}.bak" mkdocs.yml.bak > mkdocs.yml
rm -f mkdocs.yml.bak
rm -f "${OUT}.bak"

sed -i "s/\(.*index.*\/\)\(.*\)\(.md'\)/\1$(date --rfc-3339=date)\3/" mkdocs.yml
sed -i "s/\(.*dnes.*\/\)\(.*\)\(.md'\)/\1$(date --rfc-3339=date)\3/" mkdocs.yml

ARCHIVE="docs/archiv.md"

echo """# Archiv

""" > $ARCHIVE
# Archive
for i in $(cd docs; ls -1r misal/*.md)
do
  echo "* [$(basename ${i%.*})]($i)" >> $ARCHIVE
done

sed -i "s/\($(date --rfc-3339=date)\)/\1 (dnes)/" $ARCHIVE
sed -i "s/\($(date -d '-1 day' --rfc-3339=date)\)/\1 (včera)/" $ARCHIVE
