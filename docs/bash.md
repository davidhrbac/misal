Pro práci v Bash je potřeba mít nainstalováno:

!!! hint "nástroje"
    * curl
    * jq

Z datových souborů JSON se data dají velmi elegantně extrahovat. Následující příklady ilustrují případné příkazy.

## Zobrazení 1. čtení

``` bash hl_lines="1"
$ curl -s https://misal.hrbac.cz/test/api/v1/2021-02-08.json | jq '.misal."1-cteni"'
{
  "name": "1. čtení – Gn 1,1-19",
  "type": "1. čtení",
  "ref": "Gn 1,1-19",
  "lines": [
    "Bůh řekl, a stalo se tak.",
    "Začátek první knihy Mojžíšovy.",
    "Na počátku stvořil Bůh nebe a zemi. Země však byla pustá a prázdná, temnota byla nad propastnou hlubinou a Boží dech vanul nad vodami. Tu Bůh řekl: „Buď světlo!“ A bylo světlo. Bůh viděl, že světlo je dobré, a oddělil světlo od temnoty. Bůh nazval světlo dnem a temnotu nocí. Nastal večer, nastalo jitro – den první. Potom Bůh řekl: „Ať je obloha uprostřed vod, ať odděluje jedny vody od druhých!“ A stalo se tak. Bůh udělal oblohu, která oddělila vody pod oblohou od vod nad oblohou. Oblohu nazval Bůh nebem. Nastal večer, nastalo jitro – den druhý. Potom Bůh řekl: „Ať se shromáždí vody, které jsou pod nebem, na jedno místo a ukáže se souš!“ A stalo se tak. Bůh nazval souš zemí a shromážděné vody nazval mořem. Bůh viděl, že je to dobré. Bůh řekl: „Ať vydá země zeleň, semenotvorné rostliny a ovocné stromy, které plodí na zemi ovoce nejrůznějšího druhu, s jádry uvnitř!“ A stalo se tak. Tu země vydala zeleň, semenotvorné rostliny nejrůznějších druhů a stromy nesoucí ovoce s jádry uvnitř, nejrůznějšího druhu. A Bůh viděl, že je to dobré. Nastal večer, nastalo jitro – den třetí. Potom Bůh řekl: „Ať jsou svítilny na nebeské obloze, aby oddělovaly den od noci a byly jako znamení, ať (označují) údobí, dny a roky. Ať jsou svítilnami na nebeské obloze a osvětlují zemi!“ A stalo se tak. Bůh udělal dvě velké svítilny: svítilnu větší, aby vládla dni, a svítilnu menší, aby vládla noci, a hvězdy. Bůh je umístil na nebeskou oblohu, aby svítily na zem, vládly dni a noci a oddělovaly světlo od temnoty. Bůh viděl, že je to dobré. Nastal večer, nastalo jitro – den čtvrtý."
  ]
}
```

## Zobrazení 1. čtení - zdroj

``` bash hl_lines="1"
$ curl -s https://misal.hrbac.cz/test/api/v1/2021-02-08.json | jq '.misal."1-cteni".lines[1]'
"Začátek první knihy Mojžíšovy."
```

## Zobrazení 1. čtení - jméno

``` bash hl_lines="1"
$ curl -s https://misal.hrbac.cz/test/api/v1/2021-02-08.json | jq '.misal."1-cteni".name'
"1. čtení – Gn 1,1-19"
```

## Zobrazení 1. čtení - text

``` bash hl_lines="1"
$ curl -s https://misal.hrbac.cz/test/api/v1/2021-02-08.json | jq '.misal."1-cteni".lines[-1]'
"Na počátku stvořil Bůh nebe a zemi. Země však byla pustá a prázdná, temnota byla nad propastnou hlubinou a Boží dech vanul nad vodami. Tu Bůh řekl: „Buď světlo!“ A bylo světlo. Bůh viděl, že světlo je dobré, a oddělil světlo od temnoty. Bůh nazval světlo dnem a temnotu nocí. Nastal večer, nastalo jitro – den první. Potom Bůh řekl: „Ať je obloha uprostřed vod, ať odděluje jedny vody od druhých!“ A stalo se tak. Bůh udělal oblohu, která oddělila vody pod oblohou od vod nad oblohou. Oblohu nazval Bůh nebem. Nastal večer, nastalo jitro – den druhý. Potom Bůh řekl: „Ať se shromáždí vody, které jsou pod nebem, na jedno místo a ukáže se souš!“ A stalo se tak. Bůh nazval souš zemí a shromážděné vody nazval mořem. Bůh viděl, že je to dobré. Bůh řekl: „Ať vydá země zeleň, semenotvorné rostliny a ovocné stromy, které plodí na zemi ovoce nejrůznějšího druhu, s jádry uvnitř!“ A stalo se tak. Tu země vydala zeleň, semenotvorné rostliny nejrůznějších druhů a stromy nesoucí ovoce s jádry uvnitř, nejrůznějšího druhu. A Bůh viděl, že je to dobré. Nastal večer, nastalo jitro – den třetí. Potom Bůh řekl: „Ať jsou svítilny na nebeské obloze, aby oddělovaly den od noci a byly jako znamení, ať (označují) údobí, dny a roky. Ať jsou svítilnami na nebeské obloze a osvětlují zemi!“ A stalo se tak. Bůh udělal dvě velké svítilny: svítilnu větší, aby vládla dni, a svítilnu menší, aby vládla noci, a hvězdy. Bůh je umístil na nebeskou oblohu, aby svítily na zem, vládly dni a noci a oddělovaly světlo od temnoty. Bůh viděl, že je to dobré. Nastal večer, nastalo jitro – den čtvrtý."
```
