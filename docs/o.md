Cílem projektu je poskytnout otevřená data ve strojově čitelném formátu k dalšímu možnému zpracování.

Data jsou čerpána z **veřejně** dostupných zdrojů a jsou **primárně** připravována pro [Youtube kanál](https://www.youtube.com/channel/UC1_za1Bje20GqJ7HwGDVluw) vysílání [Římskokatolické farnosti Ostrava-Poruba a Vřesina](http://www.fporuba.cz/).

!!! hint "Textové zdroje"
    * web [liturgie.cz](http://m.liturgie.cz/misal/index.htm)
    * web [josefolejnik.cz](https://josefolejnik.cz/liturgicky-kalendar/)
