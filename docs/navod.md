Aktuálně se daří parsovat běžné dny a běžnou neděli. Svátky a slavnosti či alternativní varianty jsou s ohledem na zdroj dat zatím problematické.

Přístup k datům je možný ve třech formátech, přičemž pouze **poslední dva jsou vhodné pro strojové zpracování**.

!!! hint "typy souborů"
    * HTML
    * JSON
    * YAML

## Přístup na API

Strojově čitelná data jsou přístupná přes API na adrese https://misal.hrbac.cz/api/v1/

!!! hint "struktura API V1"
    ```
    api/
    └── v1/
        ├── obs/
        │   ├── 2021-02-21
        │   │   ├── evangelium.html
        │   │   ├── mezizpev.html
        │   │   ├── olejnik/
        │   │   │   └── mezizpev.html
        │   │   ├── 1-cteni.html
        │   │   └── 2-cteni.html
        ├── olejnik/
        │   ├── 2021-02-21.json
        │   └── 2021-02-21.yaml
        ├── 2021-02-21.json
        └── 2021-02-21.yaml
    ```

## Testovací data

Pro účely testování jsou dostupná testovací data, která jsou konstantní.

!!! important "typy souborů"
    * HTML - [https://misal.hrbac.cz/test/misal/2021-02-08/](https://misal.hrbac.cz/test/misal/2021-02-08/)
    * JSON - [https://misal.hrbac.cz/test/api/v1/2021-02-08.json](https://misal.hrbac.cz/test/api/v1/2021-02-08.json)
    * YAML - [https://misal.hrbac.cz/test/api/v1/2021-02-08.yaml](https://misal.hrbac.cz/test/api/v1/2021-02-08.yaml)
