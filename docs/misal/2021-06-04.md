---
title: '2021-06-04'
---
# Pátek po 9. neděli v mezidobí

## 1. čtení – Tob 11,5-17

*Čtení z knihy Tobiáš*

Anna seděla a obhlížela cestu, kudy odešel její syn. Zpozorovala, že přichází, a řekla jeho otci: "Hle, přichází tvůj syn i ten člověk, který odešel s ním." Rafael řekl Tobiášovi, dříve než se přiblížil k otci: "Vím, že jeho oči budou otevřeny. Potři rybí žlučí jeho oči. Ten lék vytáhne a odstraní z jeho očí bílý zákal. Tvůj otec prohlédne a uvidí světlo." Anna přiběhla, padla svému synovi kolem krku a řekla mu: "Opět tě vidím, dítě. Teď už mohu zemřít." A dala se do pláče. (Otec) Tobiáš vstal; klopýtal, ale došel ke dveřím do nádvoří. (Syn) Tobiáš k němu přistoupil s rybí žlučí v ruce; dýchl mu do očí, podepřel ho a řekl: "Buď dobré mysli, otče." A přiložil lék na jeho oči. Pak oběma rukama odstranil bílý zákal z koutků očí. Otec mu padl kolem krku, rozplakal se a řekl mu: "Opět tě vidím, synu, světlo mých očí." A pokračoval: "Požehnaný je Bůh a požehnané jeho veliké jméno. Požehnaní jsou všichni jeho svatí andělé po všechny věky. Neboť on mne stihl ranou - a hle, vidím svého syna Tobiáše." (Otec) Tobiáš a jeho manželka Anna s radostí vešli do domu a z celého srdce dobrořečili Bohu za všechno, co se stalo. (Syn) Tobiáš oznámil otci, že se jeho cesta zdařila a že přinesl stříbro. Také jak si vzal za manželku Sáru, dceru Raguelovu, a ta že právě přichází a je blízko ninivské brány. (Otec) Tobiáš a Anna se radovali a vyšli vstříc své snaše k ninivské bráně. Když ho viděli ninivští obyvatelé, jak jde a kráčí v plné síle a že ho nikdo nevede za ruku, užasli. Vyznal před nimi, že se nad ním Bůh smiloval a otevřel jeho oči. Pak přistoupil k Sáře, manželce svého syna, a požehnal jí slovy: "Kéž vejdeš ve zdraví, dcero. Požehnaný tvůj Bůh, že tě k nám přivedl, dcero. A požehnaný tvůj otec, požehnaný můj syn Tobiáš a požehnaná ty, dcero. Vejdi do svého domu ve zdraví, s požehnáním a s radostí; vejdi, dcero!"
Toho dne nastala veliká radost všem židům žijícím v Ninive.

## Mezizpěv – Žl 146,1b-2.6c-7.8-9a.9bc-10

Duše má, chval Hospodina!

## Zpěv před evangeliem – Jan 14,23

Aleluja. Kdo mě miluje, bude zachovávat mé slovo, praví Pán, a můj Otec ho bude milovat a přijdeme k němu. Aleluja.

## Evangelium – Mk 12,35-37

*Slova svatého evangelia podle Marka*

Když Ježíš učil v chrámě, řekl: "Jak mohou učitelé Zákona tvrdit, že Mesiáš je syn Davidův? Vždyť sám David pravil v Duchu Svatém: `Řekl Hospodin mému Pánu: Zasedni po mé pravici, dokud ti 
nepoložím tvé nepřátele pod nohy.' Sám David ho nazývá Pánem, jak tedy může být jeho synem?" A lidé ve velkém počtu mu rádi naslouchali.

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-06-04.json](/api/v1/2021-06-04.json)
    * YAML - [2021-06-04.yaml](/api/v1/2021-06-04.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-06-04.json](/api/v1/olejnik/2021-06-04.json)
    * YAML - [olejnik/2021-06-04.yaml](/api/v1/olejnik/2021-06-04.yaml)

### Pravopis

!!! failure ""
    {--*dobrořečili*--}: dobrořečení<br>
    {--*Ninive*--}: Ninině, Ničivé, Ničivě<br>
    {--*ninivské*--}: Miňovské, nicovské, níhovské, jinínské, neněvské, vinidské<br>
    {--*ninivští*--}: Miňovští, mínívati, ničivosti, ničivostí, nicovští, níhovští, činívati, jinínští, neněvští, vinidští<br>
    {--*Raguelovu*--}: Rafaelovu, Rauerovu, Samuelovu<br>

### Typografie

!!! bug ""
    Když Ježíš učil v chrámě, řekl: "Jak mohou učitelé Zákona tvrdit, že Mesiáš je syn Davidův? Vždyť sám David pravil v Duchu Svatém: {--*\`*--}Řekl Hospodin mému Pánu: Zasedni po mé pravici, dokud ti 

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-06-04/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-06-04/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-06-04/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-06-04/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "green")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
