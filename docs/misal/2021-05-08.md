---
title: '2021-05-08'
---
# Sobota po 5. neděli velikonoční

## 1. čtení – Sk 16,1-10

*Čtení ze Skutků apoštolů*

Pavel přišel do Derbe a Lystry. Tam byl jeden učedník jménem Timotej. Byl to syn židovky, která se přidala k víře, a pohanského otce. Protože měl u bratří v Lystře a v Ikóniu dobrou pověst, chtěl si ho Pavel vzít s sebou na cesty. Z ohledu na tamější židy ho obřezal, protože všichni věděli, že jeho otec byl pohan. Cestou pak ve všech městech, kudy procházeli, sdělovali ta ustanovení, o kterých apoštolové a starší v Jeruzalémě rozhodli, že se mají zachovávat. Tak se církevní obce utvrzovaly ve víře a jejich počet každý den velmi rostl. Potom šli přes Frýgii a galatským krajem, protože Duch Svatý jim zabránil kázat slovo Boží v (provincii) Asii. Když došli k Mýsii, chtěli přejít do Bithýnie, ale Ježíšův Duch jim to nedovolil. Prošli tedy Mýsií dolů do Troady. V noci pak měl Pavel vidění: stál před ním nějaký Makedoňan a prosil ho: „Přejdi do Makedonie a pomoz nám!“ Po tomto jeho vidění jsme se snažili dostat co nejdříve na cestu do Makedonie. Usoudili jsme totiž z toho, že nás tam Bůh volá, abychom jim hlásali radostnou zvěst.

## Mezizpěv – Žl 100,1-2.3.5

Plesejte Hospodinu, všechny země.

## Zpěv před evangeliem – Kol 3,1

Aleluja. Když jste s Kristem byli vzkříšeni, usilujte o to, co pochází shůry, kde je Kristus po Boží pravici. Aleluja.

## Evangelium – Jan 15,18-21

*Slova svatého evangelia podle Jana*

Ježíš řekl svým učedníkům: „Jestliže vás svět nenávidí, uvažte, že mne nenáviděl dříve než vás. Kdybyste byli ze světa, svět by miloval to, co mu patří. Že však nejste ze světa, ale já jsem vás ze světa vyvolil, proto vás svět nenávidí. Vzpomeňte si na slovo, které jsem vám řekl: `Služebník není víc než jeho pán.' Když pronásledovali mne, budou pronásledovat i vás. Když zachovali moje slovo, budou zachovávat i vaše. Ale to všechno vám způsobí kvůli mému jménu, protože neznají toho, který mě poslal.“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-05-08.json](/api/v1/2021-05-08.json)
    * YAML - [2021-05-08.yaml](/api/v1/2021-05-08.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-05-08.json](/api/v1/olejnik/2021-05-08.json)
    * YAML - [olejnik/2021-05-08.yaml](/api/v1/olejnik/2021-05-08.yaml)

### Pravopis

!!! failure ""
    {--*Derbe*--}: Děrné, Děrně, Dere, Drbe, Erbe, Derby, Derme, Derte, Dělbě<br>
    {--*Frýgii*--}: Frygičtí, Freii, Freií, Fryči, Orgií<br>
    {--*Ikóniu*--}: Ikonou, Ikonu, Ikonku<br>
    {--*Lystry*--}: Lustry, Bystrý, Bystrá<br>
    {--*Lystře*--}: Lustre, Bystré, Bystře, Vystře, Ly stře, Ly-stře<br>
    {--*Makedoňan*--}: Makedonkám, Makedonka, Makedončin, Makedonce, Makedonci, Makedonců, Makedonec, Makedonek, Makedonie, Makedonii, Makedonií, Makedonko, Makedonku, Makedonky, Makedonče<br>
    {--*Mýsii*--}: Myšici, Myšicí, Myšími, Myši, Myší, Mysík, Misii, Misií, Mysli, Myslí, Myšic, Myšin, Myšmi, Myším<br>
    {--*Mýsií*--}: Myšici, Myšicí, Myšími, Myši, Myší, Mysík, Misii, Misií, Mysli, Myslí, Myšic, Myšin, Myšmi, Myším<br>
    {--*stádce*--}: Stadice, Stádlce, stače, stádě, stáče, sádce, štace, Starce, Starče, Stánce, Stázce, sladce, starce, starče, stydce, stádně, stávce<br>
    {--*Timotej*--}: Timotea, Timotee, Timotei, Timoteu, Timoteů, Ti motej, Ti-motej<br>
    {--*Troady*--}: Triády, Trojdy, Troudy<br>

### Typografie

!!! bug ""
    Plesejte Hospodinu, všechny zem{--*ě,s[m[Klužte Hospodinu s radost[01;31m[Kí,v*--}stupte před něho s jásotem.
    Uznejte, že Hospodin je Bůh:on nás učinil, a my mu náležím{--*e,j*--}sme jeho lid a stádce jeho pastvy.
    Neboť Hospodin je dobr{--*ý,j[m[Keho milosrdenství je věčn[01;31m[Ké,p*--}o všechna pokolení trvá jeho věrnost.
    Ježíš řekl svým učedníkům: „Jestliže vás svět nenávidí, uvažte, že mne nenáviděl dříve než vás. Kdybyste byli ze světa, svět by miloval to, co mu patří. Že však nejste ze světa, ale já jsem vás ze světa vyvolil, proto vás svět nenávidí. Vzpomeňte si na slovo, které jsem vám řekl: {--*\`*--}Služebník není víc než jeho pán.' Když pronásledovali mne, budou pronásledovat i vás. Když zachovali moje slovo, budou zachovávat i vaše. Ale to všechno vám způsobí kvůli mému jménu, protože neznají toho, který mě poslal.“

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-05-08/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-05-08/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-05-08/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-05-08/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "white")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
