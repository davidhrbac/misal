---
title: '2021-06-25'
---
# Pátek po 12. neděli v mezidobí

## 

null

null

## Mezizpěv – Žl 128,1-2.3.4-5

Hle, tak bývá požehnán muž, který se bojí Hospodina. 

## Zpěv před evangeliem – Mt 8,17

Aleluja. Pán vzal na sebe naše slabosti a nesl naše nemoci. Aleluja.

## Evangelium – Mt 8,1-4

*Slova svatého evangelia podle Matouše*

Když Ježíš sestoupil s hory, šly za ním velké zástupy. Tu přišel jeden malomocný, klekl před ním a řekl: „Pane, chceš-li, můžeš mě očistit.“ Vztáhl ruku, dotkl se ho a řekl: „Chci, buď čistý.“ A hned byl od svého malomocenství očištěn. Ježíš mu pak řekl: „Ne abys někomu o tom říkal! Ale jdi, ukaž se knězi a obětuj dar, jak nařídil Mojžíš – 
jim na svědectví.“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-06-25.json](/api/v1/2021-06-25.json)
    * YAML - [2021-06-25.yaml](/api/v1/2021-06-25.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-06-25.json](/api/v1/olejnik/2021-06-25.json)
    * YAML - [olejnik/2021-06-25.yaml](/api/v1/olejnik/2021-06-25.yaml)

### Pravopis

!!! failure ""
    {--*Abrám*--}: Bárám, Abrhám, Ambrám, Babrám, Žábrám, Aurám<br>
    {--*Abrámovi*--}: Abrhámovi, Baranovi, Abrhámovo, Abrhámovu, Abrahamovi, Abrahámovi, Barákovi, Barákoví, Rámoví, Abrhámova, Abrhámovy, Abrhámová, Abrhámové, Abrhámově, Ayřanovi, Zbraňoví, Adamovi, Arabovi, Bradovi, Bromovi, Brátovi, Arakoví, Bradoví, Brakoví, Bromoví, Gramoví, Trámoví, Olbramovi, Oubramovi, Walramovi, Vibramoví, Chrámoví, Obratoví, Obrazoví<br>
    {--*Izmaele*--}: Izraele, Zamele, Izabele, Izraelce, Izraelec, Izraelek, Izraelem, Izraelče, Izrael, Izraeli, Izraelí, Izraelů<br>
    {--*Izmael*--}: Izrael, Zamel, Izabel, Zmáčel, Zimmel, Izme, Zmal, Zmel, Změl, Izraele, Izraeli, Izraelí, Izraelů, Izmem, Zmagl, Zmall, Zmapl, Zmákl, Zmátl, Vzmátl<br>
    {--*Sáraj*--}: Sára, Šára, Sárám, Šarád, Sár aj, Sár-aj<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni-gn-17-1-5-9-10-15-22.html](/api/v1/obs/2021-06-25/1-cteni-gn-17-1-5-9-10-15-22.html)
    * [mezizpev.html](/api/v1/obs/2021-06-25/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-06-25/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-06-25/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "green")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
