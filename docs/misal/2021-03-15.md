---
title: '2021-03-15'
---
# Pondělí po 4. neděli postní

## 1. čtení – Iz 65,17-21

*Čtení z knihy proroka Izaiáše*

Toto praví Hospodin: „Hle, utvořím nová nebesa a novou zemi, nebude se vzpomínat na to, co minulo, (nikomu) to nevstoupí na mysl. Plesat a jásat budou navěky nad tím, co stvořím: neboť - hle - v jásot přetvořím Jeruzalém a lid jeho v radost! Plesat budu nad Jeruzalémem, radovat se budu ze svého lidu. Neuslyší se v něm hlas pláče nebo křiku. Nebude v něm dítě, (které by žilo) jen několik dní, ani stařec, který nenaplní svá léta; teprve (ve stáří) sta let jinoch zemře, za prokletého bude považován, kdo se nedožije sta let. Vystavějí si domy a budou (v nich) bydlet, vysadí vinice a budou jíst jejich plody.“

## Mezizpěv – Žl 30,2+4.5-6.11-12a+13b

Chci tě oslavovat, Hospodine, neboť jsi mě vysvobodil.

## Zpěv před evangeliem – srov. Am 5,14

Hledejte dobro, a ne zlo, abyste žili, a Pán bude s vámi.

## Evangelium – Jan 4,43-54

*Slova svatého evangelia podle Jana*

Ježíš odešel ze Samařska do Galileje. Sám totiž dosvědčil, že prorok ve svém rodném kraji není ve vážnosti. Když tedy přišel do Galileje, Galilejané ho (vlídně) přijali, protože viděli všechno, co vykonal v Jeruzalémě o svátcích; i oni tam totiž byli na svátky. Přišel tedy zase do Kány v Galileji, kde proměnil vodu ve víno. Byl tam jeden královský úředník, jehož syn ležel nemocen v Kafarnau. Když uslyšel, že Ježíš přišel z Judska do Galileje, vyhledal ho a prosil, aby šel a uzdravil mu syna - už totiž skoro umíral. Ježíš mu řekl: „Jestliže neuvidíte znamení a zázraky, nikdy neuvěříte.“ Královský úředník mu odpověděl: „Pane, přijď, než moje dítě umře!“ Ježíš mu řekl: „Jen jdi, tvůj syn je živ.“ Ten člověk uvěřil tomu slovu, které mu Ježíš řekl, a šel. Když ještě byl na cestě, přišli mu naproti jeho služebníci a hlásili: „Tvůj syn je živ!“ Zeptal se jich tedy na hodinu, kdy mu začalo být lépe. Odpověděli mu: „Včera v jednu hodinu odpoledne mu přestala horečka.“ Poznal tedy otec, že to bylo právě v tu chvíli, kdy mu Ježíš řekl: `Tvůj syn je živ.' A uvěřil on i všichni v jeho domě. To bylo druhé znamení, které Ježíš vykonal, když se vrátil z Judska do Galileje.

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-03-15.json](/api/v1/2021-03-15.json)
    * YAML - [2021-03-15.yaml](/api/v1/2021-03-15.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-03-15.json](/api/v1/olejnik/2021-03-15.json)
    * YAML - [olejnik/2021-03-15.yaml](/api/v1/olejnik/2021-03-15.yaml)

### Pravopis

!!! failure ""
    {--*Galilejané*--}: Galilejské<br>
    {--*Galileje*--}: Galileem, Galeje, Galilejské, Galileové, Galileově, Galilea, Galileo, Galileu, Galiley, Galileů, Basileje, Galileům, Galileův<br>
    {--*Galileji*--}: Galileo, Galileu, Galileů, Galejí, Galilejští, Galileovi, Galiemi, Galilea, Galiley, Basileji, Basilejí, Galileem, Galileům, Galileův, Nalitěji, Pálivěji, Zalitěji, Zavileji, Šálivěji<br>
    {--*Kafarnau*--}: Katařanu, Katařanů, Kasárna, Kavárna, Kavárnu, Kasárnám, Kavárnou, Kavárnám<br>
    {--*Kány*--}: Kainy, Kanty, Kaňky, Klány, Kárný, Kalný, Kamny, Kanýr, Kašny, Klany, Kmány, Kyany, Jany, Kant, Kaňu, Kaňů, Káby, Káňu, Káňů, Kanu, Lany, Lány, Kaň, Skaný, Tkaný, Tkány, Dany, Dány, Hany, Hány, Kaly, Kani, Kaňa, Kaňo, Kony, Kuny, Káni, Káry, Káňa, Káňo, Many, Pany, Pány, Daný, Kana, Kane, Kary, Kasy, Katy, Kazy, Kiny, Kály, Káně, Kání, Kávy, Raný, Rány, Taný, Tány, Vany, Wany<br>
    {--*Samařska*--}: Sadařská, Saharská, Šamanská<br>
    {--*zachovals*--}: Zachovala, zachovala, zachovalá, Zachoval, zachoval, zachováš, Zachovale, Zachovalu, Zachovaly, Zachovalů, zachovale, zachovali, zachovalo, zachovaly, zachovalé, zachovalí, zachovalý<br>
    {--*zjitra*--}: jitra, zjitř, zítra, zjitře, zjitři, zjitřl, zjitří<br>

### Typografie

!!! bug ""
    Ježíš odešel ze Samařska do Galileje. Sám totiž dosvědčil, že prorok ve svém rodném kraji není ve vážnosti. Když tedy přišel do Galileje, Galilejané ho (vlídně) přijali, protože viděli všechno, co vykonal v Jeruzalémě o svátcích; i oni tam totiž byli na svátky. Přišel tedy zase do Kány v Galileji, kde proměnil vodu ve víno. Byl tam jeden královský úředník, jehož syn ležel nemocen v Kafarnau. Když uslyšel, že Ježíš přišel z Judska do Galileje, vyhledal ho a prosil, aby šel a uzdravil mu syna - už totiž skoro umíral. Ježíš mu řekl: „Jestliže neuvidíte znamení a zázraky, nikdy neuvěříte.“ Královský úředník mu odpověděl: „Pane, přijď, než moje dítě umře!“ Ježíš mu řekl: „Jen jdi, tvůj syn je živ.“ Ten člověk uvěřil tomu slovu, které mu Ježíš řekl, a šel. Když ještě byl na cestě, přišli mu naproti jeho služebníci a hlásili: „Tvůj syn je živ!“ Zeptal se jich tedy na hodinu, kdy mu začalo být lépe. Odpověděli mu: „Včera v jednu hodinu odpoledne mu přestala horečka.“ Poznal tedy otec, že to bylo právě v tu chvíli, kdy mu Ježíš řekl: {--*\`*--}Tvůj syn je živ.' A uvěřil on i všichni v jeho domě. To bylo druhé znamení, které Ježíš vykonal, když se vrátil z Judska do Galileje.

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-03-15/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-03-15/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-03-15/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-03-15/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "deep-purple")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
