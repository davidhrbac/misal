---
title: '2021-04-25'
---
# 4. neděle velikonoční

## 1. čtení – Sk 4,8-12

*Čtení ze Skutků apoštolů*

Petr, naplněn Duchem Svatým, řekl: „Přední mužové v lidu a starší! Když se dnes musíme odpovídat z dobrého skutku na nemocném člověku, kým že byl uzdraven, tedy ať to víte vy všichni a celý izraelský národ: Ve jménu Ježíše Krista Nazaretského, kterého jste vy ukřižovali, ale kterého Bůh vzkřísil z mrtvých: skrze něho stojí tento člověk před vámi zdravý. On je ten `kámen, který jste vy stavitelé odhodili, ale z kterého se stal kámen nárožní'. V nikom jiném není spásy. Neboť pod nebem není lidem dáno žádné jiné jméno, v němž bychom mohli dojít spásy.“

## Mezizpěv – Žl 118,1+8-9.21-23.26+28-29

Kámen, který stavitelé zavrhli, stal se kvádrem nárožním.

## 2. čtení – 1 Jan 3,1-2

*Čtení z prvního listu svatého apoštola Jana*

Milovaní! Hleďte, jak velikou lásku nám Otec projevil, že se (nejen) smíme nazývat Božími dětmi, ale (že jimi) také jsme! Proto nás svět nezná, že nepoznal jeho. Milovaní, už teď jsme Boží děti. Ale čím budeme, není ještě zřejmé. Víme však, že až on se ukáže, budeme mu podobní, a proto ho budeme vidět tak, jak je.

## Zpěv před evangeliem – Jan 10,14

Aleluja. Já jsem dobrý pastýř, praví Pán, znám svoje ovce a moje ovce znají mne. Aleluja.

## Evangelium – Jan 10,11-18

*Slova svatého evangelia podle Jana*

Ježíš řekl: „Já jsem pastýř dobrý! Dobrý pastýř dává za ovce svůj život. Kdo je najatý za mzdu a není pastýř a jemuž ovce nepatří, (jak) vidí přicházet vlka, opouští ovce a dává se na útěk - a vlk je uchvacuje a rozhání - vždyť (kdo) je najatý za mzdu, tomu na ovcích nezáleží. Já jsem dobrý pastýř; znám svoje (ovce) a moje (ovce) znají mne, jako mne zná Otec a já znám Otce; a za ovce dávám svůj život. Mám i jiné ovce, které nejsou z tohoto ovčince. Také ty musím přivést; a uposlechnou mého hlasu a bude jen jedno stádce, jen jeden pastýř. Proto mě Otec miluje, že dávám svůj život, a zase ho přijmu nazpátek. Nikdo mi ho nemůže vzít, ale já ho dávám sám od sebe. Mám moc (život) dát a mám moc ho zase přijmout. Takový příkaz jsem dostal od svého Otce.“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-04-25.json](/api/v1/2021-04-25.json)
    * YAML - [2021-04-25.yaml](/api/v1/2021-04-25.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-04-25.json](/api/v1/olejnik/2021-04-25.json)
    * YAML - [olejnik/2021-04-25.yaml](/api/v1/olejnik/2021-04-25.yaml)

### Pravopis

!!! failure ""
    {--*Hospodinunež*--}: Hospodinu než, Hospodinu-než<br>
    {--*stádce*--}: Stadice, Stádlce, stače, stádě, stáče, sádce, štace, Starce, Starče, Stánce, Stázce, sladce, starce, starče, stydce, stádně, stávce<br>

### Typografie

!!! bug ""
    Petr, naplněn Duchem Svatým, řekl: „Přední mužové v lidu a starší! Když se dnes musíme odpovídat z dobrého skutku na nemocném člověku, kým že byl uzdraven, tedy ať to víte vy všichni a celý izraelský národ: Ve jménu Ježíše Krista Nazaretského, kterého jste vy ukřižovali, ale kterého Bůh vzkřísil z mrtvých: skrze něho stojí tento člověk před vámi zdravý. On je ten {--*\`*--}kámen, který jste vy stavitelé odhodili, ale z kterého se stal kámen nárožní'. V nikom jiném není spásy. Neboť pod nebem není lidem dáno žádné jiné jméno, v němž bychom mohli dojít spásy.“
    Oslavujte Hospodina, neboť je dobr{--*ý,j[m[Keho milosrdenství trvá navěk[01;31m[Ky.L[m[Képe je utíkat se k Hospodinunež důvěřovat v člověk[01;31m[Ka.L*--}épe je utíkat se k Hospodinunež důvěřovat v mocné.
    Děkuji ti, žes mě vyslyšela stal se mou spáso{--*u.K[m[Kámen, který stavitelé zavrhl[01;31m[Ki,s[m[Ktal se kvádrem nárožní[01;31m[Km.H[m[Kospodinovým řízením se tak stal[01;31m[Ko,j*--}e to podivuhodné v našich očích.
    Požehnaný, kdo přichází v Hospodinově jmén{--*u.Ž[m[Kehnáme vám z Hospodinova dom[01;31m[Ku.T[m[Ky jsi můj Bůh, děkuji t[01;31m[Ki;b[m[Kudu tě slavit, můj Bože!Oslavujte Hospodina, neboť je dobr[01;31m[Ký,j*--}eho milosrdenství trvá navěky.

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-04-25/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-04-25/mezizpev.html)
    * [2-cteni.html](/api/v1/obs/2021-04-25/2-cteni.html)
    * [evangelium.html](/api/v1/obs/2021-04-25/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-04-25/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "white")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
