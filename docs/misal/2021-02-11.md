---
title: '2021-02-11'
---
# Čtvrtek po 5. neděli v mezidobí

## 1. čtení – Gn 2,18-25

*Čtení z první knihy Mojžíšovy*

Hospodin Bůh řekl: „Není dobré, že člověk je sám. Udělám mu pomocníka, který by se k němu hodil.“ Hospodin Bůh uhnětl z hlíny všechnu divokou zvěř a všechno nebeské ptactvo a přivedl (je) k člověku, aby viděl, jaké jim dá jméno: takové mělo být jejich jméno, jak by všechny živočichy pojmenoval. A člověk dal jméno všem krotkým zvířatům, nebeskému ptactvu a veškeré divoké zvěři, ale pro člověka se nenašel pomocník, který by se k němu hodil. Tu Hospodin Bůh seslal na člověka hluboký spánek, a když usnul, vzal jedno z jeho žeber a to místo uzavřel masem. Hospodin Bůh pak ze žebra, které vzal z člověka, vytvořil ženu a přivedl ji k člověku. Ten zvolal: „To je konečně kost z mých kostí a tělo z mého těla! Bude se nazývat manželkou, neboť z manžela byla vzata.“ Proto muž opustí otce i matku a přidrží se své ženy a budou jeden člověk. Oba dva byli nazí, člověk i jeho žena, ale nestyděli se.

## Mezizpěv – Žl 128,1-2.3.4-5

Blaze každému, kdo se bojí Hospodina.

## Zpěv před evangeliem – Jak 1,21bc

Aleluja. Buďte vnímaví pro slovo, které do vás bylo vloženo jako semeno a může zachránit vaši duši. Aleluja.

## Evangelium – Mk 7,24-30

*Slova svatého evangelia podle Marka*

Ježíš se vydal na cestu do tyrského kraje. Vešel do jednoho domu a nechtěl, aby to někdo věděl, ale nemohlo se to utajit. Hned o něm uslyšela nějaká žena, jejíž dcera byla posedlá nečistým duchem. Přišla a padla mu k nohám. Byla to pohanka, rodem Syroféničanka, a prosila ho, aby vyhnal zlého ducha z její dcery. Řekl jí: „Nech najíst napřed děti! Není přece správné vzít chléb dětem a hodit ho psíkům.“ Ale ona mu odpověděla: „Ovšem, Pane, jenže i psíci se živí pod stolem kousky po dětech.“ Nato jí řekl: „Že jsi to řekla, jdi, zlý duch vyšel z tvé dcery.“ Žena odešla domů a nalezla dítě ležet na lůžku a zlý duch byl pryč.

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-02-11.json](/api/v1/2021-02-11.json)
    * YAML - [2021-02-11.yaml](/api/v1/2021-02-11.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-02-11.json](/api/v1/olejnik/2021-02-11.json)
    * YAML - [olejnik/2021-02-11.yaml](/api/v1/olejnik/2021-02-11.yaml)

### Pravopis

!!! failure ""
    {--*tyrského*--}: štýrského, nýrského, syrského, týnského<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-02-11/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-02-11/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-02-11/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-02-11/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "green")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
