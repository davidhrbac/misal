---
title: '2021-06-30'
---
# Středa po 13. neděli v mezidobí

## 

null

null

## Mezizpěv – Žl 50,7.8-9.10.11.12-13.16bc-17

Kdo žije správně, tomu ukážu Boží spásu.

## Zpěv před evangeliem – Jak 1,18

Aleluja. Otec rozhodl, že nám dá život slovem pravdy, abychom byli jako prvotiny ze všeho, co stvořil. Aleluja.

## Evangelium – Mt 8,28-34

*Slova svatého evangelia podle Matouše*

Když se Ježíš dostal na protější břeh do gadarského kraje, vyšli proti němu dva posedlí, kteří vystoupili z hrobních slují. Byli velmi nebezpeční, takže nikdo nemohl projít tou cestou. Začali křičet: „Co je ti po nás, Boží synu? Přišel jsi nás trápit, dříve než nastal čas?“ Opodál se páslo velké stádo vepřů. Zlí duchové ho tedy prosili: „Vyháníš-li nás, pošli nás do toho stáda vepřů.“ Řekl jim: „Jděte!“ Vyšli tedy a vstoupili do vepřů – a vtom se celé stádo hnalo po příkrém srázu do moře a zahynulo ve vodách. Pasáci utekli, přišli do města a tam všechno oznámili, i o těch posedlých. Tu vyšlo celé město Ježíšovi naproti, a když ho uviděli, prosili ho, aby z jejich kraje odešel.

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-06-30.json](/api/v1/2021-06-30.json)
    * YAML - [2021-06-30.yaml](/api/v1/2021-06-30.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-06-30.json](/api/v1/olejnik/2021-06-30.json)
    * YAML - [olejnik/2021-06-30.yaml](/api/v1/olejnik/2021-06-30.yaml)

### Pravopis

!!! failure ""
    {--*Amosa*--}: Mosa, Ámos, Kámoša, Amora, Ámose, Ámosi, Ámosů<br>
    {--*gadarského*--}: maďarského, sadařského, avarského<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni-am-5-14-15-21-24.html](/api/v1/obs/2021-06-30/1-cteni-am-5-14-15-21-24.html)
    * [mezizpev.html](/api/v1/obs/2021-06-30/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-06-30/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-06-30/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "green")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
