---
title: '2021-05-22'
---
# Sobota po 7. neděli velikonoční

## 1. čtení – Sk 28,16-20.30-31

*Čtení ze Skutků apoštolů*

Když jsme dorazili do Říma, dostal Pavel dovolení, že může bydlet sám s vojákem, který ho hlídal. Za tři dni potom svolal si tamější židovské přední muže. Když se sešli, řekl jim: „Bratři, já jsem neudělal nic proti (izraelskému) lidu ani proti zvykům přijatým od předků. Přesto však jsem byl z Jeruzaléma v poutech vydán do moci Římanům. Ti mě po výslechu chtěli propustit, protože jsem se nedopustil žádného provinění, za které bych zasluhoval smrt. Když se však židé proti tomu stavěli, byl jsem nucen odvolat se k císaři, ale ne proto, že bych chtěl na svůj národ něco žalovat. Kvůli tomu jsem si přál, abych vás mohl uvidět a promluvit s vámi. Vždyť pro naději Izraele (v Mesiáše) nosím tenhle řetěz.“ Celá dvě léta zůstal pak ve svém najatém bytě a přijímal všechny, kdo k němu přicházeli. Zcela otevřeně a bez překážek hlásal Boží království a učil o Pánu Ježíši Kristu.

## Mezizpěv – Žl 11,4.5+7

Zbožní uzří tvou tvář, Hospodine.

## Zpěv před evangeliem – srov. Jan 16,7.13

Aleluja. Pošlu k vám Ducha pravdy, praví Pán; on vás uvede do celé pravdy. Aleluja.

## Evangelium – Jan 21,20-25

*Slova svatého evangelia podle Jana*

Petr se obrátil a viděl, jak za ním jde učedník, kterého Ježíš miloval a který ležel při večeři na jeho prsou a zeptal se: „Pane, kdo tě zradí?“ Když ho tedy Petr viděl, zeptal se Ježíše: „Pane, a co on?“ Ježíš mu odpověděl: „Jestliže chci, aby zůstal, až přijdu, proč se o to staráš? Ty mě následuj!“ Mezi bratry se proto rozšířila řeč, že ten učedník nezemře. Ježíš však Petrovi neřekl: „Nezemře“, ale: „Jestliže chci, aby zůstal, až přijdu, proč se o to staráš?“ To je ten učedník, který o tom všem vydává svědectví a to všechno zaznamenal
a víme, že jeho svědectví je pravdivé. Je však ještě mnoho jiných věcí, které Ježíš vykonal. Kdyby měla být vypsána každá zvlášť, myslím, že by celý svět neobsáhl knihy o tom napsané.

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-05-22.json](/api/v1/2021-05-22.json)
    * YAML - [2021-05-22.yaml](/api/v1/2021-05-22.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-05-22.json](/api/v1/olejnik/2021-05-22.json)
    * YAML - [olejnik/2021-05-22.yaml](/api/v1/olejnik/2021-05-22.yaml)

### Pravopis

!!! failure ""
    N/A

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-05-22/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-05-22/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-05-22/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-05-22/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "white")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
