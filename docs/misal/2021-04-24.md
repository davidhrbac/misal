---
title: '2021-04-24'
---
# Sobota po 3. neděli velikonoční

## 1. čtení – Sk 9,31-42

*Čtení ze Skutků apoštolů*

Církev měla pokoj v celém Judsku, Galileji i Samařsku. S úspěchem se vyvíjela, žila v bázni před Pánem a rostla přispěním Ducha Svatého. Petr se vydal na okružní cestu a přišel i ke křesťanům, kteří žili v Lyddě. Tam našel jednoho člověka, jmenoval se Eneáš, který ležel osm let ochrnutý na lehátku. Petr mu řekl: „Eneáši, Ježíš Kristus ti vrací zdraví! Vstaň a sám si ustel!“ A on ihned vstal. Všichni lyddští a saronští obyvatelé ho viděli, a obrátili se k Pánu. V Joppe žila jedna učednice jménem Tabita, to je v překladu Srnka. Konala velmi mnoho dobrých skutků a bohatě rozdávala almužny. Ale roznemohla se tehdy a zemřela. Umyli ji a položili do horní místnosti. Joppe leží blízko Lyddy. Když učedníci uslyšeli, že je tam Petr, poslali k němu dva muže s prosbou: „Neváhej a přijď sem k nám!“ Petr tedy vstal a šel s nimi. Když tam přišel, zavedli ho do horní místnosti. Všechny vdovy se postavily kolem něho a s pláčem mu ukazovaly sukně a pláště, které jim ušila Srnka, dokud ještě byla s nimi. Petr poslal všechny ven, poklekl a modlil se. Potom se obrátil k mrtvole a řekl: „Tabito, vstaň!“ Ona otevřela oči, podívala se na Petra a posadila se. Podal jí ruku a pozdvihl ji. Zavolal pak dovnitř křesťany a vdovy a ukázal jim ji živou. Zpráva o tom se roznesla po celém Joppe a mnoho lidí přijalo víru v Pána.

## Mezizpěv – Žl 116,12-13.14-15.16-17

Čím se odplatím Hospodinu za všechno, co mi prokázal?

## Zpěv před evangeliem – Jan 6,63b.68b

Aleluja. Tvá slova, Pane, jsou duch a jsou život; ty máš slova věčného života. Aleluja.

## Evangelium – Jan 6,60-69

*Slova svatého evangelia podle Jana*

Mnozí z jeho učedníků řekli: „To je tvrdá řeč! Kdopak to má poslouchat?“ Ježíš věděl sám od sebe, že jeho učedníci na to reptají, a proto jim řekl: „Nad tím se pohoršujete? Co teprve, až uvidíte Syna člověka, jak vystupuje tam, kde byl dříve? Co dává život, je duch, tělo nic neznamená. Slova, která jsem vám mluvil, jsou duch a jsou život. Ale jsou mezi vámi někteří, kdo nevěří.“ Ježíš totiž věděl od začátku, kdo jsou ti, kdo nevěří, a kdo je ten, který ho zradí. A dodal: „Proto jsem vám říkal, že nikdo ke mně nemůže přijít, není-li mu to dáno od Otce.“ Proto mnoho z jeho učedníků odešlo a už s ním nechodili. Ježíš tedy řekl Dvanácti: „I vy chcete odejít?“ Šimon Petr mu odpověděl: „Pane, ke komu půjdeme? Ty máš slova věčného života, a my jsme uvěřili a poznali, že ty jsi ten Svatý Boží!“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-04-24.json](/api/v1/2021-04-24.json)
    * YAML - [2021-04-24.yaml](/api/v1/2021-04-24.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-04-24.json](/api/v1/olejnik/2021-04-24.json)
    * YAML - [olejnik/2021-04-24.yaml](/api/v1/olejnik/2021-04-24.yaml)

### Pravopis

!!! failure ""
    {--*Eneáši*--}: Neraší, Neřasí, Nehasí, Netasí, Naši, Naší, Beneši, Engelsi, Genesi, Genesí, Nejsi, Nepsí, Snáší, Unáší, Vnáší, Eliáši, Hnědší, Invasi, Invasí, Snědší, Vnější<br>
    {--*Eneáš*--}: Neraš, Neřas, Nesa, Nečas, Nedáš, Nehas, Nemáš, Nenesa, Nepas, Netas, Nes, Nás, Náš, Snesa, Unesa, Vnesa, Beneš, Engels, Hnětáš, Hněváš, Nenes, Ženeš, Efes, Enga, Dnes, Enek, Enka, Hneš, Lneš, Mneš, Pneš, Snes, Tneš, Unes, Vnes, Znáš, Žneš, Eliáš, Inkas, Onkáš<br>
    {--*Galileji*--}: Galileo, Galileu, Galileů, Galejí, Galilejští, Galileovi, Galiemi, Galilea, Galiley, Basileji, Basilejí, Galileem, Galileům, Galileův, Nalitěji, Pálivěji, Zalitěji, Zavileji, Šálivěji<br>
    {--*Joppe*--}: Pope, Knoppe, Kippe, Kope, Kopě, Ope, Zippe, Houpe, Kopce, Kopie, Kopne, Kopné, Kopně, Kopre, Kopte, Kopře, Koupe, Koupě, Jobe, Cope, Jode, Jole, Jotě, Jóde, Józe, Mope, Opce, Opře, Ropě, Topě, Pople, Pompě, Popce, Popře, Poupě, Johne, Joure, Jožce, Puppe, Zappe, Docpe, Dopne, Doupě, Doúpě, Jolce, Joule, Loupe, Loupě, Ropné, Ropně, Roupe, Sopce, Sople, Soptě, Topme, Topné, Topně, Topte, Šopce, Šoupe, Šoupě<br>
    {--*Lyddě*--}: Lýdie, Lysé, Lyse, Vyděď, Vydědě, Luďce, Luďme, Luďte, Lymfě, Lysce, Lyské, Lysče, Ladě, Lídě, Lade, Lede, Ledě, Lide, Lidé, Lodě, Lyže, Lyře, Hydre, Ledče, Lidce, Lindě, Lodže, Lyone, Lédle, Lýdii, Lýdií, Nýdře, Rýdle, Bydle, Hydře, Hyzdě, Hýždě, Ladné, Ladně, Laďme, Laďte, Ledne, Lodně, Lorde, Loudě, Loďce, Lynče, Lídře, Mydle, Mýdle, Rydle, Týdne, Týdně, Vydme, Vydře, Vyjde, Vyudě, Vyzdě<br>
    {--*lyddští*--}: lyští, lurdští, Lymští, lidští, lymští, lymeští, lyonští<br>
    {--*Lyddy*--}: Lysý, Ludvy, Luďky, Lymfy, Lysky, Lyský, Lady, Lídy, Ledy, Lyry, Lýky, Cyrdy, Hydry, Landy, Lidky, Lindy, Loudy, Lyony, Lédly, Lýdie, Lýdii, Lýdií, Nýdry, Rydly, Ryndy, Rýdly, Vydry, Bydly, Ladný, Ledky, Ledny, Lordy, Loďky, Lídry, Lýtky, Mýdly, Týdny<br>
    {--*Samařsku*--}: Samařanku, Damašku, Damašků, Samaru, Sársku, Sadařskou, Saharskou, Tamaryšku, Tamaryšků, Šamanskou, Sadařský, Saharsky, Saharský, Šamansky, Šamanský, Samarou, Zámrsku, Zámrsků, Samariu, Šamanku, Maďarsku, Somálsku, Sadařská, Sadařské, Saharská, Saharské, Šafaříku, Šafaříků, Šamalíku, Šamalíků, Šamanská, Šamanské, Šampusku<br>
    {--*saronští*--}: baronští, saxonští, Marovští<br>
    {--*Tabita*--}: Nabita, Nabitá, Zabita, Zabitá, Ta bita, Ta-bita, Rabiát, Rabiáta, Tábora, Anita, Sbita, Sbitá, Tavit, Bita, Bitá, Táborita, Táta, Ťata, Ťatá, Habitat, Nablita, Nablitá, Tableta, Tahiťan, Taoista, Thalitá, Tubista, Tánina, Nabírá, Rabíma, Rabína, Rarita, Sanita, Tapíra, Tavila, Taviti, Tavíte, Trnitá, Zabírá, Zavita, Zavitá, Zavítá, Ťápota, Faita, Arita, Hbitá, Hábit, Nabit, Nabít, Obita, Obitá, Tabla, Tajit, Tamta, Tasit, Ubita, Ubitá, Vbita, Vbitá, Vábit, Zabit, Zabít, Zbita, Zbitá, Babíka, Gábina, Kabáta, Kábrta, Labíka, Sabina, Tatiná, Tálína, Babina, Babíma, Bábina, Dobita, Dobitá, Galitá, Habite, Habitu, Habity, Habitů, Hobita, Hábite, Hábitu, Hábity, Hábitů, Kabina, Masitá, Nabila, Nabiti, Nabito, Nabitu, Nabity, Nabité, Nabitě, Nabití, Nabitý, Nabyta, Nabytá<br>
    {--*Tabito*--}: Nabito, Zabito, Ta bito, Ta-bito, Taviti, Anito, Sbito, Tavit, Bito, Tato, Tito, Táborito, Táto, Ťato, Nabitou, Nablito, Tableto, Taoisto, Tubisto, Zabitou, Tánino, Nabiti, Nabití, Nabíti, Rabího, Rarito, Sanito, Tajiti, Tasiti, Tavilo, Tavíte, Vábiti, Zabiti, Zabití, Zabíti, Zavito, Ťápoto, Arito, Hábit, Nabit, Nabít, Obito, Tablo, Tabló, Tajit, Takto, Tamto, Tasit, Ubito, Vbito, Vábit, Zabit, Zabít, Zbito, Gábino, Sabino, Babino, Babího, Bábino, Dobito, Habite, Habitu, Habity, Habitů, Hábite, Hábitu, Hábity, Hábitů, Kabino, Nabilo, Nabita, Nabitu, Nabity, Nabitá, Nabité, Nabitě, Nabitý, Nabyto, Nalito, Napito, Našito, Nebito, Orbito, Parito, Pobito, Tajilo, Tajíte, Tapeto, Tasilo, Tasíte, Těmito, Vybito, Vábilo, Vábíte, Zabilo, Zabita, Zabitu, Zabity, Zabitá, Zabité<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-04-24/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-04-24/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-04-24/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-04-24/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "white")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
