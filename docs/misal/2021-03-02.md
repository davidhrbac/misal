---
title: '2021-03-02'
---
# Úterý po 2. neděli postní

## 1. čtení – Iz 1,10.16-20

*Čtení z knihy proroka Izaiáše*

Slyšte Hospodinovo slovo, sodomská knížata, poslechněte příkaz našeho Boha, gomorský lide! Umyjte se čistě! Odstraňte své špatné skutky pryč z mých očí; přestaňte jednat zle, učte se jednat dobře: hledejte spravedlnost, přispějte utlačenému, sirotku pomozte k právu, zastaňte se vdovy. Nuže, suďme se – praví Hospodin. I kdyby jak šarlat byly vaše hříchy, vybílí se jako sníh. I kdyby se červenaly jako purpur, budou jako (bílá) vlna. Jestliže ochotně poslechnete, budete jíst z dobrodiní země. Jestliže se však vzepřete, budete-li se bouřit, pohltí vás meč! Ano, (tak) promluvila Hospodinova ústa.

## Mezizpěv – Žl 50,8-9.16bc-17.21+23

Kdo žije správně, tomu ukážu Boží spásu.

## Zpěv před evangeliem – srov. Ez 18,31

Odvrhněte od sebe všechny své zvrácenosti, praví Pán, a obnovte své srdce a svého ducha.

## Evangelium – Mt 23,1-12

*Slova svatého evangelia podle Matouše*

Ježíš mluvil k zástupům i ke svým učedníkům: „Na Mojžíšův stolec zasedli učitelé Zákona a farizeové. Dělejte a zachovávejte všechno, co vám řeknou, ale podle jejich skutků nejednejte, neboť mluví, ale nejednají. Svazují těžká a neúnosná břemena a vkládají je lidem na ramena, ale sami se jich nechtějí dotknout ani prstem. Všechny své skutky dělají jen proto, aby se ukázali před lidmi. Dávají si zhotovovat zvlášť široké modlitební řemínky a zvlášť velké střapce na šatech, mají rádi čestná místa na hostinách a přední sedadla v synagogách, mají rádi pozdravy na ulicích, a když jim lidé říkají `mistře'. Vy však si nedávejte říkat `mistr', jenom jeden je váš Mistr, a vy všichni jste bratři. A nikomu na zemi nedávejte jméno `otec', jenom jeden je váš Otec, a ten je v nebi. Ani si nedávejte říkat `učitel', jenom jeden je váš Učitel - 
Kristus. Kdo je mezi vámi největší, ať je vaším služebníkem. Kdo se povyšuje, bude ponížen, a kdo se ponižuje, bude povýšen."

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-03-02.json](/api/v1/2021-03-02.json)
    * YAML - [2021-03-02.yaml](/api/v1/2021-03-02.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-03-02.json](/api/v1/olejnik/2021-03-02.json)
    * YAML - [olejnik/2021-03-02.yaml](/api/v1/olejnik/2021-03-02.yaml)

### Pravopis

!!! failure ""
    N/A

### Typografie

!!! bug ""
    Ježíš mluvil k zástupům i ke svým učedníkům: „Na Mojžíšův stolec zasedli učitelé Zákona a farizeové. Dělejte a zachovávejte všechno, co vám řeknou, ale podle jejich skutků nejednejte, neboť mluví, ale nejednají. Svazují těžká a neúnosná břemena a vkládají je lidem na ramena, ale sami se jich nechtějí dotknout ani prstem. Všechny své skutky dělají jen proto, aby se ukázali před lidmi. Dávají si zhotovovat zvlášť široké modlitební řemínky a zvlášť velké střapce na šatech, mají rádi čestná místa na hostinách a přední sedadla v synagogách, mají rádi pozdravy na ulicích, a když jim lidé říkají {--*\`*--}mistře'. Vy však si nedávejte říkat {--*\`*--}mistr', jenom jeden je váš Mistr, a vy všichni jste bratři. A nikomu na zemi nedávejte jméno {--*\`*--}otec', jenom jeden je váš Otec, a ten je v nebi. Ani si nedávejte říkat {--*\`*--}učitel', jenom jeden je váš Učitel - 

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-03-02/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-03-02/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-03-02/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-03-02/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "deep-purple")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
