---
title: '2021-05-26'
---














## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-05-26.json](/api/v1/2021-05-26.json)
    * YAML - [2021-05-26.yaml](/api/v1/2021-05-26.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-05-26.json](/api/v1/olejnik/2021-05-26.json)
    * YAML - [olejnik/2021-05-26.yaml](/api/v1/olejnik/2021-05-26.yaml)

### Pravopis

!!! failure ""
    N/A

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [olejnik/mezizpev.html](/api/v1/obs/2021-05-26/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
