---
title: '2021-06-10'
---
# Čtvrtek po 10. neděli v mezidobí

## 1. čtení – 2 Kor 3,15 - 4,1.3-6

*Čtení z druhého listu svatého apoštola Pavla Korinťanům*

(Bratři!) Dodnes leží rouška na srdci Izraelitů, kdykoli se předčítá Mojžíš. Ale až se obrátí k Pánu, rouška bude odstraněna. Ten Pán je však duch; a kde je duch Páně, tam je svoboda. My všichni s nezakrytou tváří odrážíme jako v zrcadle velebnost Páně, a tak se přetvořujeme stále víc a víc k zářivé podobě, jakou má on. (Působí to) duch Páně. Když jsme pověřeni tou službou, nenecháváme se ovládnout malomyslností, protože nám Bůh milosrdně pomáhá. A je-li přesto naše evangelium nesrozumitelné, pak jen těm, kteří jdou k záhubě. Nevěří, neboť bůh tohoto světa je zaslepil, takže nevidí jasně světlo evangelia o božské slávě Krista, Božího obrazu. Vždyť přece nehlásáme sebe, ale (kážeme, že) Ježíš Kristus je Pán, my však že jsme vaši služebníci kvůli Ježíši. Neboť Bůh, který řekl: 'Ať ze tmy zazáří světlo!', zazářil i v našem srdci, aby osvítil (lidi) poznáním Boží velebnosti, která je na Kristově tváři.

## Mezizpěv – Žl 85,9ab+10.11-12.13-14

Velebnost Páně bude sídlit v naší zemi.

## Zpěv před evangeliem – Jan 13,34

Aleluja. Ježíši Kriste, ty jsi svědek hodný víry, prvorozený z mrtvých, miloval jsi nás a obmyl svou krví od našich hříchů. Aleluja.

## Evangelium – Mt 5,20-26

*Slova svatého evangelia podle Matouše*

Ježíš řekl svým učedníkům: "Nebude-li vaše spravedlnost mnohem dokonalejší než spravedlnost učitelů Zákona a farizeů, do nebeského království nevejdete. Slyšeli jste, že bylo řečeno předkům: 'Nezabiješ.' Kdo by zabil, propadne soudu. Ale já vám říkám: Každý, kdo se na svého bratra hněvá, propadne soudu; kdo svého bratra tupí, propadne veleradě; a kdo ho zatracuje, propadne pekelnému ohni.Přinášíš-li tedy svůj dar k oltáři a tam si vzpomeneš, že tvůj bratr má něco proti tobě, nech tam svůj dar před oltářem a jdi se napřed smířit se svým bratrem, teprve potom přijď a obětuj svůj dar. Dohodni se rychle se svým protivníkem, dokud jsi s ním na cestě, aby tě tvůj protivník neodevzdal soudci a soudce služebníkovi, a byl bys uvržen do žaláře. Amen, pravím ti: Nevyjdeš odtamtud, dokud nezaplatíš do posledního halíře."

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-06-10.json](/api/v1/2021-06-10.json)
    * YAML - [2021-06-10.yaml](/api/v1/2021-06-10.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-06-10.json](/api/v1/olejnik/2021-06-10.json)
    * YAML - [olejnik/2021-06-10.yaml](/api/v1/olejnik/2021-06-10.yaml)

### Pravopis

!!! failure ""
    {--*přetvořujeme*--}: přetvarujeme, přetvařujeme, předtvarujeme, přetvarujme, přetvařujme, převařujeme, přetvarujete, přetvařujete, znetvořujeme<br>

### Typografie

!!! bug ""
    Ježíš řekl svým učedníkům: "Nebude-li vaše spravedlnost mnohem dokonalejší než spravedlnost učitelů Zákona a farizeů, do nebeského království nevejdete. Slyšeli jste, že bylo řečeno předkům: 'Nezabiješ.' Kdo by zabil, propadne soudu. Ale já vám říkám: Každý, kdo se na svého bratra hněvá, propadne soudu; kdo svého bratra tupí, propadne veleradě; a kdo ho zatracuje, propadne pekelnému ohn{--*i.P*--}řinášíš-li tedy svůj dar k oltáři a tam si vzpomeneš, že tvůj bratr má něco proti tobě, nech tam svůj dar před oltářem a jdi se napřed smířit se svým bratrem, teprve potom přijď a obětuj svůj dar. Dohodni se rychle se svým protivníkem, dokud jsi s ním na cestě, aby tě tvůj protivník neodevzdal soudci a soudce služebníkovi, a byl bys uvržen do žaláře. Amen, pravím ti: Nevyjdeš odtamtud, dokud nezaplatíš do posledního halíře."

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-06-10/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-06-10/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-06-10/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-06-10/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "green")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
