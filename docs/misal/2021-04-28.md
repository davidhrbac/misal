---
title: '2021-04-28'
---
# Středa po 4. neděli velikonoční

## 1. čtení – Sk 12,24; 13,5a

*Čtení ze Skutků apoštolů*

Boží slovo se šířilo a rozrůstalo. Když Barnabáš a Šavel vyplnili svůj úkol, vrátili se z Jeruzaléma a vzali s sebou Jana, kterému říkali Marek. V antiošské církevní obci byli muži, kteří měli dar promlouvat z vnuknutí a učit: Barnabáš a Šimon, kterému říkali Niger, Lucius z Kyrény a Manahen, který byl vychován spolu s údělným knížetem Herodem, a Šavel. Když konali bohoslužbu Pánu a postili se, řekl Duch Svatý: „Oddělte mi Barnabáše a Šavla pro dílo, ke kterému jsem je povolal!“ Postili se tedy a modlili, potom na ně vložili ruce a propustili je. Tak oni, vysláni od Ducha Svatého, přišli do Seleukie. Odtamtud odjeli lodí na Kypr. V Salamině vystoupili a kázali Boží slovo v židovských synagogách.

## Mezizpěv – Žl 67,2-3.5.6+8

Ať tě, Bože, velebí národy, ať tě velebí kdekterý národ!

## Zpěv před evangeliem – Jan 8,12

Aleluja. Já jsem světlo světa, praví Pán, kdo mě následuje, bude mít světlo života. Aleluja.

## Evangelium – Jan 12,44-50

*Slova svatého evangelia podle Jana*

Ježíš zvolal hlasitě: „Kdo věří ve mne, věří ne ve mne, ale v toho, který mě poslal; a kdo vidí mne, vidí toho, který mě poslal. Já jsem přišel na svět jako světlo, aby žádný, kdo věří ve mne, nezůstal v temnotě. Kdo moje slova poslouchá, ale nezachovává, toho já nesoudím; vždyť jsem nepřišel, abych svět soudil, ale abych svět spasil. Kdo mnou pohrdá a moje slova nepřijímá, má svého soudce: to slovo, které jsem hlásal, bude ho soudit v poslední den. Neboť já jsem nemluvil sám ze sebe, ale Otec, který mě poslal, ten mi dal příkaz, co mám říkat a co hlásat. A já vím, že jeho příkaz je věčný život. Co tedy já mluvím, mluvím tak, jak mi to pověděl Otec.“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-04-28.json](/api/v1/2021-04-28.json)
    * YAML - [2021-04-28.yaml](/api/v1/2021-04-28.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-04-28.json](/api/v1/olejnik/2021-04-28.json)
    * YAML - [olejnik/2021-04-28.yaml](/api/v1/olejnik/2021-04-28.yaml)

### Pravopis

!!! failure ""
    {--*antiošské*--}: antilské<br>
    {--*Barnabáš*--}: Barnatá<br>
    {--*Herodem*--}: Heroldem, Herolde, Hrdém, Rodem, Herdě, Hodem, Neroďme, Hebronem, Heroutem, Herzogem, Hésiodem, Bezrodém, Heroinem, Heroldům, Nerodném, Jeromem, Jerovém, Herdům, Hermem, Hronem, Brodem, Exodem, Hercem, Herdám, Hexodě, Hradem, Hrobem, Hromem, Hrotem, Hřadem, Nerodě, Zrodem, Přerodem, Heranem, Hertzem, Herákem, Neradem, Cerovém, Děrovém, Férovém, Hexodám, Hororem, Keřovém, Marodem, Měrovém, Nerodím, Nerudém, Neřádem, Národem, Peronem, Perovém, Perónem, Peřovém, Porodem, Pérovém, Péřovém, Sérovém, Terorem, Térovém, Xeroxem, Zerovém<br>
    {--*Kyrény*--}: Kypřeny, Kypřený, Křeny, Hýřeny, Hýřený, Kořeny, Kypěny, Kypěný, Kýžený<br>
    {--*Lucius*--}: Licous, Loučíš, Lučinu, Lučišť, Lucii, Lucií, Luxus, Lícíš, Líčíš, Učíš, Hlučíš, Klučíš, Nuncius, Luciin, Luciím, Lučina, Lucie, Lucku, Bučíš, Fučíš, Hučíš, Lučin, Léčíš, Mučíš, Pučíš, Ručíš, Čučíš, Julius, Lucích, Lučino, Lučiny, Lučině<br>
    {--*Manahen*--}: Manager, Nanášen, Manche, Machen, Maňásek, Manažer, Manažér, Zanášen<br>
    {--*Salamině*--}: Kalamíne, Šalamoune, Alanine, Slanině, Saláme, Sálané, Sálaně, Sáláme, Šálami, Kalamitně, Kalamínem, Salámové, Aladine, Slavíne, Slavíně, Kalamín, Slabině, Slaměné, Slaměně, Slatině, Sazomíne, Sazomíně, Kalamitě, Kalamínu, Kalamíny, Kalamínů, Nalaminl, Nalámané, Nalámaně, Palatine, Sálajíce<br>
    {--*Seleukie*--}: Selekce<br>
    {--*spravedlivěa*--}: spravedlivá, spravedlivé, spravedlivě, spravedlivém<br>
    {--*Šavel*--}: Šavle, Stavěl, Šťavel, Sábel, Savek, Savé, Savě, Svěl, Havel, Pavel, Navěl, Savec, Savém, Skvěl, Sádel, Sázel, Zavel, Zavěl, Šavll, Ševel<br>
    {--*Šavla*--}: Salva, Svála, Sábla, Savka, Šábla, Sáva, Savá, Sála, Sálá, Šála, Havla, Pavla, Saxla, Sádla, Mávla, Sáhla, Sákla, Šavle, Šavli, Šavll, Šavlí<br>

### Typografie

!!! bug ""
    Bože, buď milostiv a žehnej ná{--*m,u[m[Kkaž nám svou jasnou tvá[01;31m[Kř,k[m[Kéž se pozná na zemi, jak jedná[01;31m[Kš,k*--}éž poznají všechny národy, jak zachraňuješ.
    Nechť se lidé radují a jásaj{--*í,ž*--}e soudíš národy spravedlivěa lidi na zemi řídíš.
    Ať tě, Bože, velebí národ{--*y,a[m[Kť tě velebí kdekterý náro[01;31m[Kd.K[m[Kéž nám Bůh žehn[01;31m[Ká,a*--}ť ho ctí všechny končiny země!

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-04-28/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-04-28/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-04-28/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-04-28/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "white")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
