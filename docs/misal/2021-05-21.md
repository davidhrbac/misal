---
title: '2021-05-21'
---
# Pátek po 7. neděli velikonoční

## 1. čtení – Sk 25,13b-21

*Čtení ze Skutků apoštolů*

Král Agrippa a Bereniké zavítali do Césareje; přišli pozdravit Festa a zdrželi se tam více dní. Festus přitom předložil králi Pavlův případ: „Je tady ve vazbě ještě z Felixovy doby jeden vězeň. Když jsem byl v Jeruzalémě, velekněží a židovští starší podali na něho žalobu a žádali, aby byl odsouzen. Odpověděl jsem jim, že Římané nemají ve zvyku, aby vydali nějakého člověka dříve, než by obžalovaný měl svoje žalobce před sebou a než by se mu dostalo příležitosti hájit se proti obvinění. Když sem tedy přišli, hned druhý den jsem bez odkladu zasedl k soudu a nařídil toho muže předvést. Žalobci se sice postavili proti němu, ale nekladli mu za vinu žádnou ze špatností, které jsem předpokládal. Měli proti němu jen nějaké sporné otázky o jejich náboženství a o nějakém mrtvém Ježíšovi, o kterém však Pavel tvrdil, že žije. Já jsem byl na rozpacích, co s těmi spornými otázkami. Proto jsem se ho zeptal, zdali by nechtěl jít do Jeruzaléma a tam se dát soudit v tomto sporu. Ale Pavel podal odvolání, aby byl ponechán ve vazbě, dokud by o něm nerozhodl císař. Proto jsem nařídil, aby zůstal ve vězení, až ho budu moci poslat k císaři.“

## Mezizpěv – Žl 103,1-2.11-12.19-20ab

Hospodin si zřídil na nebi trůn.

## Zpěv před evangeliem – Jan 14,26

Aleluja. Duch Svatý vás naučí všemu a připomene vám všechno, co jsem vám řekl. Aleluja.

## Evangelium – Jan 21,15-19

*Slova svatého evangelia podle Jana*

Když (se Ježíš ukázal svým učedníkům a) posnídali, zeptal se (Ježíš) Šimona Petra: „Šimone, (synu) Janův, miluješ mě více než ti zde?“ Odpověděl mu: „Ano, Pane, ty víš, že tě miluji.“ Ježíš mu řekl: „Pas mé beránky.“ Podruhé se ho zeptal: „Šimone, (synu) Janův, miluješ mě?“ Odpověděl mu: „Ano, Pane, ty víš, že tě miluji.“ Ježíš mu řekl: „Pas moje ovce.“ Zeptal se ho potřetí: „Šimone, (synu) Janův, miluješ mě?“ Petr se zarmoutil, že se ho potřetí zeptal: „Miluješ mě?“, a odpověděl mu: „Pane, ty víš všechno
ty víš, že tě miluji!“ Ježíš mu řekl: „Pas moje ovce! Amen, amen, pravím ti: Dokud jsi byl mladší, sám ses přepásával a chodils, kam jsi chtěl. Ale až zestárneš, vztáhneš ruce, a jiný tě přepásá a povede, kam nechceš.“ To řekl, aby naznačil, jakou smrtí oslaví Boha. A po těch slovech ho vyzval: „Následuj mě!“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-05-21.json](/api/v1/2021-05-21.json)
    * YAML - [2021-05-21.yaml](/api/v1/2021-05-21.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-05-21.json](/api/v1/olejnik/2021-05-21.json)
    * YAML - [olejnik/2021-05-21.yaml](/api/v1/olejnik/2021-05-21.yaml)

### Pravopis

!!! failure ""
    {--*Agrippa*--}: Grappa<br>
    {--*Bereniké*--}: Buřenické, Bernské, Buřenice, Kerenské, Beranice, Beranské, Beraníce, Beraníme, Beraníte, Bezenské, Perenské, Čerenské<br>
    {--*Césareje*--}: Česaje, Nesvářeje, Česávejme, Česávejte, Česávej, Nesázeje, Česaněji, Česávaje<br>
    {--*Festa*--}: Gesta, Bešta, Fejta, Feřta, Pešta, Cesta, Fešná, Města, Testa, Těsta, Vesta, Šestá, Fe sta, Fe-sta<br>
    {--*Festus*--}: Dešťů, Fetiš, Gestu, Ešus, Dešťům, Gestům, Leštíš, Městys, Nestuď, Pěstíš, Věstíš, Věštíš, Beštu, Beštů, Fejtu, Fejtů, Feřtu, Feřtů, Peštu, Peštů, Cestu, Fetuj, Městu, Nesuš, Netuš, Restu, Restů, Retuš, Testu, Testů, Těstu, Vestu, Žesťů, Beštům, Beštův, Faltus, Fejtům, Fejtův, Feřtům, Feřtův, Peštům, Peštův, Cestuj, Fiškus, Městům, Nesluš, Nestul, Nestůj, Pěstuj, Restuj, Restům, Sestup, Testuj, Testům, Těstům, Žesťům<br>
    {--*chodils*--}: chodila, chodil, chodíš, chodili, chodilo, chodily<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-05-21/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-05-21/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-05-21/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-05-21/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "white")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
