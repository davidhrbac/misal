---
title: '2021-03-03'
---
# Středa po 2. neděli postní

## 1. čtení – Jer 18,18-20

*Čtení z knihy proroka Jeremiáše*

Řekli (moji nepřátelé): „Pojďme a osnujme plán proti Jeremiášovi! Přece kněží nebudou bez rady, mudrci bez důvtipu ani proroci bez řečí! Pojďme a ubijme ho vlastním jeho jazykem, číhejme na všechna jeho slova!“ Všimni si mě, Hospodine, a slyš hlas mých protivníků! Má se snad odplácet za dobro zlem? Vždyť mi kopají jámu! Vzpomeň, že jsem před tebou stával, abych se za ně přimlouval, abych od nich odvrátil tvůj hněv!

## Mezizpěv – Žl 31,5-6.14.15-16

Zachraň mě, Hospodine, svou slitovností.

## Zpěv před evangeliem – Jan 8,12

Já jsem světlo světa, praví Pán, kdo mě následuje, bude mít světlo života.

## Evangelium – Mt 20,17-28

*Slova svatého evangelia podle Matouše*

Když se Ježíš vydal na cestu do Jeruzaléma, vzal si stranou dvanáct učedníků a na cestě jim řekl: „Teď jdeme do Jeruzaléma. Tam Syn člověka bude vydán velekněžím a učitelům Zákona. Odsoudí ho k smrti a vydají pohanům, aby se mu posmívali, zbičovali a ukřižovali ho, ale třetího dne bude vzkříšen.“ Tu k němu přistoupila matka Zebedeových synů se svými syny a padla mu k nohám, aby ho o něco požádala. Zeptal se jí: „Co si přeješ?“ Odpověděla mu: „Poruč, aby tito dva moji synové zasedli jeden po tvé pravici a druhý po tvé levici v tvém království.“ Ježíš na to řekl: „Nevíte, oč žádáte. Můžete pít kalich, který já budu pít?“ Odpověděli: „Můžeme.“ Řekl jim: „Můj kalich sice pít budete, ale udělovat místa po mé pravici a levici není má věc, ta místa patří těm, kterým jsou připravena od mého Otce.“ Když to uslyšelo ostatních deset, rozmrzeli se na ty dva bratry. Ježíš si je však zavolal a řekl: „Víte, že panovníci tvrdě vládnou nad národy a velmoži jim dávají cítit svou moc. Mezi vámi však tomu tak nebude. Ale kdo by chtěl být mezi vámi veliký, ať je vaším služebníkem, a kdo by chtěl být mezi vámi první, ať je vaším otrokem. Vždyť ani Syn člověka nepřišel, aby si nechal sloužit, ale aby sloužil a dal svůj život jako výkupné za všechny.“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-03-03.json](/api/v1/2021-03-03.json)
    * YAML - [2021-03-03.yaml](/api/v1/2021-03-03.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-03-03.json](/api/v1/olejnik/2021-03-03.json)
    * YAML - [olejnik/2021-03-03.yaml](/api/v1/olejnik/2021-03-03.yaml)

### Pravopis

!!! failure ""
    {--*Zebedeových*--}: Lebedových<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-03-03/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-03-03/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-03-03/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-03-03/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "deep-purple")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
