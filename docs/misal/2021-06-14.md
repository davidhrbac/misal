---
title: '2021-06-14'
---
# Pondělí po 11. neděli v mezidobí

## 1. čtení – 2 Kor 6,1-10

*Čtení z druhého listu svatého apoštola Pavla Korinťanům*

(Bratři!) Jako (Boží) spolupracovníci vás napomínáme, abyste nepřijali milost Boží nadarmo! (Bůh) přece říká: `V době příhodné jsem tě vyslyšel, v den spásy jsem ti pomohl.' Hle, teď je ta "doba příhodná", hle, teď je ten "den spásy"! Nikomu v ničem nedáváme pohoršení, aby nebyla potupena (naše) služba, ale ve všem se prokazujeme jako Boží služebníci všestrannou vytrvalostí, v souženích, v nesnázích, v úzkostech, v ranách, v žalářích, v nepokojích, v námahách, ve bděních, v postech; čistotou, poznáním, shovívavostí, dobrotivostí, Duchem Svatým, upřímnou láskou, slovem pravdy, silou Boží, zbrojí spravedlnosti v útoku i v obraně; za pocty i potupy, při zlé i dobré pověsti. Že prý jsme svůdcové, a přece jsme pravdiví; že prý neznámí, a zatím jsme dobře známí; jako zmírající, a hle - žijeme; jako trestu propadlí, a přece na smrt vydáni nejsme; prý jsme smutní, a zatím se stále radujeme, jakoby chudáci, a přece mnohé obohacujeme, jako bychom nic neměli, a zatím máme všechno.

## Mezizpěv – Žl 98,1.2-3ab.3cd-4

Hospodin uvedl ve známost svou spásu.

## Zpěv před evangeliem – Žl 119,105

Aleluja. Svítilnou mým nohám je tvé slovo a světlem mé stezce. Aleluja.

## Evangelium – Mt 5,38-42

*Slova svatého evangelia podle Matouše*

Ježíš řekl svým učedníkům: "Slyšeli jste, že bylo řečeno: `Oko za oko a zub za zub.' Ale já vám říkám: Neodporujte zlému. Spíše naopak: Když tě někdo udeří na pravou tvář, nastav mu i druhou; a tomu, kdo se chce s tebou soudit a vzít tvé šaty, (tomu) nech i plášť; a když tě někdo nutí, abys ho doprovázel jednu míli, jdi s ním dvě. Tomu, kdo tě prosí, dej a od toho, kdo si chce od tebe vypůjčit, se neodvracej."

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-06-14.json](/api/v1/2021-06-14.json)
    * YAML - [2021-06-14.yaml](/api/v1/2021-06-14.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-06-14.json](/api/v1/olejnik/2021-06-14.json)
    * YAML - [olejnik/2021-06-14.yaml](/api/v1/olejnik/2021-06-14.yaml)

### Pravopis

!!! failure ""
    {--*hrejte*--}: hřejte, hrkejte, hárejte, hřejete, hřmějte, hřeje, ohřejte, uhřejte, hrajte, hřejme, hřešte, orejte, prejte, přejte<br>
    {--*Izraelovu*--}: Izraelcovu, Izraelcovou, Izraelcovi, Izraelcovy, Izraelkou, Izraelců, Ožralovu, Izraelcův, Izraelů, Izraelitovu, Izraelcova, Izraelcovo, Izraelcově, Izraelitu, Izraelitů, Izraelku<br>

### Typografie

!!! bug ""
    (Bratři!) Jako (Boží) spolupracovníci vás napomínáme, abyste nepřijali milost Boží nadarmo! (Bůh) přece říká: {--*\`*--}V době příhodné jsem tě vyslyšel, v den spásy jsem ti pomohl.' Hle, teď je ta "doba příhodná", hle, teď je ten "den spásy"! Nikomu v ničem nedáváme pohoršení, aby nebyla potupena (naše) služba, ale ve všem se prokazujeme jako Boží služebníci všestrannou vytrvalostí, v souženích, v nesnázích, v úzkostech, v ranách, v žalářích, v nepokojích, v námahách, ve bděních, v postech; čistotou, poznáním, shovívavostí, dobrotivostí, Duchem Svatým, upřímnou láskou, slovem pravdy, silou Boží, zbrojí spravedlnosti v útoku i v obraně; za pocty i potupy, při zlé i dobré pověsti. Že prý jsme svůdcové, a přece jsme pravdiví; že prý neznámí, a zatím jsme dobře známí; jako zmírající, a hle - žijeme; jako trestu propadlí, a přece na smrt vydáni nejsme; prý jsme smutní, a zatím se stále radujeme, jakoby chudáci, a přece mnohé obohacujeme, jako bychom nic neměli, a zatím máme všechno.
    Ježíš řekl svým učedníkům: "Slyšeli jste, že bylo řečeno: {--*\`*--}Oko za oko a zub za zub.' Ale já vám říkám: Neodporujte zlému. Spíše naopak: Když tě někdo udeří na pravou tvář, nastav mu i druhou; a tomu, kdo se chce s tebou soudit a vzít tvé šaty, (tomu) nech i plášť; a když tě někdo nutí, abys ho doprovázel jednu míli, jdi s ním dvě. Tomu, kdo tě prosí, dej a od toho, kdo si chce od tebe vypůjčit, se neodvracej."

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-06-14/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-06-14/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-06-14/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-06-14/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "green")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
