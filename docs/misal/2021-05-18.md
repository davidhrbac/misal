---
title: '2021-05-18'
---
# Úterý po 7. neděli velikonoční

## 1. čtení – Sk 20,17-27

*Čtení ze Skutků apoštolů*

Pavel poslal z Miléta do Efesu a povolal k sobě starší z církevní obce. Když k němu přišli, řekl jim: „Vy víte, jak jsem si u vás počínal od prvního dne, kdy jsem vstoupil do Asie, i po celý ostatní čas. Sloužil jsem Pánu se vší pokorou, se slzami a ve zkouškách, které mě potkaly pro úklady židů. Neopomenul jsem nic z toho, co by vám bylo k užitku, ale hlásal jsem vám to a učil vás tomu veřejně i po domech. Vážně jsem kladl na srdce židům i pohanům, aby se obrátili k Bohu s vírou v našeho Pána Ježíše. A nyní mě to uvnitř všechno nutí k tomu, abych šel do Jeruzaléma. Co mě tam potká, nevím. Jen od města k městu mi Duch Svatý dosvědčuje a říká, že mě čekají pouta a soužení. Ale můj život nemá vůbec pro mě cenu, jen když dokončím svůj běh a úkol, který jsem přijal od Pána Ježíše: vydávat svědectví pro evangelium o Boží milosti. Konal jsem mezi vámi cesty a hlásal království (Boží). A nyní
já vím, že nikdo z vás mou tvář už nespatří. Slavnostně vám tedy dnes prohlašuji: Zahyne-li někdo, já na tom vinu nemám! Nic jsem nezameškal, ale zvěstoval jsem vám všechno, co rozhodl Bůh.“

## Mezizpěv – Žl 68,10-11.20-21

Pozemské říše, zpívejte Bohu.

## Zpěv před evangeliem – srov. Jan 14,16

Aleluja. Otec vám dá jiného Přímluvce, aby s vámi zůstal navždy. Aleluja.

## Evangelium – Jan 17,1-11a

*Slova svatého evangelia podle Jana*

Ježíš pozvedl oči k nebi a modlil se: „Otče, přišla ta hodina. Oslav svého Syna, aby Syn oslavil tebe. Obdařils ho mocí nade všemi lidmi, aby dal věčný život všem, které jsi mu dal. Věčný život pak je to, že poznají tebe, jediného pravého Boha, a toho, kterého jsi poslal, Ježíše Krista. Já jsem tě oslavil na zemi: dokončil jsem dílo, které jsi mi svěřil, abych ho vykonal. Nyní oslav ty mne u sebe, Otče, slávou, kterou jsem měl u tebe, dříve než byl svět. Zjevil jsem tvé jméno lidem, které jsi mi dal ze světa. Byli tvoji a mně jsi je dal a zachovali tvoje slovo. Nyní poznali, že všechno, cos mi dal, je od tebe; vždyť slova, která jsi dal mně, dal jsem jim. Oni je přijali a skutečně poznali, že jsem vyšel od tebe, a uvěřili, že jsi mě poslal. Já prosím za ně. Neprosím za svět, ale za ty, které jsi mi dal, vždyť jsou tvoji; a všechno mé je tvé a všechno tvé je mé. V nich jsem oslaven. Už nejsem na světě, ale oni jsou na světě; a já jdu k tobě.“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-05-18.json](/api/v1/2021-05-18.json)
    * YAML - [2021-05-18.yaml](/api/v1/2021-05-18.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-05-18.json](/api/v1/olejnik/2021-05-18.json)
    * YAML - [olejnik/2021-05-18.yaml](/api/v1/olejnik/2021-05-18.yaml)

### Pravopis

!!! failure ""
    {--*Miléta*--}: Miléťan, Muleta, Mleta, Mletá, Milena, Milota, Milená, Mineta, Mi léta, Mi-léta, Milé ta, Milé-ta<br>
    {--*Obdařils*--}: Obdařila, Obdařil, Obdaříš, Obdařili, Obdařilo, Obdařily<br>
    {--*stádce*--}: Stadice, Stádlce, stače, stádě, stáče, sádce, štace, Starce, Starče, Stánce, Stázce, sladce, starce, starče, stydce, stádně, stávce<br>
    {--*vzkřísils*--}: vzkřísila, vzkřísil, vzkřísíš, vzkřísili, vzkřísilo, vzkřísily<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-05-18/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-05-18/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-05-18/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-05-18/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "white")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
