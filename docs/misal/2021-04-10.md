---
title: '2021-04-10'
---
# Sobota v oktávu velikonočním

## 1. čtení – Sk 4,13-21

*Čtení ze Skutků apoštolů*

(Když přední muži v lidu, starší a učitelé Zákona) viděli Petrovu a Janovu odvahu a uvážili, že jsou to lidé neučení a prostí, divili se. Poznávali je, že bývali s Ježíšem; ale protože teď zároveň s nimi viděli toho uzdraveného člověka, neměli, co by proti tomu mohli říci. Poručili jim proto, aby vyšli z velerady, a radili se mezi sebou: „Co máme dělat s těmito lidmi? Že se skrze ně stal zřejmý zázrak, je zjevné všem obyvatelům Jeruzaléma, a my to nemůžeme popřít. Aby se to však nerozneslo mezi lidem ještě více, pohrozíme jim, že v tom jménu již nesmějí k nikomu mluvit.“ Zavolali je tedy a přikázali jim, že vůbec ve jménu Ježíšově nesmějí kázat ani učit. Ale Petr a Jan jim na to řekli: „Suďte sami, zda je to před Bohem správné, abychom poslouchali více vás než Boha. Je přece nemožné, abychom nemluvili o tom, co jsme viděli a slyšeli.“ Oni jim znova pohrozili a propustili je, protože nemohli najít nic, zač by je potrestali – už kvůli lidu, neboť všichni velebili Boha za to, co se událo.

## Mezizpěv – Žl 118,1+14-15.16ab-18.19-21

Děkuji ti, Hospodine, žes mě vyslyšel.

## Zpěv před evangeliem – Žl 118,24

Aleluja. Toto je den, který učinil Hospodin, jásejme a radujme se z něho. Aleluja.

## Evangelium – Mk 16,9-15

*Slova svatého evangelia podle Marka*

Ježíš po svém zmrtvýchvstání ráno první den v týdnu se zjevil nejdříve Marii Magdalské, z které kdysi vyhnal sedm zlých duchů. Ona šla a oznámila to těm, kdo bývali s ním, ale teď truchlili a plakali. Ti, když slyšeli, že on žije a že ho viděla, tomu nevěřili. Potom se v jiné podobě zjevil dvěma z nich na cestě, když šli na venkov. Ti se vrátili a oznámili to ostatním, ale ani jim nevěřili. Konečně ze zjevil i jedenácti (apoštolům), když byli právě u stolu, a káral je pro jejich nevěru a tvrdost srdce, že nevěřili těm, kteří ho spatřili vzkříšeného. A řekl jim: „Jděte do celého světa a hlásejte evangelium všemu tvorstvu!“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-04-10.json](/api/v1/2021-04-10.json)
    * YAML - [2021-04-10.yaml](/api/v1/2021-04-10.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-04-10.json](/api/v1/olejnik/2021-04-10.json)
    * YAML - [olejnik/2021-04-10.yaml](/api/v1/olejnik/2021-04-10.yaml)

### Pravopis

!!! failure ""
    {--*Magdalské*--}: Maršálské, Maďarské, Magdaleně, Magdaléně, Bagdádské, Magnátské, Vandalské<br>
    {--*Otevřte*--}: Otevřete, Otevře, Otevřle<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-04-10/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-04-10/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-04-10/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-04-10/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "white")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
