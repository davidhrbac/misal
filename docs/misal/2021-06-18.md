---
title: '2021-06-18'
---
# Pátek po 11. neděli v mezidobí

## 1. čtení – 2 Kor 11,18.21b-30

*Čtení z druhého listu svatého apoštola Pavla Korinťanům*

(Bratři!) Když se mnozí chlubí věcmi jen lidskými, pochlubím se také. Ať už si kdo zakládá na čemkoli - to mluvím jako nerozumný - na tom si mohu zakládat i já. Jsou Hebreové? Já také! Jsou Izraelité? Já také! Jsou Abrahámovi potomci? Já také! Jsou Kristovi služebníci? To mluvím už úplně bez rozumu: Já více než oni! Lopotil jsem se mnohem více, do žaláře jsem se dostal častěji, týrání jsem zakusil nad veškerou míru, v nebezpečí smrti jsem se octl častokrát. Pětkrát jsem od židů dostal čtyřicet ran bez jedné, třikrát jsem byl bit pruty, jednou kamenován, třikrát jsem ztroskotal na lodi, den a noc jsem se zmítal na širém moři. Často na cestách, v nebezpečích na řekách, v nebezpečích od lupičů, v nebezpečích od vlastního národa, v nebezpečích od pohanů, v nebezpečích v městě, v nebezpečích v neobydlených krajích, v nebezpečích na moři, v nebezpečích mezi falešnými bratry. K tomu vyčerpávající práce, mnohé bezesné noci, hlad 
a žízeň, časté posty, zima a chatrné oblečení. Kromě toho každodenní nával ke mně a starost o všechny církevní obce. Kdo cítí slabost, abych ji necítil i já? Kdo je sváděn ke hříchu, aby to nepálilo i mne? Když už se musím chlubit, chci se chlubit svou slabostí.

## Mezizpěv – Žl 34,2-3.4-5.6-7

Hospodin vysvobodil spravedlivé z každé jejich tísně.

## Zpěv před evangeliem – Mt 5,3

Aleluja. Blahoslavení chudí v duchu, neboť jejich je nebeské království. Aleluja.

## 

null

null

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-06-18.json](/api/v1/2021-06-18.json)
    * YAML - [2021-06-18.yaml](/api/v1/2021-06-18.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-06-18.json](/api/v1/olejnik/2021-06-18.json)
    * YAML - [olejnik/2021-06-18.yaml](/api/v1/olejnik/2021-06-18.yaml)

### Pravopis

!!! failure ""
    {--*Hebreové*--}: Herbenové, Herbenově, Hebrejcově, Hegerové, Hegerově, Huberové, Huberově, Weberové, Weberově, Nebřezové, Nebřezově, Šebrleové, Šebrleově, Hebrejce, Hebrejče, Henryové, Hebrone, Herdové, Herdově, Hermové, Vebrové, Vebrově, Vébrové, Vébrově, Webrové, Webrově, Habrové, Habrově, Hercově, Hobrové, Šebrove, Šebrově, Žebrové, Žebrově, Heardově, Heineově, Hedgeové, Hedgeově, Šebrlové<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-06-18/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-06-18/mezizpev.html)
    * [evangelium-mt-6-19-23.html](/api/v1/obs/2021-06-18/evangelium-mt-6-19-23.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-06-18/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "green")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
