---
title: '2021-05-23'
---
# Slavnost Seslání Ducha Svatého – V den slavnosti

## 1. čtení – Sk 2,1-11

*Čtení ze Skutků apoštolů*

Nastal den letnic a všichni byli společně pohromadě. Najednou se ozval z nebe hukot, jako když se přižene silný vítr, a naplnil celý dům, kde se zdržovali. A ukázaly se jim jazyky jako z ohně, rozdělily se a nad každým z nich se usadil jeden. Všichni byli naplněni Duchem Svatým a začali mluvit cizími jazyky, jak jim Duch vnukal, aby promlouvali. V Jeruzalémě bydleli zbožní židé ze všech možných národů pod nebem. Když se ten zvuk ozval, hodně lidí se sběhlo a byli ohromeni, protože každý z nich je slyšel, jak mluví jeho vlastní řečí. Žasli, divili se a říkali: „Ti, co tak mluví, nejsou to všichni Galilejci? Jak to tedy, že každý z nás slyší svou mateřštinu? My Parthové, Médové, Elamité, obyvatelé Mezopotámie, Judska a Kappadokie, Pontu a Asie, Frýgie a Pamfýlie, Egypta a lybijského kraje u Kyrény, my, kteří jsme připutovali z Říma, židé i proselyté, Kréťané i Arabové: slyšíme, jak našimi jazyky hlásají velké Boží skutky.“

## Mezizpěv – Žl 104,1ab+24ac.29bc-30.31+34

Sešli svého ducha, Hospodine, a obnov tvář země!

## 2. čtení – Gal 5,16-25

*Čtení z listu svatého apoštola Pavla Galaťanům*

(Bratři!) Žijte duchovně, a nepropadnete žádostem těla. Tělo totiž touží proti duchu, a duch zase proti tělu. Mezi nimi je vzájemný odpor, takže neděláte, co byste chtěli. Jestliže se však necháváte vést Duchem, nejste už pod Zákonem. K jakým skutkům vede tělo, je všeobecně známo. Je to: smilstvo, nečistota, chlípnost, modloslužba, čarodějnictví, nepřátelství, sváry, žárlivost, hněvy, ctižádost, nesvornost, stranictví, závist, opilství, hýření a jiné takové věci. Řekl jsem vám to už dříve a říkám to (ještě jednou): lidé, kteří takovéto věci dělají, nebudou mít podíl v Božím království. Ale ovocem Ducha je láska, radost, pokoj, shovívavost, vlídnost, dobrota, věrnost, tichost, zdrženlivost. Proti takovým věcem se nestaví žádný zákon. Ti, kdo (náležejí) Kristu Ježíši, ukřižovali svoje tělo i s jeho vášněmi a žádostmi. Protože Duch je naším životem, podle Ducha také jednejme!

## Zpěv před evangeliem

Aleluja. Přijď, Duchu Svatý, naplň srdce svých věrných a zapal v nich oheň své lásky. Aleluja.

## Evangelium – Jan 15,26-27; 16,12-15

*Slova svatého evangelia podle Jana*

Ježíš řekl svým učedníkům: „Až přijde Přímluvce, kterého vám pošlu od Otce, Duch pravdy, který vychází od Otce, ten vydá o mně svědectví. Vy také vydávejte svědectví, protože jste se mnou od začátku. Měl bych vám ještě mnoho jiného říci, ale teď byste to nemohli snést. Ale až přijde on, Duch pravdy, uvede vás do celé pravdy. On totiž nebude mluvit sám ze sebe, ale bude mluvit to, co uslyší, a oznámí vám, co má přijít. On mě oslaví, protože z mého vezme a vám to oznámí. Všechno, co má Otec, je moje; proto jsem řekl, že z mého vezme a vám to oznámí.“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-05-23.json](/api/v1/2021-05-23.json)
    * YAML - [2021-05-23.yaml](/api/v1/2021-05-23.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-05-23.json](/api/v1/olejnik/2021-05-23.json)
    * YAML - [olejnik/2021-05-23.yaml](/api/v1/olejnik/2021-05-23.yaml)

### Pravopis

!!! failure ""
    {--*Elamité*--}: Elitě, Blanité, Blanitě, Dlanité, Dlanitě, Klaníte, Planíte, Slaníte, Klamte, Ladíte, Lapíte, Limite, Limitě, Lomíte, Lámete, Mámíte, Kalamitě, Neladíte, Nelapíte, Nelomíte, Nelámete, Nemámíte, Pelášíte, Axamite, Adamité, Blažíte, Elative, Etamíne, Hladíte, Hlasité, Hlasitě, Hlásíte, Klamete, Klátíte, Mlátíte, Olámete, Omámíte, Osamíte, Platíte, Plavíte, Plazíte, Plašíte, Sladíte, Slavíte, Tlačíte, Tlumíte, Uladíte, Ulomíte, Ulámete, Vlomíte, Vlámete, Vláčíte, Zlatité, Zlatitě, Zlatíte, Zlomíte, Zlámete, Zmámíte<br>
    {--*Frýgie*--}: Frygické, Freie, Fryče, Fryči, Rygle, Orgie, Frýdce<br>
    {--*Galilejci*--}: Galilejští, Galileovi, Galileech<br>
    {--*Kyrény*--}: Kypřeny, Kypřený, Křeny, Hýřeny, Hýřený, Kořeny, Kypěny, Kypěný, Kýžený<br>
    {--*lybijského*--}: libyjského, libínského, oybinského<br>
    {--*Médové*--}: Medové, Medově, Medkové, Medkově, Medlove, Medlově, Medovém, Edové, Edově, Bédové, Bédově, Béďové, Béďově, Dědové, Dědově, Medova, Medovi, Medovo, Medovu, Medovy, Medová, Jedové, Jedově, Ledové, Ledově, Medoví, Medový, Mečové, Mečově, Mzdové, Mzdově, Měnové, Měnově, Měrové, Měrově, Šedové, Šedově<br>
    {--*Parthové*--}: Barthové, Barthově, Partiové, Partiově<br>
    {--*proselyté*--}: prosely té, prosely-té, prosely, proseté, prosetě, prosytě, proděláte, prosekáte, prosejte, proserte, proseďte, prosolte, provelte, prosklete, prosedíte, prosejete, proserete, prosklíte, prosolíte, proséváte, provelíte, prošetřte<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-05-23/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-05-23/mezizpev.html)
    * [2-cteni.html](/api/v1/obs/2021-05-23/2-cteni.html)
    * [sekvence.html](/api/v1/obs/2021-05-23/sekvence.html)
    * [evangelium.html](/api/v1/obs/2021-05-23/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-05-23/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "red")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
