---
title: '2021-02-17'
---
# Popeleční středa

## 1. čtení – Jl 2,12-18

*Čtení z knihy proroka Joela*

Nyní – praví Hospodin – obraťte se ke mně celým svým srdcem, v postu, nářku a pláči! Roztrhněte svá srdce, a ne (pouze) šaty, a obraťte se k Hospodinu, svému Bohu, neboť je dobrotivý a milosrdný, shovívavý a plný lásky, slituje se v neštěstí. Kdo ví, zda se neobrátí a neodpustí, nezanechá po sobě požehnání: obětní dar a úlitbu pro Hospodina, vašeho Boha? Na Siónu zatrubte na polnici, nařiďte půst, svolejte shromáždění, svolejte lid, zasvěťte obec, sezvěte starce, shromážděte děti i kojence; ženich ať vyjde ze svého pokoje, nevěsta ze své ložnice! Kněží, Hospodinovi služebníci, ať pláčou mezi předsíní a oltářem a říkají: „Ušetř, Hospodine, svůj lid, nevydávej své dědictví v potupu, aby nad ním nevládli pohané!“ Proč se má mezi národy říkat: „Kdepak je ten jejich Bůh?“ Velkou láskou se Hospodin roznítil ke své zemi, smiloval se nad svým lidem.

## Mezizpěv – Žl 51,3-4.5-6a.12-13.14+17

Smiluj se, Pane, neboť jsme zhřešili.

## 2. čtení – 2 Kor 5,20-6,2

*Čtení z druhého listu svatého apoštola Pavla Korinťanům*

(Bratři!) Jsme Kristovi vyslanci, jako by skrze nás napomínal Bůh. Kristovým jménem vyzýváme: Smiřte se s Bohem! S tím, který byl bez hříchu, jednal kvůli nám jako s největším hříšníkem, abychom my skrze něho byli spravedliví u Boha. Jako (Boží) spolupracovníci vás proto napomínáme, abyste nepřijali milost Boží nadarmo! (Bůh) přece říká: „V době příhodné jsem tě vyslyšel, v den spásy jsem ti pomohl.“ Hle, teď je ta „doba příhodná“, hle, teď je ten „den spásy“!

## Zpěv před evangeliem – srov. Žl 95,8ab

Nezatvrzujte dnes svá srdce, ale slyšte hlas Hospodinův.

## Evangelium – Mt 6,1-6.16-18

*Slova svatého evangelia podle Matouše*

Ježíš řekl svým učedníkům: „Dejte si pozor, abyste nekonali dobré skutky okázale před lidmi, jinak nemáte odplatu u svého Otce v nebesích. Když tedy dáváš almužnu, nevytrubuj před sebou, jak to dělají pokrytci v synagogách a na ulicích, aby je lidé velebili. Amen, pravím vám: Ti už svou odplatu dostali. Když však dáváš almužnu ty, ať neví tvoje levice, co dělá tvoje pravice, aby tvoje almužna zůstala skrytá, a tvůj Otec, který vidí i to, co je skryté, ti odplatí. A když se modlíte, nebuďte jako pokrytci. Ti se rádi stavějí k modlitbě v synagogách a na rozích ulic, aby je lidé viděli. Amen, pravím vám: Ti už svou odplatu dostali. Když se však modlíš ty, vejdi do své komůrky, zavři dveře a modli se k svému Otci, který je ve skrytosti, a tvůj Otec, který vidí i to, co je skryté, ti odplatí. A když se postíte, nedělejte ztrápený obličej jako pokrytci. Ti totiž dělají svůj obličej nevzhledným, aby lidem ukazovali, že se postí. Amen, pravím vám: Ti už svou odplatu dostali. Když se však postíš ty, pomaž si hlavu a umyj si tvář, abys neukazoval lidem, že se postíš, ale svému Otci, který je ve skrytosti; a tvůj Otec, který vidí i to, co je skryté, ti odplatí.

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-02-17.json](/api/v1/2021-02-17.json)
    * YAML - [2021-02-17.yaml](/api/v1/2021-02-17.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-02-17.json](/api/v1/olejnik/2021-02-17.json)
    * YAML - [olejnik/2021-02-17.yaml](/api/v1/olejnik/2021-02-17.yaml)

### Pravopis

!!! failure ""
    {--*Joela*--}: Ojela, Jela, Jola<br>
    {--*Neodvrhuj*--}: Nepodvrhuj, Neodtrhuj, Nedovrhej<br>
    {--*očisť*--}: očist, odčíst, očista, očisti, očisto, očistu, očisty, očistě, očistí, očisťl, očišťl, čist, číst, dočíst, počíst, ojíst, učíst<br>
    {--*shromážděte*--}: shromážděné, shromážděně, shromáždíte<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-02-17/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-02-17/mezizpev.html)
    * [2-cteni.html](/api/v1/obs/2021-02-17/2-cteni.html)
    * [evangelium.html](/api/v1/obs/2021-02-17/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-02-17/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "deep-purple")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
