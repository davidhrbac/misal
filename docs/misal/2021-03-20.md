---
title: '2021-03-20'
---
# Sobota po 4. neděli postní

## 1. čtení – Jer 11,18-20

*Čtení z knihy proroka Jeremiáše*

Hospodin mě o tom poučil, a já jsem to poznal; tehdy jsem viděl jejich zlé skutky. Byl jsem jako krotký beránek, kterého vedou na porážku. Ani potuchy jsem neměl, že mi strojí úklady: „Zničme strom i s jeho mízou, vytněme ho ze země živých, ať není památky po jeho jméně!“ Hospodine zástupů, spravedlivý soudce, který zkoumáš srdce i ledví, kéž vidím tvoji pomstu nad nimi, neboť tobě svěřuji svou při.

## Mezizpěv – Žl 7,2-3.9bc-10.11-12

Hospodine, můj Bože, k tobě se utíkám.

## Zpěv před evangeliem – srov. Lk 8,15

Blahoslavení, kteří slovo Páně uchovávají v dobrém a upřímném srdci a s vytrvalostí přinášejí užitek.

## Evangelium – Jan 7,40-53

*Slova svatého evangelia podle Jana*

Když někteří ze zástupu slyšeli Ježíšova slova, říkali: „To je skutečně ten Prorok!“ Druzí zase: „To je Mesiáš!“ Ale jiní namítali: „Copak přijde Mesiáš z Galileje? Neříká Písmo, že Mesiáš vzejde z potomstva Davidova a z vesnice Betléma, odkud David pocházel?“ Proto kvůli němu došlo v zástupu k roztržce. Někteří z nich ho chtěli zatknout, ale nikdo na něho nevztáhl ruku. Služebníci (velerady) se vrátili k velekněžím a farizeům a ti se jich zeptali: „Proč jste ho nepřivedli?“ Služebníci odpověděli: „Žádný člověk nikdy tak nemluvil.“ Farizeové jim na to řekli: „I vy jste se nechali svést? Uvěřil v něho někdo z předních mužů nebo z farizeů? Leda jen tahle luza, která nezná Zákon - kletba na ně!“ Jeden z nich, Nikodém, ten, který už dříve přišel za Ježíšem, jim namítl: „Odsoudí náš Zákon někoho, dokud ho nevyslechne a nezjistí, co dělá?“ Odpověděli mu: „Nejsi ty také z Galileje? Zkoumej a uvidíš, že z Galileje prorok nepovstane.“ A každý se vrátil domů.

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-03-20.json](/api/v1/2021-03-20.json)
    * YAML - [2021-03-20.yaml](/api/v1/2021-03-20.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-03-20.json](/api/v1/olejnik/2021-03-20.json)
    * YAML - [olejnik/2021-03-20.yaml](/api/v1/olejnik/2021-03-20.yaml)

### Pravopis

!!! failure ""
    {--*Galileje*--}: Galileem, Galeje, Galilejské, Galileové, Galileově, Galilea, Galileo, Galileu, Galiley, Galileů, Basileje, Galileům, Galileův<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-03-20/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-03-20/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-03-20/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-03-20/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "deep-purple")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
