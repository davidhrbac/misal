---
title: '2021-03-24'
---
# Středa po 5. neděli postní

## 1. čtení – Dan 3,14-20.91-92.95

*Čtení z knihy proroka Daniela*

Král Nabuchodonosor řekl: „Je to pravda, Šadrachu, Mešachu a Abednego, že nectíte mé bohy a neklaníte se zlaté soše, kterou jsem dal postavit? Nuže, jste připraveni hned, jak uslyšíte zvuk rohu, flétny, citery, harfy, psalteria, dud a hudebníky všeho druhu, padnout a klanět se soše, kterou jsem dal postavit? Jestliže se však klanět nebudete, hned budete vhozeni do rozpálené ohnivé pece – a který bůh vás bude moci vysvobodit z mých rukou?“ Šadrach, Mešach a Abednego odpověděli králi Nabuchodonosorovi: „Není třeba, abychom kvůli tomu tobě odpovídali. Jestliže se to stane, náš Bůh, jehož ctíme, může nás vysvobodit; z rozpálené ohnivé pece i z tvé ruky, králi, nás vysvobodí; i kdyby ne, nechť je ti známo, králi, že (ani pak) tvé bohy nectíme a zlaté soše, kterou jsi dal postavit, se neklaníme!“ Tu se Nabuchodonosor vůči Šadrachovi, Mešachovi a Abednegovi naplnil hněvem, který změnil jeho tvář. Poručil rozpálit pec sedmkrát víc, než obvykle se rozpalovala. Několika nejsilnějším mužům svého vojska rozkázal svázat Šadracha, Mešacha a Abednega, aby je vhodili do rozpálené ohnivé pece. (Najednou) král Nabuchodonosor užasl, rychle vstal a řekl svým dvořanům: „Copak jste nevhodili tři svázané muže doprostřed ohně?“ Odpověděli a řekli králi: „Ano, králi!“ (Král) pravil: „Hle, já vidím čtyři muže rozvázané, jak se procházejí uprostřed ohně, není na nich úhony a vzhled čtvrtého je podobný Božímu synu.“ Tu zvolal Nabuchodonosor: „Požehnaný Bůh Šadrachův, Mešachův a Abednegův, jenž poslal svého anděla a vysvobodil své služebníky, kteří v něj důvěřovali! Přestoupili královský rozkaz a vydali svá těla, aby nemuseli uctívat žádného boha, jedině Boha svého, a klanět se mu.“

## Mezizpěv – Dan 3,52a.52b.53.54.55.56

Požehnaný jsi, Pane, Bože našich otců,
O: chvályhodný a svrchovaně velebený navěky.
Požehnané je tvé slavné svaté jméno
O: a svrchovaně chvályhodné a velebené navěky.
Požehnaný jsi ty ve svém svatém slavném chrámě
O: a svrchovaně chvályhodný a slavný navěky.
Požehnaný jsi ty na svém královském trůně
O: a svrchovaně chvályhodný a velebený navěky.
Požehnaný jsi ty, který shlížíš na hlubiny
 a trůníš na cherubech,
O: a svrchovaně chvályhodný a velebený navěky.
Požehnaný jsi ty na klenbě nebes
O: a velebený a plný slávy navěky.

## Zpěv před evangeliem – srov. Lk 8,15

Blahoslavení, kteří slovo Páně uchovávají v dobrém a upřímném srdci a s vytrvalostí přinášejí užitek.

## Evangelium – Jan 8,31-42

*Slova svatého evangelia podle Jana*

Ježíš řekl židům, kteří v něho uvěřili: „Když vytrváte v mém slovu, budete opravdu mými učedníky. Poznáte pravdu, a pravda vás osvobodí.“ Namítli mu: „Jsme potomci Abrahámovi a nikdy jsme nikomu neotročili. Jak ty můžeš říci: `Stanete se svobodnými'?“ Ježíš jim odpověděl: „Amen, amen, pravím vám: Každý, kdo páchá hřích, je otrokem hříchu. Otrok nezůstává v domě navždy, syn zůstává navždy. Když vás Syn osvobodí, budete skutečně svobodní. Vím, že jste potomci Abrahámovi, ale ukládáte mi o život, protože mé slovo se u vás neuchytí. Já mluvím, co jsem viděl u svého Otce, a vy děláte, co jste slyšeli od vašeho otce.“ Řekli mu na to: „Náš otec je Abrahám.“ Ježíš jim řekl: „Kdybyste byli děti Abrahámovy, konali byste skutky jako Abrahám. Nyní však mě chcete zabít – toho, kdo vám mluvil pravdu, kterou slyšel od Boha. Tak Abrahám nejednal. Vy konáte skutky vašeho otce.“ Odpověděli mu: „My jsme se nenarodili ze smilstva, máme jednoho Otce, Boha.“ Ježíš jim řekl: „Kdyby byl vaším otcem Bůh, milovali byste mě, vždyť já jsem vyšel od Boha a od něho přicházím. Nepřišel jsem totiž sám od sebe, ale on mě poslal.“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-03-24.json](/api/v1/2021-03-24.json)
    * YAML - [2021-03-24.yaml](/api/v1/2021-03-24.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-03-24.json](/api/v1/olejnik/2021-03-24.json)
    * YAML - [olejnik/2021-03-24.yaml](/api/v1/olejnik/2021-03-24.yaml)

### Pravopis

!!! failure ""
    {--*Abednega*--}: Bedněna, Zabedněna, Zabedněná, Ubedněna<br>
    {--*Abednego*--}: Bědného, Bedněno, Zabedněno, Ubedněno<br>
    {--*Mešacha*--}: Nespáchá, Nesahá, Macha, Mácha, Máchá, Šacha, Měsťácká, Měšťačka, Měšťácká, Měšťáčka, Nemáchá, Nepáchá, Nesochá, NeŠacká, Nešachl, Mášách, Míšách, Masách, Melách, Metách, Meších, Měnách, Měších, Mísách, Melicha, Fešácká, Myšáčka, Měsíčná, Měšická, Pěšácká, Sesychá, Sesýchá, Česačka<br>
    {--*Mešach*--}: Mášách, Míšách, Masách, Melách, Metách, Meších, Měnách, Měších, Mísách<br>
    {--*Mešachovi*--}: Nešachoví, Machovi, Máchovi, Mechoví, Měchoví, Šachovi, Šachoví, Měšťáčkovi, Nepachoví, Nešachová, Nešachové, Nešachově, Nešachový, Česačovi, Česáčovi, Melichovi, Myšáčkovi, Měsíčkoví<br>
    {--*Mešachu*--}: Nešachuj, Machu, Machů, Máchu, Máchů, Mechu, Mechů, Měchu, Měchů, Měšců, Šachu, Šachů, Měšťačku, Měšťáčku, Měšťáčků, Nešachl, Mášách, Městců, Míšách, Masách, Melách, Metách, Meších, Měnách, Měsíců, Měších, Mísách, Česačů, Česáčů, Melichu, Melichů, Myšáčku, Myšáčků, Měsíčku, Měsíčků, Věšáčku, Věšáčků, Česačku<br>
    {--*Mešachův*--}: Machův, Máchův, Šachův, Měšťáčkův, Nešachuj, Česačův, Česáčův, Melichův, Myšáčkův<br>
    {--*Šadracha*--}: Sádrách, Sandrách, Sárách, Sadách, Nadrchá, Raracha, Sadbách, Sádkách, Štrachá<br>
    {--*Šadrachovi*--}: Rarachovi<br>
    {--*Šadrach*--}: Sádrách, Sandrách, Sárách, Sadách, Sadbách, Sádkách<br>
    {--*Šadrachu*--}: Sádrách, Sandrách, Sárách, Sadách, Rarachu, Rarachů, Sadbách, Strachu, Strachů, Sádkách<br>
    {--*Šadrachův*--}: Sádrách, Rarachův<br>

### Typografie

!!! bug ""
    Ježíš řekl židům, kteří v něho uvěřili: „Když vytrváte v mém slovu, budete opravdu mými učedníky. Poznáte pravdu, a pravda vás osvobodí.“ Namítli mu: „Jsme potomci Abrahámovi a nikdy jsme nikomu neotročili. Jak ty můžeš říci: {--*\`*--}Stanete se svobodnými'?“ Ježíš jim odpověděl: „Amen, amen, pravím vám: Každý, kdo páchá hřích, je otrokem hříchu. Otrok nezůstává v domě navždy, syn zůstává navždy. Když vás Syn osvobodí, budete skutečně svobodní. Vím, že jste potomci Abrahámovi, ale ukládáte mi o život, protože mé slovo se u vás neuchytí. Já mluvím, co jsem viděl u svého Otce, a vy děláte, co jste slyšeli od vašeho otce.“ Řekli mu na to: „Náš otec je Abrahám.“ Ježíš jim řekl: „Kdybyste byli děti Abrahámovy, konali byste skutky jako Abrahám. Nyní však mě chcete zabít – toho, kdo vám mluvil pravdu, kterou slyšel od Boha. Tak Abrahám nejednal. Vy konáte skutky vašeho otce.“ Odpověděli mu: „My jsme se nenarodili ze smilstva, máme jednoho Otce, Boha.“ Ježíš jim řekl: „Kdyby byl vaším otcem Bůh, milovali byste mě, vždyť já jsem vyšel od Boha a od něho přicházím. Nepřišel jsem totiž sám od sebe, ale on mě poslal.“

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-03-24/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-03-24/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-03-24/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-03-24/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "deep-purple")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
