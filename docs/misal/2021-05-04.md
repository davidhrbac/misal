---
title: '2021-05-04'
---
# Úterý po 5. neděli velikonoční

## 1. čtení – Sk 14,19-28

*Čtení ze Skutků apoštolů*

Do Lystry přišli židé z Antiochie a z Ikónia, přemluvili dav a začali Pavla kamenovat. Pak ho vyvlekli ven z města, protože mysleli, že už je mrtvý. Když ho však učedníci obstoupili, vstal a šel zase do města. Ale na druhý den odešel s Barnabášem do Derbe. Také v tomto městě hlásali radostnou zvěst a získali mnoho učedníků. Potom se vrátili do Lystry, Ikónia a do Antiochie. Utvrzovali tam učedníky a povzbuzovali je, aby byli ve víře vytrvalí, protože do Božího království vejdeme jen tehdy, když hodně vytrpíme. V jednotlivých církevních obcích jim po modlitbě a postu ustanovili starší a poručili je Pánu, v kterého uvěřili. Potom prošli Pisídií a Pamfýlií a hlásali slovo Páně v Perge. Pak odešli do Atálie. Odtamtud odpluli lodí do Antiochie, kde byli kdysi doporučeni milosti Boží k dílu, které teď ukončili. Když tam přišli, svolali církevní obec a vypravovali, co všechno Bůh s nimi vykonal a jak otevřel bránu k víře pohanům. A zůstali pak s (tamějšími) učedníky delší čas.

## Mezizpěv – Žl 145,10-11.12-13ab.21

Ať tvoji zbožní, Hospodine, vypravují o slávě tvé vznešené říše.

## Zpěv před evangeliem – srov. Lk 24,46.26

Aleluja. Kristus musel trpět a vstát z mrtvých, a tak vejít do své slávy. Aleluja.

## Evangelium – Jan 14,27-31a

*Slova svatého evangelia podle Jana*

Ježíš řekl svým učedníkům: „Pokoj vám zanechávám, svůj pokoj vám dávám; ne ten, který dává svět, já vám dávám. Ať se vaše srdce nechvěje a neděsí. Slyšeli jste, že jsem vám řekl: `Odcházím' a `(zase) k vám přijdu.' Kdybyste mě milovali, radovali byste se, že jdu k Otci, neboť Otec je větší než já. Řekl jsem vám to už teď, dříve než se to stane, abyste uvěřili, až se to stane. Už s vámi nebudu mnoho mluvit, protože přichází vládce tohoto světa. Proti mně však nezmůže nic. Ale ať svět pozná, že miluji Otce a jednám, jak mi Otec přikázal.“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-05-04.json](/api/v1/2021-05-04.json)
    * YAML - [2021-05-04.yaml](/api/v1/2021-05-04.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-05-04.json](/api/v1/olejnik/2021-05-04.json)
    * YAML - [olejnik/2021-05-04.yaml](/api/v1/olejnik/2021-05-04.yaml)

### Pravopis

!!! failure ""
    {--*Atálie*--}: Natálie, Patálie, Amálie, Itálie, Ataxie<br>
    {--*Derbe*--}: Děrné, Děrně, Dere, Drbe, Erbe, Derby, Derme, Derte, Dělbě<br>
    {--*dílaa*--}: díla, dílka, dílna<br>
    {--*Ikónia*--}: Ikona, Ikonka, Ilona<br>
    {--*Lystry*--}: Lustry, Bystrý, Bystrá<br>
    {--*Perge*--}: Pere, Perné, Perle, Perme, Perně, Perte<br>
    {--*Pisídií*--}: Píšícími, Iridii, Iridií, Píšící, Basidii, Basidií, Píšícím<br>
    {--*věkůa*--}: veka, veku, věku, věků, vakua, věkům<br>

### Typografie

!!! bug ""
    Ať tě chválí, Hospodine, všechna tvá dílaa tvoji zbožní ať tě velebí!Ať vypravují o slávě tvého královstv{--*í,a*--}ť mluví o tvé síle!
    Aby poučili lidi o tvé moc{--*i,o[m[K slávě tvé vznešené říš[01;31m[Ke.T*--}vé království je království všech věkůa tvá vláda trvá po všechna pokolení.
    Ať má ústa hlásají Hospodinovu chvál{--*u,v*--}še, co žije, ať velebí jeho svaté jméno po všechny věky!
    Ježíš řekl svým učedníkům: „Pokoj vám zanechávám, svůj pokoj vám dávám; ne ten, který dává svět, já vám dávám. Ať se vaše srdce nechvěje a neděsí. Slyšeli jste, že jsem vám řekl: {--*\`*--}Odcházím' a {--*\`*--}(zase) k vám přijdu.' Kdybyste mě milovali, radovali byste se, že jdu k Otci, neboť Otec je větší než já. Řekl jsem vám to už teď, dříve než se to stane, abyste uvěřili, až se to stane. Už s vámi nebudu mnoho mluvit, protože přichází vládce tohoto světa. Proti mně však nezmůže nic. Ale ať svět pozná, že miluji Otce a jednám, jak mi Otec přikázal.“

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-05-04/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-05-04/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-05-04/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-05-04/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "white")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
