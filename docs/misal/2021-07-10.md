---
title: '2021-07-10'
---
# Sobota po 14. neděli v mezidobí

## 

null

null

## Mezizpěv – Žl 105,1-2.3-4.6-7

Hledejte Pána, ubožáci, a vaše duše bude žít.

## Zpěv před evangeliem – 1 Petr 4,14

Aleluja. Když musíte snášet urážky pro Kristovo jméno, blaze vám, neboť na vás spočívá Duch Boží. Aleluja.

## Evangelium – Mt 10,24-33

*Slova svatého evangelia podle Matouše*

Ježíš řekl svým apoštolům: „Není žák nad učitele ani služebník nad svého pána. Stačí, aby žák byl jako jeho učitel a služebník jako jeho pán. Když nazvali Belzebubem hospodáře, čím spíše jeho lidi! Proto se jich nebojte! Nic není tak tajného, že by to nebylo odhaleno, a nic skrytého, že by to nebylo poznáno. Co vám říkám ve tmě, povězte na světle, a co se vám šeptá do ucha, hlásejte ze střech! A nebojte se těch, kdo zabíjejí tělo
duši zabít nemohou. Spíše se bojte toho, který může zahubit v pekle duši i tělo. Copak se neprodávají dva vrabci za halíř? A ani jeden z nich nespadne na zem bez vědomí vašeho Otce. U vás však jsou spočítány i všechny vlasy na hlavě. Nebojte se tedy: Máte větší cenu než všichni vrabci. Ke každému, kdo se ke mně přizná před lidmi, i já se přiznám před svým Otcem v nebi; ale každého, kdo mě před lidmi zapře, zapřu i já před svým Otcem v nebi.“ 

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-07-10.json](/api/v1/2021-07-10.json)
    * YAML - [2021-07-10.yaml](/api/v1/2021-07-10.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-07-10.json](/api/v1/olejnik/2021-07-10.json)
    * YAML - [olejnik/2021-07-10.yaml](/api/v1/olejnik/2021-07-10.yaml)

### Pravopis

!!! failure ""
    {--*Efraima*--}: Erasma<br>
    {--*Efrona*--}: Ferina, Froňka, Fronta, Egona, Verona, Férová, Nefron, Eatona, Fiona, Fraňa, Fráňa, Hrona, Frkna, Front, Nefrkna, Nefrone, Nefronu, Nefrony, Nefronů, Barona, Evropa, Korona, Koróna<br>
    {--*hrejte*--}: hřejte, hrkejte, hárejte, hřejete, hřmějte, hřeje, ohřejte, uhřejte, hrajte, hřejme, hřešte, orejte, prejte, přejte<br>
    {--*Kanaán*--}: Kanaďan, Kankán, Kadaň<br>
    {--*Machira*--}: Machra, Machara, Bachura<br>
    {--*Machpela*--}: Načpělá, Máchla, Máčela, Machala, Máchala, Začpěla, Michaela, Zachtěla, Zachvěla<br>
    {--*Mamre*--}: Mařme, Mamte, Máře, Maře, Máme, Mámě, Hamře, Maure, Mádře, Hamre, Mamce, Mamme<br>
    {--*Manassova*--}: Maňáskova, Maňásková, Janásova, Janásová, Matasova, Matasová, Mánesova, Mánesová, Matáskova, Matásková, Maňáskovi, Maňáskovo, Maňáskovu, Maňáskovy, Maňáskové, Maňáskově, Maňáskoví, Maňáskový<br>
    {--*Rebeka*--}: Rebelka, Rebela, Brebera<br>

### Typografie

!!! bug ""
    Jakub dal příkaz svým synům: „Já už budu připojen ke svým příbuzným. Pochovejte mě u mých otců do jeskyně na poli Chetity Efrona, do jeskyně na poli Machpela proti Mamre v zemi Kanaán, kterou si koupil Abrahám od Chetity Efrona do vlastnictví jako pohřebiště. Tam byl pochován Abrahám a jeho žena Sára, tam byl pochován Izák a jeho žena Rebeka a tam jsem pochoval Leu. To pole a jeskyně, která je na něm, byly získány od Chetitů!“ Když Jakub dokončil pořízení se svými syny, položil se na lůžko a zemřel. Byl připojen ke svým příbuzným. Když Josefovi bratři viděli, že jejich otec je mrtev, řekli si: „Třeba nás bude Josef pronásledovat a odplatí nám všechno to zlé, co jsme mu způsobili.“ Poslali Josefovi vzkaz: „Tvůj otec před smrtí poručil: Tak řekněte Josefovi: {--*\`*--}Odpusť, prosím, zločin svým bratřím a jejich hřích, že ti způsobili zlé!' Nuže, odpusť, prosíme, zločin služebníkům Boha svého otce!“ Josef se nad jejich vzkazem rozplakal. I jeho bratři přišli, padli před ním a řekli: „Hle, jsme tvými otroky!“ Josef odpověděl: „Nebojte se, copak jsem namísto Boha? Vy jste zamýšleli se mnou zlé, ale Bůh to obrátil v dobré, aby provedl, co je dnes zřejmé: zachoval život četnému lidu. Jen se nebojte! Postarám se o vás i o vaše děti.“ Upokojil je a přátelsky jim domlouval. Josef i rodina jeho otce přebývali v Egyptě. Josef byl živ sto deset let a viděl pravnuky Efraima. Také synové Machira, syna Manassova, se narodili na Josefova kolena. Potom řekl Josef svým bratřím: „Já už umřu, ale Bůh se vás jistě ujme a vyvede vás z této země do země, kterou přísahou zaslíbil Abrahámovi, Izákovi a Jakubovi.“ Nato je zavázal přísahou: „Až se vás Bůh ujme, vezměte s sebou mé kosti z tohoto místa.“ Josef zemřel ve stáří sto deseti let.

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni-gn-49-29-32-50-15-26a-hebr-49-29-33-50-15-25.html](/api/v1/obs/2021-07-10/1-cteni-gn-49-29-32-50-15-26a-hebr-49-29-33-50-15-25.html)
    * [mezizpev.html](/api/v1/obs/2021-07-10/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-07-10/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-07-10/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "green")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
