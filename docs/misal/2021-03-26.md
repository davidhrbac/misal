---
title: '2021-03-26'
---
# Pátek po 5. neděli postní

## 1. čtení – Jer 20,10-13

*Čtení z knihy proroka Jeremiáše*

Slyšel jsem nepřátelské umlouvání mnohých: „Hrůza ze všech stran! Udejte ho! Udáme ho!“ I ti, kteří se mnou žili v přátelství, číhají na můj pád: „Snad se dá svést a zmocníme se ho a pomstíme se na něm!“ Ale Hospodin je se mnou jako silný bojovník; proto ti, kteří mě stíhají, padnou a nic nesvedou. Velmi budou zahanbeni, neboť ničeho nedosáhnou; bude to věčná hanba, nezapomene se na ni. Hospodine zástupů, který zkoušíš spravedlivého, který vidíš ledví i srdce, kéž uzřím tvou pomstu nad nimi, neboť tobě jsem svěřil svou při. Zpívejte Hospodinu, chvalte Hospodina, že vysvobodil život ubohého z ruky zlosynů!

## Mezizpěv – Žl 18,2-3a.3b-4.5-6.7

Ve své tísni jsem vzýval Hospodina, a vyslyšel mě.

## Zpěv před evangeliem – Jan 6,63b.68b

Tvá slova, Pane, jsou duch a jsou život; ty máš slova věčného života.

## Evangelium – Jan 10,31-42

*Slova svatého evangelia podle Jana*

Židé brali kameny, aby Ježíše kamenovali. Ježíš jim na to řekl: „Ukázal jsem vám mnoho dobrých skutků od Otce. Pro který z nich mě chcete kamenovat?“ Židé mu odpověděli: „Pro dobrý skutek tě nechceme kamenovat, ale pro rouhání: ty jsi jen člověk, a děláš ze sebe Boha.“ Na to jim Ježíš řekl: „Ve vašem Zákoně je přece psáno: `Já jsem řekl: Jste bohové.' Jestliže nazval bohy ty, kterým se dostalo Božího slova – a Písmo nemůže být zrušeno – můžete vy říkat o tom, kterého Otec posvětil a poslal na svět, že se rouhá, protože jsem řekl: `Jsem Syn Boží'? Nekonám-li skutky svého Otce, nevěřte mi. Jestliže však je konám a nevěříte mně, věřte těm skutkům, abyste poznali a (konečně) pochopili, že Otec je ve mně a já v Otci.“ Znovu by se ho byli rádi zmocnili, ale on jim unikl. Zase odešel za Jordán na to místo, kde dříve křtíval Jan, a tam zůstal. Přišlo k němu mnoho lidí. Říkali: „Jan sice neudělal žádné znamení, ale všechno, co Jan řekl o něm, bylo pravda.“ A mnoho jich tam v něho uvěřilo.

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-03-26.json](/api/v1/2021-03-26.json)
    * YAML - [2021-03-26.yaml](/api/v1/2021-03-26.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-03-26.json](/api/v1/olejnik/2021-03-26.json)
    * YAML - [olejnik/2021-03-26.yaml](/api/v1/olejnik/2021-03-26.yaml)

### Pravopis

!!! failure ""
    {--*křtíval*--}: vrtíval, křtí val, křtí-val, křtila, Krtova, křivá, křtil, rival, kostival, křičíval, okříval, vrtívala, vrtívali, vrtívalo, vrtívaly, kytoval, kótoval, drtivá, kravál, krmiva, krmivá, krtina, křivil, křížal, přival, přivál, příval, vrtivá, vrtívá, škrtával, krkával, krmivář, krtinám, kutával, křtinám, křtitel, mstíval, uctíval, vrtával, vrtívat, vrtívám, vrtíváš<br>
    {--*záhubné*--}: zahuben, zahubené, zahubeně, zahubme, zahubě, zhubne, záhubě, zahubte, zárubně<br>

### Typografie

!!! bug ""
    Židé brali kameny, aby Ježíše kamenovali. Ježíš jim na to řekl: „Ukázal jsem vám mnoho dobrých skutků od Otce. Pro který z nich mě chcete kamenovat?“ Židé mu odpověděli: „Pro dobrý skutek tě nechceme kamenovat, ale pro rouhání: ty jsi jen člověk, a děláš ze sebe Boha.“ Na to jim Ježíš řekl: „Ve vašem Zákoně je přece psáno: {--*\`*--}Já jsem řekl: Jste bohové.' Jestliže nazval bohy ty, kterým se dostalo Božího slova – a Písmo nemůže být zrušeno – můžete vy říkat o tom, kterého Otec posvětil a poslal na svět, že se rouhá, protože jsem řekl: {--*\`*--}Jsem Syn Boží'? Nekonám-li skutky svého Otce, nevěřte mi. Jestliže však je konám a nevěříte mně, věřte těm skutkům, abyste poznali a (konečně) pochopili, že Otec je ve mně a já v Otci.“ Znovu by se ho byli rádi zmocnili, ale on jim unikl. Zase odešel za Jordán na to místo, kde dříve křtíval Jan, a tam zůstal. Přišlo k němu mnoho lidí. Říkali: „Jan sice neudělal žádné znamení, ale všechno, co Jan řekl o něm, bylo pravda.“ A mnoho jich tam v něho uvěřilo.

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-03-26/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-03-26/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-03-26/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-03-26/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "deep-purple")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
