---
title: '2021-07-08'
---
# Čtvrtek po 14. neděli v mezidobí

## 1. čtení – Gn 44,18-21.23b-29; 45,1-5

*Čtení z první knihy Mojžíšovy*

(Když bratři přišli k Josefovi podruhé), Juda k němu přistoupil a řekl: „Dovol mi, pane, ať smí tvůj služebník něco říci, dopřej mu sluchu! Ať se nevznítí tvůj hněv na tvého služebníka, neboť ty stojíš tak vysoko jako farao. Můj pán se ptal svých služebníků: `Máte otce nebo bratra?' Řekli jsme mému pánu: `Máme starého otce a malého bratra, který se mu narodil ve stáří. Jeho bratr je mrtvý, a tak on zůstal jako jediné dítě své matky a otec ho miluje.' Řekl jsi svým služebníkům: `Přiveďte ho ke mně, abych ho viděl na vlastní oči. Jestliže váš bratr nepřijde s vámi, neuvidíte už mou tvář!' Proto jsme přišli k tvému služebníku, našemu otci, oznámili jsme mu slova mého pána. Když pak nám řekl otec: `Jděte znovu nakoupit trochu živobytí' – odpověděli jsme: `Nesmíme tam přijít! Půjdeme tam, jen bude-li s námi náš nejmladší bratr. Nesmíme totiž spatřit tvář toho muže, nebude-li s námi náš nejmladší bratr.' Tu nám řekl tvůj služebník, náš otec: `Vy víte, že mi má žena porodila jen dva syny. Jeden ode mě odešel a musel jsem říci: Jistě byl roztrhán od dravé zvěře! – až dosud jsem ho nespatřil. Vezmete-li mně i tohoto a potká ho nehoda, neštěstím přivedete mé šediny do podsvětí!“ Josef se už nemohl zdržet přede všemi, kteří kolem něho stáli, a zvolal: „Vyjděte všichni ode mě!“ A nikdo u něho nebyl, když se dal svým bratřím poznat. Hlasitě zaplakal. Slyšeli to všichni Egypťané a zpráva o tom přišla až k faraónovu dvoru. Josef řekl svým bratřím: „Já jsem Josef! Můj otec je tedy ještě živ?“ Jeho bratři nebyli s to mu odpovědět, neboť se ho zděsili. Josef řekl svým bratřím: „Přistupte ke mně!“ Přistoupili tedy. Znovu jim řekl: „Já jsem Josef, váš bratr, kterého jste prodali do Egypta. Ale nebuďte sklíčeni a nermuťte se, že jste mě sem prodali, neboť Bůh mě poslal před vámi, aby vás zachoval naživu.“

## Mezizpěv – Žl 105,16-17.18-19.20-21

Pamatujte na divy, které učinil Hospodin.

## Zpěv před evangeliem – Mk 1,15

Aleluja. Přiblížilo se Boží království, praví Pán; obraťte se a věřte evangeliu. Aleluja.

## Evangelium – Mt 10,7-15

*Slova svatého evangelia podle Matouše*

Ježíš řekl svým apoštolům: „Jděte a hlásejte: `Přiblížilo se nebeské království.' Uzdravujte nemocné, probouzejte k životu mrtvé, očišťujte malomocné, vyhánějte zlé duchy. Zadarmo jste dostali, zadarmo dávejte. Neshánějte si do opasku ani zlaté, ani stříbrné, ani měděné peníze, neberte si na cestu mošnu ani dvoje šaty ani opánky ani hůl, protože dělník má právo na svou obživu. V každém městě nebo v každé vesnici, kam vejdete, se vyptejte, kdo si to v nich zaslouží, a tam zůstaňte, dokud se nevydáte na další cestu. Když vstoupíte do domu, pozdravte ho (přáním pokoje)! A když si to ten dům zaslouží, ať na něm spočine váš pokoj; když si to nezaslouží, ať se váš pokoj vrátí k vám. A když vás někdo nepřijme a nebude na vaše slova dbát, při odchodu z toho domu nebo města si vytřeste prach ze svých nohou. Amen, pravím vám: V den soudu bude lehčeji zemi sodomské a gomorské než takovému městu.“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-07-08.json](/api/v1/2021-07-08.json)
    * YAML - [2021-07-08.yaml](/api/v1/2021-07-08.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-07-08.json](/api/v1/olejnik/2021-07-08.json)
    * YAML - [olejnik/2021-07-08.yaml](/api/v1/olejnik/2021-07-08.yaml)

### Pravopis

!!! failure ""
    N/A

### Typografie

!!! bug ""
    (Když bratři přišli k Josefovi podruhé), Juda k němu přistoupil a řekl: „Dovol mi, pane, ať smí tvůj služebník něco říci, dopřej mu sluchu! Ať se nevznítí tvůj hněv na tvého služebníka, neboť ty stojíš tak vysoko jako farao. Můj pán se ptal svých služebníků: {--*\`*--}Máte otce nebo bratra?' Řekli jsme mému pánu: {--*\`*--}Máme starého otce a malého bratra, který se mu narodil ve stáří. Jeho bratr je mrtvý, a tak on zůstal jako jediné dítě své matky a otec ho miluje.' Řekl jsi svým služebníkům: {--*\`*--}Přiveďte ho ke mně, abych ho viděl na vlastní oči. Jestliže váš bratr nepřijde s vámi, neuvidíte už mou tvář!' Proto jsme přišli k tvému služebníku, našemu otci, oznámili jsme mu slova mého pána. Když pak nám řekl otec: {--*\`*--}Jděte znovu nakoupit trochu živobytí' – odpověděli jsme: {--*\`*--}Nesmíme tam přijít! Půjdeme tam, jen bude-li s námi náš nejmladší bratr. Nesmíme totiž spatřit tvář toho muže, nebude-li s námi náš nejmladší bratr.' Tu nám řekl tvůj služebník, náš otec: {--*\`*--}Vy víte, že mi má žena porodila jen dva syny. Jeden ode mě odešel a musel jsem říci: Jistě byl roztrhán od dravé zvěře! – až dosud jsem ho nespatřil. Vezmete-li mně i tohoto a potká ho nehoda, neštěstím přivedete mé šediny do podsvětí!“ Josef se už nemohl zdržet přede všemi, kteří kolem něho stáli, a zvolal: „Vyjděte všichni ode mě!“ A nikdo u něho nebyl, když se dal svým bratřím poznat. Hlasitě zaplakal. Slyšeli to všichni Egypťané a zpráva o tom přišla až k faraónovu dvoru. Josef řekl svým bratřím: „Já jsem Josef! Můj otec je tedy ještě živ?“ Jeho bratři nebyli s to mu odpovědět, neboť se ho zděsili. Josef řekl svým bratřím: „Přistupte ke mně!“ Přistoupili tedy. Znovu jim řekl: „Já jsem Josef, váš bratr, kterého jste prodali do Egypta. Ale nebuďte sklíčeni a nermuťte se, že jste mě sem prodali, neboť Bůh mě poslal před vámi, aby vás zachoval naživu.“
    Ježíš řekl svým apoštolům: „Jděte a hlásejte: {--*\`*--}Přiblížilo se nebeské království.' Uzdravujte nemocné, probouzejte k životu mrtvé, očišťujte malomocné, vyhánějte zlé duchy. Zadarmo jste dostali, zadarmo dávejte. Neshánějte si do opasku ani zlaté, ani stříbrné, ani měděné peníze, neberte si na cestu mošnu ani dvoje šaty ani opánky ani hůl, protože dělník má právo na svou obživu. V každém městě nebo v každé vesnici, kam vejdete, se vyptejte, kdo si to v nich zaslouží, a tam zůstaňte, dokud se nevydáte na další cestu. Když vstoupíte do domu, pozdravte ho (přáním pokoje)! A když si to ten dům zaslouží, ať na něm spočine váš pokoj; když si to nezaslouží, ať se váš pokoj vrátí k vám. A když vás někdo nepřijme a nebude na vaše slova dbát, při odchodu z toho domu nebo města si vytřeste prach ze svých nohou. Amen, pravím vám: V den soudu bude lehčeji zemi sodomské a gomorské než takovému městu.“

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-07-08/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-07-08/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-07-08/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-07-08/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "green")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
