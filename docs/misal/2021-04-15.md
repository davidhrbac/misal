---
title: '2021-04-15'
---
# Čtvrtek po 2. neděli velikonoční

## 1. čtení – Sk 5,27-33

*Čtení ze Skutků apoštolů*

Když (služebníci) přivedli (apoštoly) a postavili je před veleradu, začal je velekněz vyslýchat: „Přísně jsme vám přece přikázali, že v tom jménu už nesmíte učit. Přesto však Jeruzalém je plný toho vašeho učení a chcete na nás přivolat pomstu za krev onoho člověka!“ Ale Petr a ostatní apoštolové na to řekli: „Více je třeba poslouchat Boha než lidi. Bůh našich otců vzkřísil Ježíše, když vy jste ho pověsili na dřevo a zabili. Ale Bůh ho povýšil po své pravici jako vůdce a spasitele, aby Izraeli dopřál obrácení a odpuštění hříchů. A my jsme svědky těchto událostí, stejně i Duch Svatý, kterého Bůh dal těm, kdo ho poslouchají.“ Když to uslyšeli, rozzuřili se a rozhodli, že je sprovodí ze světa.

## Mezizpěv – Žl 34,2+9.17-18.19-20

Ubožák zavolal, a Hospodin slyšel.

## Zpěv před evangeliem – Jan 20,29

Aleluja. Protože jsi mě uviděl, Tomáši, uvěřil jsi, praví Pán; blahoslavení, kdo neviděli, a uvěřili. Aleluja.

## Evangelium – Jan 3,31-36

*Slova svatého evangelia podle Jana*

Kdo přichází shora, je nade všemi; kdo pochází ze země, je pozemský a mluví pozemsky. Kdo přichází z nebe, je nade všemi; svědčí o tom, co viděl a slyšel, ale jeho svědectví nikdo nepřijímá. Kdo však jeho svědectví přijal, dotvrdil tím, že Bůh je pravdivý. Vždyť ten, kterého poslal Bůh, mluví slova Boží; (Bůh) mu totiž dává Ducha v míře neomezené. Otec miluje Syna a všechno mu dal do rukou. Kdo věří v Syna, má život věčný; kdo však odpírá věřit v Syna, nespatří život, ale zůstává na něm Boží hněv.

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-04-15.json](/api/v1/2021-04-15.json)
    * YAML - [2021-04-15.yaml](/api/v1/2021-04-15.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-04-15.json](/api/v1/olejnik/2021-04-15.json)
    * YAML - [olejnik/2021-04-15.yaml](/api/v1/olejnik/2021-04-15.yaml)

### Pravopis

!!! failure ""
    {--*slyšelvysvobodil*--}: slyšel vysvobodil, slyšel-vysvobodil<br>

### Typografie

!!! bug ""
    Ustavičně chci velebit Hospodin{--*a,v[m[Kždy bude v mých ústech jeho chvál[01;31m[Ka.O[m[Kkuste a vizte, jak je Hospodin dobr[01;31m[Ký,b*--}laze člověku, který se k němu utíká.
    Hospodinův hněv stíhá ty, kdo páchají zl{--*o,a*--}by vyhladil ze země vzpomínku na ně.(Spravedliví) volali, a Hospodin slyšelvysvobodil je z každé jejich tísně.
    Blízko je Hospodin těm, kdo mají zkroušené srdc{--*e,n[m[Ka duchu zlomené zachraňuj[01;31m[Ke.S[m[Kpravedlivý mívá mnoho soužen[01;31m[Kí,H*--}ospodin však ho ze všech vyprostí.

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-04-15/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-04-15/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-04-15/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-04-15/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "white")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
