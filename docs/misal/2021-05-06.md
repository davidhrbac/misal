---
title: '2021-05-06'
---
# Čtvrtek po 5. neděli velikonoční

## 1. čtení – Sk 15,7-21

*Čtení ze Skutků apoštolů*

Když se (o obřízce na sněmu) dlouho tak a onak rokovalo, vstal Petr a promluvil k (apoštolům a starším): „Jak víte, bratři, Bůh už dávno mezi vámi rozhodl, aby pohané uslyšeli slovo evangelia z mých úst, a tak aby přišli k víře. A Bůh, který zná srdce, dosvědčil jim (svou přízeň) tím, že jim udělil Ducha Svatého právě tak jako nám. Nedělal tedy žádný rozdíl mezi námi a mezi nimi, když jim vírou očistil srdce. Nuže, proč nyní pokoušíte Boha tím, že učedníkům navlékáte chomout, který nedovedli snést ani naši otcové, ani my? Vždyť jsme přesvědčeni, že spásy docházíme milostí Pána Ježíše my stejně jako oni.“ Celé shromáždění mlčelo a poslouchalo Barnabáše a Pavla, když podrobně vypravovali, jak veliká znamení a divy vykonal Bůh jejich prostřednictvím mezi pohany. Když přestali mluvit, ujal se slova Jakub: „Bratři, poslyšte mě! Šimon vypravoval, jak Bůh udělal první krok, když si chtěl z pohanů získat lid pro sebe. S tím souhlasí slova proroků. Je totiž psáno: `Potom znovu postavím rozpadlou Davidovu chýši, z trosek ji zase vystavím a do výše vyženu, aby i ostatní lidé hledali Pána, a všechny národy, které nesou jméno po mně. Tak praví Pán, který to vykoná. Je to rozhodnuto od věčnosti.' Proto já soudím toto: Nenakládejme pohanům, kteří se obracejí k Bohu, žádná zbytečná břemena. Jenom jim písemně nařiďme, aby se vyvarovali všeho, co je poskvrněno modlářstvím, (dále) smilstva, masa z udušených zvířat a požívání krve. Neboť Mojžíš má od dávných časů po městech své kazatele v synagogách, kde se předčítá každou sobotu.“

## Mezizpěv – Žl 96,1-2a.2b-3.10

Vypravujte mezi všemi národy o Hospodinových divech.

## Zpěv před evangeliem – Jan 10,27

Aleluja. Moje ovce slyší můj hlas, praví Pán, já je znám a ony jdou za mnou. Aleluja.

## Evangelium – Jan 15,9-11

*Slova svatého evangelia podle Jana*

Ježíš řekl svým učedníkům: „Jako Otec miloval mne, tak já jsem miloval vás. Zůstaňte v mé lásce. Zachováte-li moje přikázání, zůstanete v mé lásce, jako jsem já zachovával přikázání svého Otce a zůstávám v jeho lásce. To jsem k vám mluvil, aby moje radost byla ve vás a aby se vaše radost naplnila.“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-05-06.json](/api/v1/2021-05-06.json)
    * YAML - [2021-05-06.yaml](/api/v1/2021-05-06.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-05-06.json](/api/v1/olejnik/2021-05-06.json)
    * YAML - [olejnik/2021-05-06.yaml](/api/v1/olejnik/2021-05-06.yaml)

### Pravopis

!!! failure ""
    N/A

### Typografie

!!! bug ""
    Když se (o obřízce na sněmu) dlouho tak a onak rokovalo, vstal Petr a promluvil k (apoštolům a starším): „Jak víte, bratři, Bůh už dávno mezi vámi rozhodl, aby pohané uslyšeli slovo evangelia z mých úst, a tak aby přišli k víře. A Bůh, který zná srdce, dosvědčil jim (svou přízeň) tím, že jim udělil Ducha Svatého právě tak jako nám. Nedělal tedy žádný rozdíl mezi námi a mezi nimi, když jim vírou očistil srdce. Nuže, proč nyní pokoušíte Boha tím, že učedníkům navlékáte chomout, který nedovedli snést ani naši otcové, ani my? Vždyť jsme přesvědčeni, že spásy docházíme milostí Pána Ježíše my stejně jako oni.“ Celé shromáždění mlčelo a poslouchalo Barnabáše a Pavla, když podrobně vypravovali, jak veliká znamení a divy vykonal Bůh jejich prostřednictvím mezi pohany. Když přestali mluvit, ujal se slova Jakub: „Bratři, poslyšte mě! Šimon vypravoval, jak Bůh udělal první krok, když si chtěl z pohanů získat lid pro sebe. S tím souhlasí slova proroků. Je totiž psáno: {--*\`*--}Potom znovu postavím rozpadlou Davidovu chýši, z trosek ji zase vystavím a do výše vyženu, aby i ostatní lidé hledali Pána, a všechny národy, které nesou jméno po mně. Tak praví Pán, který to vykoná. Je to rozhodnuto od věčnosti.' Proto já soudím toto: Nenakládejme pohanům, kteří se obracejí k Bohu, žádná zbytečná břemena. Jenom jim písemně nařiďme, aby se vyvarovali všeho, co je poskvrněno modlářstvím, (dále) smilstva, masa z udušených zvířat a požívání krve. Neboť Mojžíš má od dávných časů po městech své kazatele v synagogách, kde se předčítá každou sobotu.“
    Zpívejte Hospodinu píseň novo{--*u,z*--}pívejte Hospodinu, všechny země!Zpívejte Hospodinu, velebte jeho jméno!
    Rozhlašujte den po dni jeho spás{--*u.V[m[Kypravujte mezi pohany o jeho sláv[01;31m[Kě,m*--}ezi všemi národy o jeho divech.
    Hlásejte mezi pohany: Hospodin kraluj{--*e.U*--}pevnil svět, aby nekolísal:národy řídí podle práva.

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-05-06/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-05-06/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-05-06/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-05-06/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "white")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
