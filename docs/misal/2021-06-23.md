---
title: '2021-06-23'
---
# Středa po 12. neděli v mezidobí

## 

null

null

## Mezizpěv – Žl 105,1-2.3-4.6-7.8-9

Hospodin pamatuje věčně na svoji smlouvu.

## Zpěv před evangeliem – Jan 15,4.5b

Aleluja. Zůstaňte ve mně a já zůstanu ve vás, praví Pán; kdo zůstává ve mně, ten nese mnoho ovoce. Aleluja.

## Evangelium – Mt 7,15-20

*Slova svatého evangelia podle Matouše*

Ježíš řekl svým učedníkům: „Mějte se na pozoru před nepravými proroky: přicházejí k vám převlečeni za ovce, ale uvnitř jsou draví vlci. Poznáte je po jejich ovoci. Copak se sklízejí z trní hrozny nebo z bodláků fíky? Tak každý dobrý strom nese dobré ovoce, ale špatný strom nese špatné ovoce. Dobrý strom nemůže nést špatné ovoce ani špatný strom nést dobré ovoce. Každý strom, který nenese dobré ovoce, bude poražen a hozen do ohně. Poznáte je tedy po jejich ovoci.“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-06-23.json](/api/v1/2021-06-23.json)
    * YAML - [2021-06-23.yaml](/api/v1/2021-06-23.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-06-23.json](/api/v1/olejnik/2021-06-23.json)
    * YAML - [olejnik/2021-06-23.yaml](/api/v1/olejnik/2021-06-23.yaml)

### Pravopis

!!! failure ""
    {--*Abráma*--}: Abrháma, Abraham, Abrahám, Bardáma, Barana, Barma, Bárám, Abrhám, Ambrám, Braná, Brána, Abrahama, Abraháma, Baráka, Bárami, Nabraná, Nabrána, Zabraná, Zabrána, Zábrana, Abrháme, Abrhámu, Abrhámy, Abrhámů, Ambrami, Babrám, Žábrám, Obrana, Obraná, Ubraná, Ubrána, Adama, Araba, Brada, Broma, Bráta, Agama, Aroma, Aróma, Aurám, Brala, Brává, Drama, Oubrama, Babrala, Babravá, Babráme, Nabrala, Zabrala, Žábrami, Čabraka, Abonmá, Abraze, Abrazi, Abrazí, Agrafa, Atrapa, Aurami, Horama, Obříma, Ubrala<br>
    {--*Abrám*--}: Bárám, Abrhám, Ambrám, Babrám, Žábrám, Aurám<br>
    {--*Abráme*--}: Abrháme, Babráme, Abraze<br>
    {--*Abrámem*--}: Abrhámem, Baranem, Abrháme, Braném, Šrámem, Abrahamem, Abrahámem, Barákem, Alarmem, Rámem, Ráměm, Nabraném, Zabraném, Abrhámům, Abrazemi, Babráme, Obraném, Ubraném, Adamem, Arabem, Bromem, Brátem, Abraze, Brahem, Brakem, Bravem, Brómem, Bříměm, Gramem, Gramém, Krámem, Prámem, Trámem, Oubramem, Babravém, Vibramem, Abraham, Abrahám, Abrazím, Chrámem, Obratem, Obrazem, Obřadem<br>
    {--*damašský*--}: Damašky, damašky, dam ašský, dam-ašský, Damašku, Damašků, damašku, damašků, dámsky, dámský, dalmatsky, dalmatský, lamanšsky, lamanšský, danajsky, danajský, šamansky, šamanský, Damajky, Damašek, Damaška, damašek, adamanský, jamajsky, jamajský, kazašsky, kazašský, lamačsky, lamačský, valašsky, valašský<br>
    {--*hrejte*--}: hřejte, hrkejte, hárejte, hřejete, hřmějte, hřeje, ohřejte, uhřejte, hrajte, hřejme, hřešte, orejte, prejte, přejte<br>
    {--*Uru*--}: Uhru, Uhrů, Udřu, Umřu, Upřu, Urnu, Urvu, Utřu, Uřvu, Iru, Irů, Búru, Búrů, Duru, Durů, Juru, Jurů, Puru, Purů, Rúru, Rúrů, Sůru, Sůrů, Auru, Euru, Fůru, Guru, Kuru, Kurů, Kůru, Kůrů, Kúru, Můru, Turu, Turů, Tůru, Túru, Šuru, Šurů, URL, Ufu, Aru, Arů, Dřu, Hru, Kru, Mřu, Ořů, Přu, Třu, Umu, Umů, Url, Usu, Usů, Uřl, Vřu, ÚRO, Úpu, Éru, Údu, Údů, Úlu, Úlů, Úrl, Úzu, Úzů<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni-gn-15-1-12-17-18.html](/api/v1/obs/2021-06-23/1-cteni-gn-15-1-12-17-18.html)
    * [mezizpev.html](/api/v1/obs/2021-06-23/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-06-23/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-06-23/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "green")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
