---
title: '2021-02-16'
---
# Úterý po 6. neděli v mezidobí

## 1. čtení – Gn 6,5-8; 7,1-5.10

*Čtení z první knihy Mojžíšovy*

Když Hospodin viděl, že mnoho je lidské špatnosti na zemi a že veškeré myšlení a snažení jejich srdce stále jen směřuje ke zlému, litoval Hospodin, že udělal lidi na zemi, a velmi se zarmoutil. Řekl: „Zahubím lidi, které jsem stvořil z povrchu země: s lidmi i dobytek, lezoucí havěť i nebeské ptactvo. Mrzí mě, že jsem je udělal.“ Noe však nalezl milost v Hospodinových očích. Hospodin řekl Noemovi: „Vstup do archy ty a celá tvá rodina, neboť (jenom) tebe jsem viděl spravedlivého před sebou mezi tímto pokolením. Ze všech čistých zvířat si vezmeš po sedmi, samce se samicí, a z nečistých zvířat po dvou, samce se samicí. Také z nebeských ptáků (vezmeš) po sedmi, samce se samicí, aby se uchovalo potomstvo na celé zemi. Neboť již za sedm dní sešlu na zem déšť po čtyřicet dní a čtyřicet nocí a zničím z povrchu země všechny tvory, které jsem stvořil.“ Noe tedy udělal všechno, co mu Hospodin poručil. Po sedmi dnech přišla na zem voda potopy.

## Mezizpěv – Žl 29,1a+2.3ac-4.3b+9b-10

Hospodin dá požehnání a pokoj svému lidu.

## Zpěv před evangeliem – Jan 14,23

Aleluja. Kdo mě miluje, bude zachovávat mé slovo, praví Pán, a můj Otec ho bude milovat a přijdeme k němu. Aleluja.

## Evangelium – Mk 8,14-21

*Slova svatého evangelia podle Marka*

(Když Ježíš a jeho učedníci odešli od farizeů,) zapomněli si vzít chleby a kromě jednoho chleba neměli s sebou na lodi nic. Ježíš je napomínal: „Dejte si pozor a varujte se kvasu farizejského a kvasu herodovského!“ Ale oni uvažovali mezi sebou o tom, že nemají chleba. On to poznal a řekl jim: „O čem uvažujete? Že nemáte chleba? Ještě nerozumíte ani nechápete? Máte zatvrzelé srdce? Oči máte, a nevidíte, uši máte, a neslyšíte! Nevzpomínáte si už, když jsem rozlámal těch pět chlebů pro pět tisíc (lidí), kolik košů plných kousků chleba jste ještě nasbírali?“ Odpověděli mu: „Dvanáct.“ „A když těch sedm pro čtyři tisíce, kolik košíků plných kousků chleba jste ještě nasbírali?“ Odpověděli mu: „Sedm.“ Tu jim řekl: „Ještě nechápete?“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-02-16.json](/api/v1/2021-02-16.json)
    * YAML - [2021-02-16.yaml](/api/v1/2021-02-16.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-02-16.json](/api/v1/olejnik/2021-02-16.json)
    * YAML - [olejnik/2021-02-16.yaml](/api/v1/olejnik/2021-02-16.yaml)

### Pravopis

!!! failure ""
    {--*herodovského*--}: neRodovského, Rodovského, hodovského, hronovského, nerudovského<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-02-16/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-02-16/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-02-16/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-02-16/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "green")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
