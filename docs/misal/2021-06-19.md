---
title: '2021-06-19'
---
# Sobota po 11. neděli v mezidobí

## 1. čtení – 2 Kor 12,1-10

*Čtení z druhého listu svatého apoštola Pavla Korinťanům*

(Bratři!) Když už to chlubení musí být - ač to není k ničemu - přejdu k viděním a zjevením od Pána. Znám jednoho křesťana, který byl před čtrnácti lety uchvácen až do třetího nebe. Nevím, zdali byl v těle, nevím, zdali byl mimo tělo. To ví Bůh. A vím o tom člověku, že byl uchvácen do ráje - zdali byl v těle, či mimo tělo, to nevím, to ví Bůh - a že uslyšel slova nevyslovitelná, která člověk nesmí vyřknout. Tímto člověkem se budu chlubit; sebou se však chlubit nebudu, leda svými slabostmi. Kdybych se totiž chtěl pochlubit, nebudu nerozumný, protože budu mluvit pravdu. Ale nechám toho, aby mě někdo nepokládal za něco více, než co vidí, že jsem, anebo než co o mně slyší. Abych se pro vznešenost těch zjevení nepyšnil, byl mi dán do těla osten, posel to satanův, aby mě bil (do tváře). To proto, aby se mně nezmocňovala pýcha. Kvůli tomu jsem třikrát prosil Pána, aby mě toho zbavil. Ale on mi řekl: "Stačí ti moje milost, protože síla se tím zřejměji projeví ve slabosti." Velmi rád se tedy budu chlubit spíše svými slabostmi, aby na mně spočinula Kristova moc. Proto s radostí přijímám slabosti, příkoří, nouzi, pronásledování a úzkosti (a snáším to) pro Krista. Neboť když jsem slabý, právě tehdy jsem silný.

## Mezizpěv – Žl 34,8-9.10-11.12+15

Okuste a vizte, jak je hospodin dobrý.

## Zpěv před evangeliem – 2 Kor 8,9

Aleluja. Ježíš Kristus stal se chudým, ačkoli byl bohatý, abyste vy zbohatli z jeho chudoby. Aleluja.

## 

null

null

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-06-19.json](/api/v1/2021-06-19.json)
    * YAML - [2021-06-19.yaml](/api/v1/2021-06-19.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-06-19.json](/api/v1/olejnik/2021-06-19.json)
    * YAML - [olejnik/2021-06-19.yaml](/api/v1/olejnik/2021-06-19.yaml)

### Pravopis

!!! failure ""
    {--*hospodin*--}: Hospodin, Hospodina, Hospodine, Hospodinu, Hospodiny, Hospodinů, Hospozín, hospodyň, ho spodin, ho-spodin, hospod in, hospod-in<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-06-19/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-06-19/mezizpev.html)
    * [evangelium-mt-6-24-34.html](/api/v1/obs/2021-06-19/evangelium-mt-6-24-34.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-06-19/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "green")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
