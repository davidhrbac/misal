---
title: '2021-07-09'
---
# Pátek po 14. neděli v mezidobí

## 

null

null

## Mezizpěv – Žl 37,3-4.18-19.27-28.39-40

Spravedlivým přichází spása od Hospodina.

## Zpěv před evangeliem – Jan 16,13a; 14,26b

Aleluja. Až přijde Duch pravdy, uvede vás do celé pravdy a připomene vám všechno, co jsem vám řekl já. Aleluja.

## Evangelium – Mt 10,16-23

*Slova svatého evangelia podle Matouše*

Ježíš řekl svým apoštolům: „Já vás posílám jako ovce mezi vlky! Buďte tedy opatrní jako hadi a bezelstní jako holubice. Mějte se na pozoru před lidmi! Budou vás totiž vydávat soudům a bičovat v synagogách, budou vás vodit před vladaře a krále kvůli mně, abyste vydali svědectví jim a také pohanům. Až vás vydají soudu, nedělejte si starosti, jak nebo co máte mluvit, protože v tu chvíli vám bude dáno, co máte mluvit. Neboť to už pak nemluvíte vy, ale mluví skrze vás Duch vašeho Otce. Bratr vydá na smrt bratra a otec syna, děti povstanou proti rodičům a způsobí jim smrt. Budete ode všech nenáviděni pro mé jméno. Ale kdo vytrvá až do konce, bude spasen. Když vás budou pronásledovat v jednom městě, utečte do jiného. Amen, pravím vám: Nebudete hotovi s izraelskými městy, než přijde Syn člověka.“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-07-09.json](/api/v1/2021-07-09.json)
    * YAML - [2021-07-09.yaml](/api/v1/2021-07-09.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-07-09.json](/api/v1/olejnik/2021-07-09.json)
    * YAML - [olejnik/2021-07-09.yaml](/api/v1/olejnik/2021-07-09.yaml)

### Pravopis

!!! failure ""
    {--*Beršeby*--}: Jersey, Besedy, Berbery, Bergery, Bernety<br>
    {--*Gošenu*--}: Kosenu, Nošenu, Rosenu<br>
    {--*Izraelovi*--}: Izraelcovi, Izraelcovo, Izraelcovu, Izraelci, Ožralovi, Izraeli, Izraelí, Izraelitovi, Izraelcova, Izraelcovy, Izraelcově, Izraelemi, Izraelští<br>
    {--*Kanaánu*--}: Kanaďanu, Kanaďanů, Kankánu, Kankánů<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni-gn-46-1-7-28-30.html](/api/v1/obs/2021-07-09/1-cteni-gn-46-1-7-28-30.html)
    * [mezizpev.html](/api/v1/obs/2021-07-09/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-07-09/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-07-09/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "green")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
