---
title: '2021-06-29'
---














## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-06-29.json](/api/v1/2021-06-29.json)
    * YAML - [2021-06-29.yaml](/api/v1/2021-06-29.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-06-29.json](/api/v1/olejnik/2021-06-29.json)
    * YAML - [olejnik/2021-06-29.yaml](/api/v1/olejnik/2021-06-29.yaml)

### Pravopis

!!! failure ""
    N/A

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [olejnik/mezizpev.html](/api/v1/obs/2021-06-29/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
