---
title: '2021-05-11'
---
# Úterý po 6. neděli velikonoční

## 1. čtení – Sk 16,22-34

*Čtení ze Skutků apoštolů*

z nich dali strhat šaty a nařídili bít je pruty. Když pak jim vysázeli hodně ran, dali je zavřít do vězení a žalářníkovi nařídili, aby je dobře hlídal. Když dostal takový rozkaz, dal je do nejhlubší kobky a pro jistotu jim sevřel nohy do klády. Kolem půlnoci se Pavel a Silas modlili a zpívali Bohu chvalozpěvy. (Ostatní) vězni je poslouchali. Náhle však nastalo silné zemětřesení, takže se zachvěly základy žaláře. A ihned se rozevřely všechny dveře a všem se uvolnila pouta. Když se žalářník probral ze spánku a viděl, že dveře vězení jsou dokořán, vytasil meč a chtěl si vzít život, neboť myslel, že vězni utekli. Ale Pavel zavolal silným hlasem: „Nedělej si nic zlého! Vždyť jsme tady všichni!“ Žalářník si dal tedy přinést světlo, vběhl dovnitř, celý se třásl a padl na kolena před Pavlem a Silou. Pak je vyvedl ven a zeptal se: „Pánové, co mám dělat, abych došel spásy?“ Oni odpověděli: „Uvěř v Pána Ježíše, a dojdeš spásy ty i celý tvůj dům.“ A začali hlásat slovo Boží jemu i všem lidem z jeho domu. Ještě v noci v tu hodinu je vzal s sebou, vymyl jim krvavé rány a hned se dal pokřtít on i všichni jeho lidé. Pak je uvedl do svého bytu, dal prostřít stůl a jásal nad tím, že on i celý jeho dům uvěřil v Boha.

## Mezizpěv – Žl 138,1-2a.2bc-3.7c-8

Zachraňuje mě tvá pravice, Hospodine.

## Zpěv před evangeliem – srov. Jan 16,7.13

Aleluja. Pošlu k vám Ducha pravdy, praví Pán; on vás uvede do celé pravdy. Aleluja.

## Evangelium – Jan 16,5-11

*Slova svatého evangelia podle Jana*

Ježíš řekl svým učedníkům: „Nyní jdu k tomu, který mě poslal, a nikdo z vás se mě neptá: `Kam jdeš?' Spíše je vaše srdce plné zármutku, že jsem vám to pověděl. Ale já vám říkám pravdu: Je to pro vás dobré, abych já odešel. Jestliže totiž neodejdu, Přímluvce k vám nepřijde. Odejdu-li však, pošlu ho k vám. A on, až přijde, usvědčí svět ze hříchu, ze spravedlnosti a ze soudu. Ze hříchu: že ve mne nevěří; ze spravedlnosti: že odcházím k Otci a už mě neuvidíte; ze soudu: že vládce tohoto světa je už odsouzen.“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-05-11.json](/api/v1/2021-05-11.json)
    * YAML - [2021-05-11.yaml](/api/v1/2021-05-11.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-05-11.json](/api/v1/olejnik/2021-05-11.json)
    * YAML - [olejnik/2021-05-11.yaml](/api/v1/olejnik/2021-05-11.yaml)

### Pravopis

!!! failure ""
    {--*Silas*--}: Spíláš, Šilháš, Šišláš, Sila, Síla, Šíla, Šila, Salaš, Silan, Silák, Silám, Siláž, Sináš, Splaš, Sáláš, Sílám, Sílíš, Sípáš, Síváš, Šilar, Šílíš, Šíváš, Si las, Si-las<br>
    {--*Silovi*--}: Siloví, Síloví, Šílovi, Silkoví, Siltoví, Šiklovi, Šilcovi, Šídlovi, Šimlovi, Sikovi, Díloví, Síliví, Sóloví, Šulovi, Šílovo, Šílovu, Šůlovi, Šloví, Cílovi, Mílovi, Simovi, Cíloví, Jíloví, Kiloví, Míloví, Piloví, Silová, Silové, Silově, Silový, Sinoví, Sirovi, Sáloví, Sílová, Sílové, Sílově, Sílový, Síroví, Sítoví, Síťoví, Síňoví, Viloví, Šídovi, Šílova, Šílovy, Šílová, Šílové, Šílově, Šímovi, Šínovi, Šípovi, Šírovi, Žílovi, Šáloví, Šíjoví, Šípoví, Žiloví, Čílovi<br>
    {--*vyslyšels*--}: vyslyšela, vyslyšel, vyslyšeli, vyslyšelo, vyslyšely<br>

### Typografie

!!! bug ""
    Ježíš řekl svým učedníkům: „Nyní jdu k tomu, který mě poslal, a nikdo z vás se mě neptá: {--*\`*--}Kam jdeš?' Spíše je vaše srdce plné zármutku, že jsem vám to pověděl. Ale já vám říkám pravdu: Je to pro vás dobré, abych já odešel. Jestliže totiž neodejdu, Přímluvce k vám nepřijde. Odejdu-li však, pošlu ho k vám. A on, až přijde, usvědčí svět ze hříchu, ze spravedlnosti a ze soudu. Ze hříchu: že ve mne nevěří; ze spravedlnosti: že odcházím k Otci a už mě neuvidíte; ze soudu: že vládce tohoto světa je už odsouzen.“

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-05-11/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-05-11/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-05-11/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-05-11/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "white")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
