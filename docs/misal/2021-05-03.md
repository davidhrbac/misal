---
title: '2021-05-03'
---
# Svátek sv. Filipa a Jakuba, apoštolů

## 1. čtení – 1 Kor 15,1-8

*Čtení z prvního listu svatého apoštola Pavla Korinťanům*

Chci vám, bratři, vyložit radostnou zvěst, kterou jsem vám už hlásal. Vy jste ji přijali a jste v tom pevní. Ona vás vede ke spáse, když se jí držíte přesně tak, jak jsem vám to kázal; jinak jste uvěřili nadarmo. Vyučil jsem vás především v tom, co jsem sám přijal, že Kristus umřel ve shodě s Písmem za naše hříchy; že byl pohřben a že vstal z mrtvých třetího dne ve shodě s Písmem; že se ukázal Petrovi a potom Dvanácti. Pak se zjevil více než pěti stům bratřím najednou – většina z nich dosud žije, někteří však už zesnuli. Potom se zjevil Jakubovi, pak všem apoštolům. A po všech jako poslední jsem ho uviděl i já, nedochůdče.

## Mezizpěv – Žl 19,2-3.4-5

Všude na zemi pronikl jejich hlas.

## Zpěv před evangeliem – Jan 14,6b.9c

Aleluja. Já jsem cesta, pravda a život, praví Pán; Filipe, kdo viděl mne, viděl Otce. Aleluja.

## Evangelium – Jan 14,6-14

*Slova svatého evangelia podle Jana*

Ježíš řekl Tomášovi: „Já jsem cesta, pravda a život. Nikdo nepřichází k Otci než skrze mne. Kdybyste znali mne, znali byste i mého Otce. Nyní ho už znáte a viděli jste ho.“ Filip mu řekl: „Pane, ukaž nám Otce – a to nám stačí.“ Ježíš mu odpověděl: „Filipe, tak dlouho jsem s vámi, a neznáš mě? Kdo viděl mne, viděl Otce. Jak můžeš říci: `Ukaž nám Otce'? Nevěříš, že já jsem v Otci a Otec je ve mně? Slova, která k vám mluvím, nemluvím sám ze sebe; to Otec, který ve mně přebývá, koná své skutky. Věřte mi, že já jsem v Otci a Otec ve mně. Když nevěříte, věřte aspoň pro ty skutky. Amen, amen, pravím vám: Kdo věří ve mne, i ten bude konat skutky, které já konám, ba ještě větší bude konat, protože já odcházím k Otci. A za cokoli budete prosit ve jménu mém, to všechno udělám, aby Otec byl oslaven v Synovi. Budete-li mě o něco prosit ve jménu mém, já to udělám.“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-05-03.json](/api/v1/2021-05-03.json)
    * YAML - [2021-05-03.yaml](/api/v1/2021-05-03.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-05-03.json](/api/v1/olejnik/2021-05-03.json)
    * YAML - [olejnik/2021-05-03.yaml](/api/v1/olejnik/2021-05-03.yaml)

### Pravopis

!!! failure ""
    N/A

### Typografie

!!! bug ""
    Ježíš řekl Tomášovi: „Já jsem cesta, pravda a život. Nikdo nepřichází k Otci než skrze mne. Kdybyste znali mne, znali byste i mého Otce. Nyní ho už znáte a viděli jste ho.“ Filip mu řekl: „Pane, ukaž nám Otce – a to nám stačí.“ Ježíš mu odpověděl: „Filipe, tak dlouho jsem s vámi, a neznáš mě? Kdo viděl mne, viděl Otce. Jak můžeš říci: {--*\`*--}Ukaž nám Otce'? Nevěříš, že já jsem v Otci a Otec je ve mně? Slova, která k vám mluvím, nemluvím sám ze sebe; to Otec, který ve mně přebývá, koná své skutky. Věřte mi, že já jsem v Otci a Otec ve mně. Když nevěříte, věřte aspoň pro ty skutky. Amen, amen, pravím vám: Kdo věří ve mne, i ten bude konat skutky, které já konám, ba ještě větší bude konat, protože já odcházím k Otci. A za cokoli budete prosit ve jménu mém, to všechno udělám, aby Otec byl oslaven v Synovi. Budete-li mě o něco prosit ve jménu mém, já to udělám.“

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-05-03/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-05-03/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-05-03/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-05-03/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "red")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
