---
title: '2021-07-11'
---
# 15. NEDĚLE V MEZIDOBÍ

## 

null

null

## Mezizpěv – Žl 85,9ab+10.11-12.13-14

Pane, ukaž nám své milosrdenství!

## 2. čtení – Ef 1,3-14

*Čtení z listu svatého apoštola Pavla Efesanům*

Buď pochválen Bůh a Otec našeho Pána Ježíše Krista, on nás zahrnul z nebe rozmanitými duchovními dary, protože jsme spojeni s Kristem. Vždyť v něm si nás vyvolil ještě před stvořením světa, abychom byli před ním svatí a neposkvrnění v lásce; ze svého svobodného rozhodnutí nás předurčil, abychom byli přijati za jeho děti skrze Ježíše Krista. (To proto,) aby se vzdávala chvála jeho vznešené dobrotivosti, neboť skrze ni nás obdařil milostí ve svém milovaném (Synu). V něm máme vykoupení skrze jeho krev, odpuštění hříchů pro jeho nesmírnou milost, kterou nám tak bohatě projevil s veškerou moudrostí a prozíravostí: seznámil nás totiž s tajemstvím své vůle, jak se mu to líbilo a jak si to napřed sám u sebe ustanovil, až se naplní čas pro dílo spásy: že sjednotí v Kristu vše, co je na nebi i na zemi. A skrze něho jsme se stali Božím majetkem, jak jsme k tomu byli předem určeni úradkem toho, který vše působí podle rozhodnutí své vůle. (Tak máme sloužit k tomu,) aby se šířila chvála o jeho božské velebnosti, my, kteří jsme už dříve kladli své naděje do Mesiáše. Skrze něho se dostalo i vám potvrzení od slíbeného Ducha Svatého, když jste přijali slovo pravdy, radostnou zvěst o své spáse, a když jste v něho uvěřili. (Duch) je zárukou, že nám jednou připadne dědictví. (Tak se dovrší) naše vykoupení, (protože si nás) Bůh získal jako svůj majetek, (abychom sloužili) ke chvále jeho božské velebnosti.

## Zpěv před evangeliem – srov. Ef 1,17-18

Aleluja. Otec našeho Pána Ježíše Krista ať osvítí naše srdce, abychom pochopili, jaká je naděje těch, které on povolal. Aleluja.

## Evangelium – Mk 6,7-13

*Slova svatého evangelia podle Marka*

(Ježíš) zavolal svých Dvanáct, začal je posílat po dvou a dával jim moc nad nečistými duchy. Nařídil jim, aby si na cestu nic nebrali, jen hůl: ani chléb, ani mošnu, ani peníze do opasku, jen opánky na nohy, ani aby si neoblékali dvoje šaty. Řekl jim: „Když přijdete někam do domu, zůstávejte tam, dokud se odtamtud nevydáte zase dál. Když vás však na některém místě nepřijmou a nebudou vás chtít slyšet, při odchodu odtamtud si vytřeste prach ze svých nohou na svědectví proti nim.“ Vydali se tedy na cesty a hlásali, že je třeba se obrátit. Vyháněli mnoho zlých duchů, pomazávali olejem mnoho nemocných a uzdravovali je.

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-07-11.json](/api/v1/2021-07-11.json)
    * YAML - [2021-07-11.yaml](/api/v1/2021-07-11.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-07-11.json](/api/v1/olejnik/2021-07-11.json)
    * YAML - [olejnik/2021-07-11.yaml](/api/v1/olejnik/2021-07-11.yaml)

### Pravopis

!!! failure ""
    {--*Amosa*--}: Mosa, Ámos, Kámoša, Amora, Ámose, Ámosi, Ámosů<br>
    {--*Amos*--}: Ámos, Ámose, Ámosi, Ámosů, MOS, Kámoš, Amor, CMOS, Amok<br>
    {--*Amosovi*--}: Ámosovi, Ámosovo, Ámosovu, Kámošovi, Amorovi, Amokoví, Ámosova, Ámosovy, Ámosové, Ámosově<br>

### Typografie

!!! bug ""
    Amasjáh, kněz v Betelu, řekl Amosovi: „Vidoucí, seber se a uteč do judské země, tam se živ a tam prorokuj! V Betelu už prorokovat nesmíš, neboť zde je králova svatyně a říšský chrám.“ Amos odpověděl Amasjáhovi: „Nejsem prorok ani prorocký učedník, jsem pastýř a pěstitel smokvoní. Hospodin mě vzal od stáda a řekl mi: {--*\`*--}Jdi a prorokuj mému izraelskému lidu!'„

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni-am-7-12-15.html](/api/v1/obs/2021-07-11/1-cteni-am-7-12-15.html)
    * [mezizpev.html](/api/v1/obs/2021-07-11/mezizpev.html)
    * [2-cteni.html](/api/v1/obs/2021-07-11/2-cteni.html)
    * [evangelium.html](/api/v1/obs/2021-07-11/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-07-11/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "green")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
