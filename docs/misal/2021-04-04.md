---
title: '2021-04-04'
---
# Slavnost Zmrtvýchvstání Páně 
V den slavnosti

## 1. čtení – Sk 10,34a.37-43

*Čtení ze Skutků apoštolů*

Petr se ujal slova a promluvil: „Vy víte, co se po křtu, který hlásal Jan, událo nejdříve v Galileji a potom po celém Judsku: Jak Bůh pomazal Duchem svatým a mocí Ježíše z Nazareta, jak on všude procházel, prokazoval dobrodiní, a protože Bůh byl s ním, uzdravoval všechny, které opanoval ďábel. A my jsme svědky všeho toho, co konal v Judsku a v Jeruzalémě. Ale pověsili ho na dřevo a zabili. Bůh jej však třetího dne vzkřísil a dal mu, aby se viditelně ukázal, ne všemu lidu, ale jen těm, které Bůh předem vyvolil za svědky, totiž nám, kteří jsme s ním jedli a pili po jeho zmrtvýchvstání. On nám přikázal, abychom hlásali lidu a se vší rozhodností dosvědčovali: To je Bohem ustanovený soudce nad živými i mrtvými. O něm vydávají svědectví všichni proroci, že skrze něho dostane odpuštění hříchů každý, kdo v něho věří.“

## Mezizpěv – Žl 118,1-2.16ab+17.22-23

Toto je den, který učinil Hospodin, jásejme a radujme se z něho!

## 2. čtení – Kol 3,1-4

*Čtení z listu svatého apoštola Pavla Kolosanům*

(Bratři!) Když jste s Kristem byli vzkříšeni, usilujte o to, co (pochází) shůry, kde je Kristus po Boží pravici. Na to myslete, co (pochází) shůry, ne na to, co je na zemi. Jste přece už mrtví a váš život je s Kristem skrytý v Bohu. Ale až se ukáže Kristus, náš život, potom se i vy s ním ukážete ve slávě.

## Zpěv před evangeliem – 1 Kor 5,7b-8a

Aleluja. Kristus, náš velikonoční beránek, je obětován, proto slavme svátky s Pánem. Aleluja.

## Evangelium – Jan 20,1-9

*Slova svatého evangelia podle Jana*

Prvního dne v týdnu přišla Marie Magdalská časně ráno ještě za tmy ke hrobu a viděla, že je kámen od hrobu odstraněn. Běžela proto k Šimonu Petrovi a k tomu druhému učedníkovi, kterého Ježíš miloval, a řekla jim: „Vzali Pána z hrobu a nevíme, kam ho položili.“ Petr a ten druhý učedník tedy vyšli a zamířili ke hrobu. Oba běželi zároveň, ale ten druhý učedník byl rychlejší než Petr a doběhl k hrobu první. Naklonil se dovnitř a viděl, že tam leží (pruhy) plátna, ale dovnitř nevešel. Pak za ním přišel i Šimon Petr, vešel do hrobky a viděl, že tam leží (pruhy) plátna. Rouška však, která byla na Ježíšově hlavě, neležela u těch (pruhů) plátna, ale složená zvlášť na jiném místě. Potom vstoupil i ten druhý učedník, který přišel ke hrobu první, viděl a uvěřil. Ještě totiž nerozuměli Písmu, že Ježíš musí vstát z mrtvých.

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-04-04.json](/api/v1/2021-04-04.json)
    * YAML - [2021-04-04.yaml](/api/v1/2021-04-04.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-04-04.json](/api/v1/olejnik/2021-04-04.json)
    * YAML - [olejnik/2021-04-04.yaml](/api/v1/olejnik/2021-04-04.yaml)

### Pravopis

!!! failure ""
    {--*Galileje*--}: Galileem, Galeje, Galilejské, Galileové, Galileově, Galilea, Galileo, Galileu, Galiley, Galileů, Basileje, Galileům, Galileův<br>
    {--*Galileji*--}: Galileo, Galileu, Galileů, Galejí, Galilejští, Galileovi, Galiemi, Galilea, Galiley, Basileji, Basilejí, Galileem, Galileům, Galileův, Nalitěji, Pálivěji, Zalitěji, Zavileji, Šálivěji<br>
    {--*Izraelův*--}: Izraelcův, Izraelů, Izraelům<br>
    {--*Kolosanům*--}: Kolosálním, Kolísáním, Olšanům, Kolosům, Kolotáním, Kolováním, Klokanům, Kolmanům, Koloseum, Kolesárům, Koloradům, Komořanům, Vološinům, Kolomazům<br>
    {--*Magdalská*--}: Magdalenka, Maršálská, Maďarska, Maďarská, Magdalena, Magdaléna, Bagdádská, Magnátská, Vandalská<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-04-04/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-04-04/mezizpev.html)
    * [2-cteni.html](/api/v1/obs/2021-04-04/2-cteni.html)
    * [sekvence.html](/api/v1/obs/2021-04-04/sekvence.html)
    * [evangelium.html](/api/v1/obs/2021-04-04/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-04-04/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "white")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
