---
title: '2021-04-09'
---
# Pátek v oktávu velikonočním

## 1. čtení – Sk 4,1-12

*Čtení ze Skutků apoštolů*

Zatímco (po uzdravení chromého) Petr a Jan mluvili k lidu, přišli na ně kněží, velitel chrámové stráže a saduceové. Nelibě nesli, že učí lid a hlásají, že v Ježíšovi je vzkříšení z mrtvých. Proto je dali zatknout a uvěznit do příštího dne, neboť už byl večer. Ale mnoho z těch, kdo tu řeč uslyšeli, přijalo víru, takže počet mužů stoupl asi na pět tisíc. Druhý den se shromáždili v Jeruzalémě židovští přední mužové, starší a učitelé Zákona. Byl tam i velekněz Annáš, Kaifáš, Jan, Alexandr a všichni, kteří byli z velekněžského rodu. Dali je předvést a začali je vyslýchat: „Čí mocí anebo čím jménem jste to udělali?“ Tu jim Petr, naplněn Duchem Svatým, odpověděl: „Přední mužové v lidu a starší! Když se dnes musíme odpovídat z dobrého skutku na nemocném člověku, kým že byl uzdraven, tedy ať to víte vy všichni a celý izraelský národ: Ve jménu Ježíše Krista Nazaretského, kterého jste vy ukřižovali, ale kterého Bůh vzkřísil z mrtvých: skrze něho stojí tento člověk před vámi zdravý. On je ten 'kámen, který jste vy stavitelé odhodili, ale z kterého se stal kvádr nárožní'. V nikom jiném není spásy. Neboť pod nebem není lidem dáno žádné jiné jméno, v němž bychom mohli dojít spásy.“

## Mezizpěv – Žl 118,1-2+4.22-24.25-27a

Kámen, který stavitelé zavrhli, stal se kvádrem nárožním.

## Zpěv před evangeliem – Žl 118,24

Aleluja. Toto je den, který učinil Hospodin, jásejme a radujme se z něho. Aleluja.

## Evangelium – Jan 21,1-14

*Slova svatého evangelia podle Jana*

Ježíš se znovu zjevil svým učedníkům, (a to) u Tiberiadského moře. Zjevil se takto: Byli pohromadě Šimon Petr, Tomáš zvaný Blíženec, Natanael z galilejské Kány, synové Zebedeovi a ještě jiní dva z jeho učedníků. Šimon Petr jim řekl: „Půjdu lovit ryby.“ Odpověděli mu: „I my půjdeme s tebou.“ Vyšli tedy a vstoupili na loď, ale tu noc nic nechytili. Když už nastávalo ráno, stál Ježíš na břehu, ale učedníci nevěděli, že je to on. Ježíš se jich zeptal: „Dítky, nemáte něco k jídlu?“ Odpověděli mu: „Nemáme.“ On jim řekl: „Hoďte síť na pravou stranu lodi, a najdete.“ Hodili ji tedy, a nemohli ji už ani utáhnout pro množství ryb. Tu onen učedník, kterého Ježíš miloval, řekl Petrovi: „Pán je to!“ Jakmile Šimon Petr uslyšel, že je to Pán, přehodil přes sebe svrchní šaty – byl totiž oblečen jen nalehko – a skočil do moře. Ostatní učedníci dojeli s lodí – nebyli od země daleko, jen tak asi dvě stě loket, a táhli síť s rybami. Když vystoupili na zem, viděli tam žhavé uhlí a na něm položenou rybu a (vedle) chléb. Ježíš jim řekl: „Přineste několik ryb, které jste právě chytili.“ Šimon Petr vystoupil a táhl na zem síť plnou velkých ryb, (bylo jich) stotřiapadesát. A přesto, že jich bylo tolik, síť se neprotrhla. Ježíš je vyzval: „Pojďte snídat!“ Nikdo z učedníků se ho neodvážil zeptat: „Kdo jsi!“ Věděli, že je to Pán. Ježíš přistoupil, vzal chléb a dal jim, stejně i rybu. To bylo už potřetí, co se Ježíš zjevil učedníkům po svém zmrtvýchvstání.

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-04-09.json](/api/v1/2021-04-09.json)
    * YAML - [2021-04-09.yaml](/api/v1/2021-04-09.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-04-09.json](/api/v1/olejnik/2021-04-09.json)
    * YAML - [olejnik/2021-04-09.yaml](/api/v1/olejnik/2021-04-09.yaml)

### Pravopis

!!! failure ""
    {--*Annáš*--}: Ananas, Anna, Annám<br>
    {--*Izraelův*--}: Izraelcův, Izraelů, Izraelům<br>
    {--*Kaifáš*--}: Kanafas, Kliďas, Klofáš, Kaina, Kalaš, Karas, Bafáš, Hafáš, Kakáš, Kapáš, Kasáš, Káráš, Rafáš, Ňafáš, Kainar, Kalfus, Kansas, Karfus, Kaňkáš, Klikáš, Klímáš, Kmiháš, Kmitáš, Kvikáš, Kvíkáš<br>
    {--*Kány*--}: Kainy, Kanty, Kaňky, Klány, Kárný, Kalný, Kamny, Kanýr, Kašny, Klany, Kmány, Kyany, Jany, Kant, Kaňu, Kaňů, Káby, Káňu, Káňů, Kanu, Lany, Lány, Kaň, Skaný, Tkaný, Tkány, Dany, Dány, Hany, Hány, Kaly, Kani, Kaňa, Kaňo, Kony, Kuny, Káni, Káry, Káňa, Káňo, Many, Pany, Pány, Daný, Kana, Kane, Kary, Kasy, Katy, Kazy, Kiny, Kály, Káně, Kání, Kávy, Raný, Rány, Taný, Tány, Vany, Wany<br>
    {--*Natanael*--}: Naháněl, Natahal, Natankl, Natanče, Natáčel, Naťapal, Natančen, Natančil<br>
    {--*stotřiapadesát*--}: sto třiapadesát, sto-třiapadesát<br>
    {--*Zebedeovi*--}: Lebedovi<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-04-09/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-04-09/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-04-09/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-04-09/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "white")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
