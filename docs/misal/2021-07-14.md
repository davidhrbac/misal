---
title: '2021-07-14'
---
# Středa po 15. neděli v mezidobí

## 

null

null

## Mezizpěv – Žl 103,1-2.3-4 6-7

Hospodin je milosrdný a milostivý.

## Zpěv před evangeliem – srov. Mt 11,25

Aleluja. Velebím tě, Otče, Pane nebe a země, že jsi tajemství Božího království odhalil maličkým. Aleluja.

## Evangelium – Mt 11,25-27

*Slova svatého evangelia podle Matouše*

Ježíš se ujal slova a řekl: „Velebím tě, Otče, Pane nebe a země, že když jsi tyto věci skryl před moudrými a chytrými, odhalil jsi je maličkým; ano, Otče, tak se ti zalíbilo. Všechno je mi dáno od mého Otce. A nikdo nezná Syna, jenom Otec, ani Otce nezná nikdo, jenom Syn a ten, komu to chce Syn zjevit.“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-07-14.json](/api/v1/2021-07-14.json)
    * YAML - [2021-07-14.yaml](/api/v1/2021-07-14.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-07-14.json](/api/v1/olejnik/2021-07-14.json)
    * YAML - [olejnik/2021-07-14.yaml](/api/v1/olejnik/2021-07-14.yaml)

### Pravopis

!!! failure ""
    {--*Chorebu*--}: Chorobu, Chorému, Chore bu, Chore-bu, Chotěbuz, Choření, Horenu, Horenů, Hořenu, Chebu, Chebů, Orebu, Orebů, Chore, Choru, Choré, Chorů, Choře, Chóre, Chóru, Chórů, Hřebu, Hřebů, Čorbu, Čorbů, Chorobou, Chopenu, Choroby, Chořejí, Chořeli, Chořely, Chořeti, Ohořenu, Shořenu, Uhořenu, Chřibů, Corelu, Corelů, Chlebu, Chlebů, Chodbu, Chodeb, Chorem, Chorob, Chorou, Chorém, Chořej, Chořel, Chořet, Chórem, Cherubu, Cherubů, Choleru, Chorobě, Chorech, Choroba, Chorobo, Chorošů, Chorálu, Chorálů, Chorého, Chořeje, Chořela, Chořelo, Chórech<br>
    {--*Jithra*--}: Jitra, Jíra, Jitř, Jitka, Jícha, Pitra, Piťha, Jihla, Jihna, Jikra, Jiter, Jitro, Jitru, Jitry, Jitře, Jitři, Jitří, Játra, Mitra, Nitra, Zítra, Jindra, Jiskra, Jizera, Vichra, Citera, Litera, Lithia, Lithná<br>
    {--*midjanského*--}: midianského, majánského, milánského, pijanského, abidjanského, Fidranského<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni-ex-3-1-6-9-12.html](/api/v1/obs/2021-07-14/1-cteni-ex-3-1-6-9-12.html)
    * [mezizpev.html](/api/v1/obs/2021-07-14/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-07-14/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-07-14/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "green")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
