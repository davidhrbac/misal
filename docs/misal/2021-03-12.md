---
title: '2021-03-12'
---
# Pátek po 3. neděli postní

## 1. čtení – Oz 14,2-10

*Čtení z knihy proroka Ozeáše*

Toto praví Hospodin: „Vrať se, Izraeli, k Hospodinu, svému Bohu, neboť jsi padl pro svou nepravost. Vezměte s sebou slova (modlitby), a obraťte se k Hospodinu; řekněte mu: Odpusť každou nepravost a laskavě přijmi, když ti budeme obětovat plod svých rtů. Asýrie nás nezachrání, na koně nevsedneme, nebudeme již říkat: `Bohové naši!' dílu svých rukou, poněvadž sirotek najde u tebe slitování. Zhojím jejich zradu, milovat je budu velkodušně, neboť můj hněv se od nich odvrátí. Rosou budu Izraeli, vykvete jak lilie a jak Libanon vyžene své kořeny. Rozprostřou se jeho ratolesti, jak oliva se bude skvít, jak Libanon bude vonět. Vrátí se a v mém stínu budou bydlet, obilí budou pěstovat, vyraší jak réva, budou mít věhlas jak libanonské víno. K čemu budou Efraimu modly? Vyslyším ho, shlédnu na něj, jsem zeleným cypřišem, ode mě je tvůj plod. Kdo je moudrý, kéž tomu porozumí, kdo má důvtip, ať to pozná! Přímé jsou Hospodinovy cesty, spravedliví po nich kráčejí, bezbožní však na nich padnou.“

## Mezizpěv – Žl 81,6c-8a.8bc-9.10-11ab.14+17

Já jsem Hospodin, tvůj Bůh, slyš můj hlas!

## Zpěv před evangeliem – Mt 4,17

Obraťte se, praví Pán, neboť se přiblížilo nebeské království.

## Evangelium – Mk 12,28b-34

*Slova svatého evangelia podle Marka*

Jeden z učitelů Zákona přistoupil k Ježíšovi a zeptal se ho: „Které přikázání je první ze všech?“ Ježíš odpověděl: „První je toto: `Slyš, Izraeli! Hospodin, náš Bůh, je jediný Pán. Proto miluj Pána, svého Boha, celým svým srdcem, celou svou duší, celou svou myslí a celou svou silou.' Druhé je toto: `Miluj svého bližního jako sám sebe.' Žádné jiné přikázání není větší než tato.“ Učitel Zákona mu na to řekl: „Správně, Mistře, podle pravdy jsi řekl, že on je jediný a není jiného kromě něho a milovat ho celým srdcem, celým rozumem a celou silou a milovat bližního jako sám sebe je víc než všechny oběti a dary.“ Když Ježíš viděl, že (učitel Zákona) odpověděl rozumně, řekl mu: „Nejsi daleko od Božího království.“ A nikdo se už neodvážil dát mu nějakou otázku.

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-03-12.json](/api/v1/2021-03-12.json)
    * YAML - [2021-03-12.yaml](/api/v1/2021-03-12.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-03-12.json](/api/v1/olejnik/2021-03-12.json)
    * YAML - [olejnik/2021-03-12.yaml](/api/v1/olejnik/2021-03-12.yaml)

### Pravopis

!!! failure ""
    {--*Efraimu*--}: Erasmu, Erasmů<br>
    {--*Meribě*--}: Měříne, Měříně, Měřice, Měřiče, Měříce, Měříme, Měříte<br>
    {--*Ozeáše*--}: Zřase, Oase, Ozebeš, Oženeš, Ožereš, Zase, Ožehavé, Ožehavě, Obraše, Okrase, Opráše, Ožrale, Ožralé, Ožrané, Ožraně, Oděse, Oběse, Ocase, Oteše, Ověse, Ozebe, Ozáře, Ožele, Ožene, Oženě, Ožere, Očeše, Zhase, Žďase, Úžase, Oceáne, Oceáně, Ohlase, Ohláse, Ozkuse, Ozlatě, Označe, Oznámě, Ožehle, Ožehlé, Ožehne, Oželme, Oželte, Ožerme, Ožerte, Ožeňme, Ožeňte<br>

### Typografie

!!! bug ""
    Nebudeme již říkat: {--*\`*--}Bohové naši!' dílu svých rukou.
    Toto praví Hospodin: „Vrať se, Izraeli, k Hospodinu, svému Bohu, neboť jsi padl pro svou nepravost. Vezměte s sebou slova (modlitby), a obraťte se k Hospodinu; řekněte mu: Odpusť každou nepravost a laskavě přijmi, když ti budeme obětovat plod svých rtů. Asýrie nás nezachrání, na koně nevsedneme, nebudeme již říkat: {--*\`*--}Bohové naši!' dílu svých rukou, poněvadž sirotek najde u tebe slitování. Zhojím jejich zradu, milovat je budu velkodušně, neboť můj hněv se od nich odvrátí. Rosou budu Izraeli, vykvete jak lilie a jak Libanon vyžene své kořeny. Rozprostřou se jeho ratolesti, jak oliva se bude skvít, jak Libanon bude vonět. Vrátí se a v mém stínu budou bydlet, obilí budou pěstovat, vyraší jak réva, budou mít věhlas jak libanonské víno. K čemu budou Efraimu modly? Vyslyším ho, shlédnu na něj, jsem zeleným cypřišem, ode mě je tvůj plod. Kdo je moudrý, kéž tomu porozumí, kdo má důvtip, ať to pozná! Přímé jsou Hospodinovy cesty, spravedliví po nich kráčejí, bezbožní však na nich padnou.“
    Jeden z učitelů Zákona přistoupil k Ježíšovi a zeptal se ho: „Které přikázání je první ze všech?“ Ježíš odpověděl: „První je toto: {--*\`*--}Slyš, Izraeli! Hospodin, náš Bůh, je jediný Pán. Proto miluj Pána, svého Boha, celým svým srdcem, celou svou duší, celou svou myslí a celou svou silou.' Druhé je toto: {--*\`*--}Miluj svého bližního jako sám sebe.' Žádné jiné přikázání není větší než tato.“ Učitel Zákona mu na to řekl: „Správně, Mistře, podle pravdy jsi řekl, že on je jediný a není jiného kromě něho a milovat ho celým srdcem, celým rozumem a celou silou a milovat bližního jako sám sebe je víc než všechny oběti a dary.“ Když Ježíš viděl, že (učitel Zákona) odpověděl rozumně, řekl mu: „Nejsi daleko od Božího království.“ A nikdo se už neodvážil dát mu nějakou otázku.

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-03-12/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-03-12/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-03-12/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-03-12/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "deep-purple")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
