---
title: '2021-04-26'
---
# Pondělí po 4. neděli velikonoční

## 1. čtení – Sk 11,1-18

*Čtení ze Skutků apoštolů*

Apoštolové a bratři v Judsku se dověděli, že i pohané přijali Boží slovo. Když pak Petr přišel do Jeruzaléma, věřící obrácení ze židovství mu dělali výčitky a říkali: „Vešel jsi k lidem neobřezaným a jedl jsi s nimi!“ Petr jim tedy začal po pořádku vykládat: „Byl jsem v městě Joppe a modlil jsem se. Tu jsem ve vytržení spatřil vidění: něco se snášelo z nebe. Podobalo se to velkému prostěradlu, které je spouštěno za čtyři cípy, a přišlo to až ke mně. Když jsem se na to důkladně podíval, spatřil jsem pozemské čtvernožce, zvěř, plazy a nebeské ptáky. Uslyšel jsem také, jak mi nějaký hlas říká: `Vzhůru, Petře, zabíjej a jez!' Já jsem odpověděl: `Ani za nic, Pane! Ještě nikdy jsem nevzal do úst nic poskvrněného nebo nečistého!' Ale ten hlas z nebe promluvil podruhé: `Co Bůh prohlásil za čisté, o tom neříkej, že je to poskvrněné!' To se stalo třikrát. Pak hned to všechno bylo zase vyzdviženo do nebe. A v tom okamžiku se zastavili tři muži u domu, kde jsme bydleli; byli ke mně posláni z Césareje. Duch mi řekl, abych bez váhání šel s nimi. Vydalo se se mnou na cestu i těchto šest bratří. Když jsme vstoupili do domu toho muže, (který poslal ony tři posly,) vypravoval nám, jak spatřil ve svém domě stát anděla a že mu nařídil: `Pošli do Joppe a povolej si Šimona, kterému říkají Petr. Ten tě poučí, jak dosáhneš spásy ty i celý tvůj dům.' Když jsem začal mluvit, sestoupil na ně Duch Svatý jako na začátku na nás. Tu jsem si vzpomněl na slova Páně: `Jan křtil vodou, ale vy budete pokřtěni Duchem Svatým.' Jestliže tedy Bůh dal stejný dar jim jako nám, když jsme přijali víru v Pána Ježíše Krista, jak bych se mohl já odvážit klást Bohu překážky?“ Když to vyslechli, uklidnili se. Velebili Boha a říkali: „Tedy i pohanům dal Bůh, aby se obrátili, a tak došli života.“

## Mezizpěv – Žl 42,2-3; 43,3.4

Má duše žízní po živém Bohu.

## Zpěv před evangeliem – Jan 10,14

Aleluja. Já jsem dobrý pastýř, praví Pán, znám svoje ovce a moje ovce znají mne. Aleluja.

## Evangelium – Jan 10,1-10

*Slova svatého evangelia podle Jana*

Ježíš řekl: „Amen, amen, pravím vám: Kdo nevchází do ovčince dveřmi, ale vniká tam jinudy, to je zloděj a lupič. Kdo však vchází dveřmi, je pastýř ovcí. Vrátný mu otevře a ovce slyší jeho hlas. Volá své ovce jménem a vyvádí je. Když všechny své ovce vyvede, jde před nimi a ovce ho následují, protože znají jeho hlas. Za cizím však nikdy nepůjdou, ale utečou od něho, protože hlas cizích neznají.“ Ježíš jim pověděl toto přirovnání, ale oni nepochopili, co jim tím chce říci. Ježíš proto řekl znovu: „Amen, amen, pravím vám: Já jsem dveře k ovcím. Všichni, kdo přišli přede mnou, jsou zloději a lupiči, ale ovce je neuposlechly. Já jsem dveře. Kdo vejde skrze mne, bude zachráněn; bude moci vcházet i vycházet a najde pastvu. Zloděj přichází, jen aby kradl, zabíjel a působil zkázu. Já jsem přišel, aby měly život a aby ho měly v hojnosti.“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-04-26.json](/api/v1/2021-04-26.json)
    * YAML - [2021-04-26.yaml](/api/v1/2021-04-26.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-04-26.json](/api/v1/olejnik/2021-04-26.json)
    * YAML - [olejnik/2021-04-26.yaml](/api/v1/olejnik/2021-04-26.yaml)

### Pravopis

!!! failure ""
    {--*Césareje*--}: Česaje, Nesvářeje, Česávejme, Česávejte, Česávej, Nesázeje, Česaněji, Česávaje<br>
    {--*horua*--}: Hora, Horu, Horů, hora, horu, Horká, Horna, Horta, Horům, Horův, horda, horka, horká, horuj, hořká<br>
    {--*Joppe*--}: Pope, Knoppe, Kippe, Kope, Kopě, Ope, Zippe, Houpe, Kopce, Kopie, Kopne, Kopné, Kopně, Kopre, Kopte, Kopře, Koupe, Koupě, Jobe, Cope, Jode, Jole, Jotě, Jóde, Józe, Mope, Opce, Opře, Ropě, Topě, Pople, Pompě, Popce, Popře, Poupě, Johne, Joure, Jožce, Puppe, Zappe, Docpe, Dopne, Doupě, Doúpě, Jolce, Joule, Loupe, Loupě, Ropné, Ropně, Roupe, Sopce, Sople, Soptě, Topme, Topné, Topně, Topte, Šopce, Šoupe, Šoupě<br>
    {--*plazy*--}: plazmy, plazů, plaz, pláž, splazy, Plasy, placy, plaky, planý, platy, plavý, plaze, plazi, plazí, plály, plány, pláty, pláže, pláži, pláží, žlázy<br>
    {--*vedoua*--}: vedou, védou, vědou, vědoma, vědomá, vědouc<br>

### Typografie

!!! bug ""
    Apoštolové a bratři v Judsku se dověděli, že i pohané přijali Boží slovo. Když pak Petr přišel do Jeruzaléma, věřící obrácení ze židovství mu dělali výčitky a říkali: „Vešel jsi k lidem neobřezaným a jedl jsi s nimi!“ Petr jim tedy začal po pořádku vykládat: „Byl jsem v městě Joppe a modlil jsem se. Tu jsem ve vytržení spatřil vidění: něco se snášelo z nebe. Podobalo se to velkému prostěradlu, které je spouštěno za čtyři cípy, a přišlo to až ke mně. Když jsem se na to důkladně podíval, spatřil jsem pozemské čtvernožce, zvěř, plazy a nebeské ptáky. Uslyšel jsem také, jak mi nějaký hlas říká: {--*\`*--}Vzhůru, Petře, zabíjej a jez!' Já jsem odpověděl: {--*\`*--}Ani za nic, Pane! Ještě nikdy jsem nevzal do úst nic poskvrněného nebo nečistého!' Ale ten hlas z nebe promluvil podruhé: {--*\`*--}Co Bůh prohlásil za čisté, o tom neříkej, že je to poskvrněné!' To se stalo třikrát. Pak hned to všechno bylo zase vyzdviženo do nebe. A v tom okamžiku se zastavili tři muži u domu, kde jsme bydleli; byli ke mně posláni z Césareje. Duch mi řekl, abych bez váhání šel s nimi. Vydalo se se mnou na cestu i těchto šest bratří. Když jsme vstoupili do domu toho muže, (který poslal ony tři posly,) vypravoval nám, jak spatřil ve svém domě stát anděla a že mu nařídil: {--*\`*--}Pošli do Joppe a povolej si Šimona, kterému říkají Petr. Ten tě poučí, jak dosáhneš spásy ty i celý tvůj dům.' Když jsem začal mluvit, sestoupil na ně Duch Svatý jako na začátku na nás. Tu jsem si vzpomněl na slova Páně: {--*\`*--}Jan křtil vodou, ale vy budete pokřtěni Duchem Svatým.' Jestliže tedy Bůh dal stejný dar jim jako nám, když jsme přijali víru v Pána Ježíše Krista, jak bych se mohl já odvážit klást Bohu překážky?“ Když to vyslechli, uklidnili se. Velebili Boha a říkali: „Tedy i pohanům dal Bůh, aby se obrátili, a tak došli života.“
    Jako laň prahne po vodách bystři{--*n,t*--}ak prahne má duše po tobě, Bože!Má duše žízní po Bohu, po živém Bohu:kdy už smím přijít a spatřit Boží tvář?
    Pak přistoupím k Božímu oltář{--*i,k[m[K Bohu, který mě naplňuje radost[01;31m[Kí.O[m[Kslavím tě citero[01;31m[Ku,B*--}ože, můj Bože!

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-04-26/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-04-26/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-04-26/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-04-26/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "white")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
