---
title: '2021-04-08'
---
# Čtvrtek v oktávu velikonočním

## 1. čtení – Sk 3,11-26

*Čtení ze Skutků apoštolů*

Protože se (uzdravený chromý) držel Petra a Jana, v úžasu se k nim sběhl všechen lid do tak zvaného podloubí Šalomounova. Když to Petr viděl, promluvil k lidu: „Izraelité, proč se tomu tak divíte, nebo co na nás tak hledíte, jako bychom to udělali my vlastní mocí nebo zbožností, že tenhle člověk chodí? To Bůh Abrahámův, Izákův a Jakubův, Bůh našich praotců oslavil svého služebníka Ježíše, kterého jste vy sice vydali a kterého jste se zřekli před Pilátem, ačkoliv on rozhodl, že ho propustí. Vy však jste se zřekli Svatého a Spravedlivého, a vyprosili jste si milost pro vraha. Původce života jste dali zabít, ale Bůh ho vzkřísil z mrtvých, a my jsme toho svědky. A protože v něho věříme, dalo jeho jméno sílu tomuto člověku, kterého vidíte i znáte, a (právě) víra, která od něho pochází, úplně uzdravila tohoto nemocného před očima vás všech. Já ovšem vím, bratři, že jste to udělali z nevědomosti, a stejně tak i vaši přední muži. Bůh (to však dopustil), aby se tak splnilo, co předem oznámil ústy všech proroků, že jeho Pomazaný musí trpět. Obraťte se tedy a dejte se na pokání, aby vaše hříchy byly zahlazeny, aby přišla od Pána ona doba útěchy a aby vám poslal Mesiáše, který byl pro vás určen, Ježíše. Nebe ho musí přijmout až do té doby, kdy bude zase všecko obnoveno, jak o tom už dávno mluvil ústy svých svatých proroků. Tak Mojžíš řekl: 'Proroka vám dá Pán Bůh z vašich bratří jako mne. Poslouchejte ho ve všem, co k vám bude mluvit. Kdo by však onoho proroka neposlouchal, bude z lidu vyhlazen!' Také ostatní proroci, co jich prorokovalo od Samuela dále, poukazovali zřejmě na tyto dni. Vy jste synové proroků a máte účast ve smlouvě, kterou Bůh uzavřel s vašimi otci, když prohlásil Abrahámovi: 'V tvém potomstvu budou požehnány všechny rody na zemi.' Vám na prvním místě dal Bůh svého služebníka a poslal ho, aby vám požehnal, a tak každého z vás odvrátil od jeho špatností.“

## Mezizpěv – Žl 8,2a+5.6-7.8-9

Hospodine, náš Pane, jak podivuhodné je tvé jméno po celé zemi!

## Zpěv před evangeliem – Žl 118,24

Aleluja. Toto je den, který učinil Hospodin, jásejme a radujme se z něho. Aleluja.

## Evangelium – Lk 24,35-48

*Slova svatého evangelia podle Lukáše*

Dva učedníci vypravovali, co se jim přihodilo na cestě a jak Ježíše poznali při lámání chleba. Když o tom mluvili, stál on sám uprostřed nich a řekl jim: „Pokoj vám!“ Zděsili se a ve strachu se domnívali, že vidí ducha. Řekl jim: „Proč jste rozrušeni a proč vám v mysli vyvstávají pochybnosti? Podívejte se na mé ruce a na mé nohy: vždyť jsem to já sám! Dotkněte se mě a přesvědčte se: duch přece nemá maso a kosti, jak to vidíte na mně.“ A po těch slovech jim ukázal ruce a nohy. Pro samou radost však tomu pořád ještě nemohli věřit a (jen) se divili. Proto se jich zeptal: „Máte tady něco k jídlu?“ Podali mu kus pečené ryby. Vzal si a před nimi pojedl. Dále jim řekl: „To je smysl mých slov, která jsem k vám mluvil, když jsem ještě byl s vámi: že se musí naplnit všechno, co je o mně psáno v Mojžíšově Zákoně, v Prorocích i v Žalmech.“ Tehdy jim otevřel mysl, aby rozuměli Písmu. Řekl jim: „Tak je psáno: Kristus bude trpět a třetího dne vstane z mrtvých a v jeho jménu bude hlásáno pokání, aby všem národům, počínajíc od Jeruzaléma, byly odpuštěny hříchy. Vy jste toho svědky. Hle – já vám pošlu toho, koho slíbil můj Otec. Vy tedy zůstaňte ve městě, dokud nebudete vyzbrojeni mocí z výsosti.“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-04-08.json](/api/v1/2021-04-08.json)
    * YAML - [2021-04-08.yaml](/api/v1/2021-04-08.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-04-08.json](/api/v1/olejnik/2021-04-08.json)
    * YAML - [olejnik/2021-04-08.yaml](/api/v1/olejnik/2021-04-08.yaml)

### Pravopis

!!! failure ""
    {--*dals*--}: další, dala, Dáš, dal, dál, dáš, ďas, bals, dali, dalm, dalo, daly, dále, dáli, dálí<br>
    {--*ověnčils*--}: ověnčila, ověnčil, ověnčíš, ověnčili, ověnčilo, ověnčily<br>
    {--*položils*--}: položila, položil, položíš, položili, položilo, položily<br>
    {--*Učinils*--}: Učinila, Učinil, Učiníš, Učinili, Učinilo, Učinily<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-04-08/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-04-08/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-04-08/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-04-08/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "white")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
