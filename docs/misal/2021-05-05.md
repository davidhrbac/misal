---
title: '2021-05-05'
---
# Středa po 5. neděli velikonoční

## 1. čtení – Sk 15,1-6

*Čtení ze Skutků apoštolů*

Někteří lidé přišli z Judska (do Antiochie) a poučovali bratry: „Nedáte-li se podle mojžíšského zvyku obřezat, nemůžete dojít spásy.“ Pavel a Barnabáš se však s nimi dostali do hádky a úporně se s nimi o to přeli. Bylo proto rozhodnuto, aby se kvůli této sporné otázce Pavel, Barnabáš a někteří z nich odebrali k apoštolům a starším do Jeruzaléma. Církevní obec je tedy vypravila na cestu. Šli přes Fénicii a Samařsko a obšírně vykládali o tom, jak se pohané obracejí, a tím působili všem bratřím velikou radost. Když přišli do Jeruzaléma, byli přijati od církevní obce, apoštolů a starších. Vypravovali jim, co všechno Bůh jejich prostřednictvím vykonal. Ale někteří z těch, kdo přešli k víře z farizejské strany, zasáhli a říkali: „Pohané musí přijmout obřízku a je třeba jim nařídit, aby zachovávali Mojžíšův Zákon.“ Sešli se tedy apoštolové a starší, aby tu věc uvážili.

## Mezizpěv – Žl 122,1-2.3-4a.4b-5

Do domu Hospodinova půjdeme s radostí.

## Zpěv před evangeliem – Jan 15,4a-5b

Aleluja. Zůstaňte ve mně, a já zůstanu ve vás, praví Pán; kdo zůstává ve mně, ten nese mnoho ovoce. Aleluja.

## Evangelium – Jan 15,1-8

*Slova svatého evangelia podle Jana*

Ježíš řekl svým učedníkům: „Já jsem pravý vinný kmen a můj Otec je vinař. Každou ratolest na mně, která nenese ovoce, odřezává, a každou, která nese ovoce, čistí, aby nesla ovoce ještě více. Vy jste už čistí tím slovem, které jsem k vám mluvil. Zůstaňte ve mně, a já (zůstanu) ve vás. Jako ratolest nemůže nést ovoce sama od sebe, nezůstane-li na kmeni, tak ani vy, nezůstanete-li ve mně. Já jsem vinný kmen, vy jste ratolesti. Kdo zůstává ve mně a já v něm, ten nese mnoho ovoce, neboť beze mne nemůžete dělat nic. Kdo nezůstane ve mně, bude vyhozen ven jako ratolest; uschne, seberou ji, hodí do ohně - a hoří. Zůstanete-li ve mně a zůstanou-li ve vás moje slova, můžete prosit, oč chcete, a dostanete to. Tím bude oslaven můj Otec, že ponesete mnoho ovoce a osvědčíte se jako moji učedníci.“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-05-05.json](/api/v1/2021-05-05.json)
    * YAML - [2021-05-05.yaml](/api/v1/2021-05-05.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-05-05.json](/api/v1/olejnik/2021-05-05.json)
    * YAML - [olejnik/2021-05-05.yaml](/api/v1/olejnik/2021-05-05.yaml)

### Pravopis

!!! failure ""
    {--*Barnabáš*--}: Barnatá<br>
    {--*nohyv*--}: Nohův, Nohy, nohy<br>
    {--*Samařsko*--}: Samařanko, Samaro, Sársko, Sadařskou, Saharskou, Šamanskou, Šamanko, Maďarsko, Somálsko, Sadařská, Sadařské, Sadařský, Saharsky, Saharská, Saharské, Saharský, Šamansky, Šamanská, Šamanské, Šamanský, Šampusko<br>

### Typografie

!!! bug ""
    Jeruzalém je vystavěn jako měst{--*o,s[m[Kpojené v jeden cele[01;31m[Kk.T*--}am vystupují kmeny, kmeny Hospodinovy.
    Jak to zákon přikazuje Izrael{--*i,a[m[Kby chválil Hospodinovo jmén[01;31m[Ko.T[m[Kam stojí soudní stolc[01;31m[Ke,s*--}tolce Davidova domu.

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-05-05/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-05-05/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-05-05/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-05-05/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "white")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
