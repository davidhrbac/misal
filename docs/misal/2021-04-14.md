---
title: '2021-04-14'
---
# Středa po 2. neděli velikonoční

## 1. čtení – Sk 5,17-26

*Čtení ze Skutků apoštolů*

Velekněz a všichni jeho stoupenci, totiž saducejská strana, byli plní žárlivosti, proto dali apoštoly zatknout a zavřít do veřejného žaláře. Ale anděl Páně v noci dveře žaláře otevřel, vyvedl je ven a řekl: „Jděte, postavte se v chrámě a hlásejte lidu všechna slova toho života.“ Oni uposlechli, vešli časně ráno do chrámu a učili. Zatím se dostavil velekněz a jeho stoupenci. Svolali veleradu i všechny izraelské starší a poslali (služebníky) do žaláře, aby je přivedli. Když tam však služebníci přišli, ve vězení je nenalezli. Vrátili se tedy a hlásili: „Žalář jsme našli pečlivě uzavřený a stráž stát přede dveřmi, ale když jsme otevřeli, nenašli jsme uvnitř nikoho.“ Když velitel chrámové stráže a velekněží uslyšeli ta slova, byli na rozpacích, jak se to mohlo stát. Tu kdosi přišel a hlásil jim: „Muži, které jste uvěznili, jsou v chrámu a učí lid!“ Odešel tedy velitel se svými lidmi a přivedli je, ale bez násilí. Měli totiž strach z lidu, že je bude kamenovat.

## Mezizpěv – Žl 34,2-3.4-5.6-7.8-9

Ubožák zavolal, a Hospodin slyšel.

## Zpěv před evangeliem – srov. Jan 3,16

Aleluja. Tak Bůh miloval svět, že dal svého jednorozeného Syna; každý, kdo v něho věří, má věčný život. Aleluja.

## Evangelium – Jan 3,16-21

*Slova svatého evangelia podle Jana*

Tak Bůh miloval svět, že dal svého jednorozeného Syna, aby žádný, kdo v něho věří, nezahynul, ale měl život věčný. Bůh přece neposlal svého Syna na svět, aby svět odsoudil, ale aby svět byl skrze něho spasen. Kdo v něho věří, není souzen; kdo nevěří, už je odsouzen, protože neuvěřil ve jméno jednorozeného Syna Božího. Soud pak záleží v tomto: Světlo přišlo na svět, ale lidé měli raději tmu než světlo, protože jejich skutky byly zlé. Každý totiž, kdo páchá zlo, nenávidí světlo a nejde ke světlu, aby jeho skutky nebyly odhaleny. Kdo však jedná podle pravdy, jde ke světlu, aby se ukázalo, že jeho skutky jsou vykonány v Bohu.

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-04-14.json](/api/v1/2021-04-14.json)
    * YAML - [2021-04-14.yaml](/api/v1/2021-04-14.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-04-14.json](/api/v1/olejnik/2021-04-14.json)
    * YAML - [olejnik/2021-04-14.yaml](/api/v1/olejnik/2021-04-14.yaml)

### Pravopis

!!! failure ""
    {--*andělkolem*--}: anděl kolem, anděl-kolem<br>

### Typografie

!!! bug ""
    Ustavičně chci velebit Hospodin{--*a,v[m[Kždy bude v mých ústech jeho chvál[01;31m[Ka.V[m[K Hospodinu nechť se chlubí moje duš[01;31m[Ke,a*--}ť to slyší pokorní a radují se.
    Velebte se mnou Hospodin{--*a,o[m[Kslavujme spolu jeho jméno!Hledal jsem Hospodina, a vyslyšel m[01;31m[Kě,v*--}ysvobodil mě ze všech mých obav.
    Pohleďte k němu, ať se rozveselít{--*e,v[m[Kaše tvář se nemusí zardívat hanbo[01;31m[Ku.H[m[Kle, ubožák zavolal, a Hospodin slyše[01;31m[Kl,p*--}omohl mu ve všech jeho strastech.
    Jak ochránce se utábořil Hospodinův andělkolem těch, kdo Hospodina ctí, a vysvobodil j{--*e.O[m[Kkuste a vizte, jak je Hospodin dobr[01;31m[Ký,b*--}laze člověku, který se k němu utíká.

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-04-14/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-04-14/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-04-14/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-04-14/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "white")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
