---
title: '2021-07-17'
---
# Sobota po 15. neděli v mezidobí

## 

null

null

## Mezizpěv – Žl 136,1+23-24.10-12.13-15

Oslavujte Hospodina, neboť je dobrý,
jeho milosrdenství trvá navěky.

## Zpěv před evangeliem – 2 Kor 5,19

Aleluja. Bůh pro Kristovy zásluhy smířil svět se sebou a nás pověřil kázáním o tomto usmíření. Aleluja.

## Evangelium – Mt 12,14-21

*Slova svatého evangelia podle Matouše*

Farizeové se radili, jak by Ježíše zahubili. Protože to Ježíš věděl, odebral se odtamtud. Mnoho jich šlo za ním a všechny uzdravil. Ale nařídil jim, aby ho nerozhlašovali. Tak se mělo naplnit, co řekl prorok Izaiáš: `Hle, můj služebník, kterého jsem si vyvolil, můj milovaný, v němž jsem našel zalíbení! Vložím na něho svého Ducha, aby hlásal právo národům. Nebude se hádat ani křičet, na ulici nikdo neuslyší jeho hlas. Nalomenou třtinu nedolomí, doutnající knot neuhasí, až dopomůže právu k vítězství. A v jeho jménu budou národy doufat.' 

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-07-17.json](/api/v1/2021-07-17.json)
    * YAML - [2021-07-17.yaml](/api/v1/2021-07-17.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-07-17.json](/api/v1/olejnik/2021-07-17.json)
    * YAML - [olejnik/2021-07-17.yaml](/api/v1/olejnik/2021-07-17.yaml)

### Pravopis

!!! failure ""
    {--*neizraelitů*--}: ne izraelitů, ne-izraelitů, izraelitu, izraelitů, neizraelští<br>
    {--*Ramesu*--}: Ramešů, Ramešům, Ramešův, Rameši, Rameš, Jamesů, Rameše, Remešů, Ramenu, Rámusu, Rámusů<br>
    {--*Sukkot*--}: Sulko, Sykot, Skot, Subkvót, Sulkou, Subot, Hukot, Sudko, Sukno, Sumko, Supot, Suško, Ukroť, Šubko, Šukat, Šumot, Šunko, Ťukot, Sudkov, Sudkou, Sumkou, Suškou, Štěkot, Šubkou, Šunkou, Šustot, Šuškat<br>

### Typografie

!!! bug ""
    Farizeové se radili, jak by Ježíše zahubili. Protože to Ježíš věděl, odebral se odtamtud. Mnoho jich šlo za ním a všechny uzdravil. Ale nařídil jim, aby ho nerozhlašovali. Tak se mělo naplnit, co řekl prorok Izaiáš: {--*\`*--}Hle, můj služebník, kterého jsem si vyvolil, můj milovaný, v němž jsem našel zalíbení! Vložím na něho svého Ducha, aby hlásal právo národům. Nebude se hádat ani křičet, na ulici nikdo neuslyší jeho hlas. Nalomenou třtinu nedolomí, doutnající knot neuhasí, až dopomůže právu k vítězství. A v jeho jménu budou národy doufat.' 

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni-ex-12-37-42.html](/api/v1/obs/2021-07-17/1-cteni-ex-12-37-42.html)
    * [mezizpev.html](/api/v1/obs/2021-07-17/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-07-17/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-07-17/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "green")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
