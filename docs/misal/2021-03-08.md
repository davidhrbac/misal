---
title: '2021-03-08'
---
# Pondělí po 3. neděli postní

## 1. čtení – 2 Král 5,1-15a

*Čtení z druhé knihy Královské*

Náman, velitel vojska aramejského krále, byl u svého pána vlivným mužem a váženým pro vítězství, které Hospodin popřál skrze něj Aramejcům, ale byl malomocný. Když jednou Aramejci vyšli v houfech k nájezdu, zajali z izraelské země malou dívku, která se stala služkou u Námanovy ženy. Řekla své paní: „Kéž by byl můj pán u proroka v Samaří, ten by ho zbavil jeho malomocenství.“ Náman to šel oznámit svému pánu: „Tak a tak mluvila dívka z izraelské země.“ Aramejský král řekl: „Nuže, jdi tam, pošlu dopis izraelskému králi.“ Náman šel a vzal s sebou deset talentů stříbra, šest tisíc šiklů zlata a desatero svátečních šatů a přinesl izraelskému králi dopis tohoto znění: „Zároveň s tímto dopisem posílám k tobě svého služebníka Námana, abys ho zbavil malomocenství.“ Když izraelský král dopis přečetl, roztrhl svůj šat a zvolal: „Copak jsem Bůh, abych rozhodoval o životě a smrti, že ke mně posílá člověka, abych ho zbavil malomocenství? Je zřejmé, sami to vidíte, že hledá proti mně záminku!“ Když se Elizeus dozvěděl, že si izraelský král roztrhl šat, poslal k němu vzkaz: „Proč sis roztrhl šat? Ať přijde ke mně a pozná, že je v Izraeli prorok.“ Náman tedy přijel se svým spřežením a vozem a zastavil se u dveří Elizeova domu. Elizeus poslal k němu posla se vzkazem: „Jdi se umýt sedmkrát v Jordáně a tvé tělo se obnoví; jdi a budeš čistý!“ Náman se rozhněval a odcházel se slovy: „Řekl jsem si: Jistě mi vyjde vstříc a osobně přede mnou bude vzývat jméno Hospodina, svého Boha, vztáhne ruku k nemocnému místu a zbaví mě malomocenství. Copak nejsou lepší Abana a Parpar, řeky damašské, než všechny vody izraelské? Nemohl bych se umýt v nich, abych byl čistý?“ Obrátil se s hněvem a odcházel. Tu přistoupili jeho služebníci a domlouvali mu: „Otče, kdyby ti prorok nařídil něco nesnadného, neudělal bys to? Tím spíše, když ti řekl: Umyj se a budeš čistý!“ Sestoupil tedy a ponořil se do Jordánu sedmkrát podle nařízení Božího muže a jeho tělo se obnovilo jako tělo malého dítěte a byl čistý. Náman se vrátil k Božímu muži s celým svým doprovodem, stanul před ním a řekl: „Hle, už vím, že není Boha po celé zemi, jen v Izraeli.“

## Mezizpěv – Žl 42,2.3; 43,3.4

Má duše žízní po živém Bohu, kdy už ho spatřím tváří v tvář?

## Zpěv před evangeliem – srov. Žl 130,5.7

Doufám v Hospodina, doufám v jeho slovo, neboť u něho je slitování, hojné je u něho vykoupení.

## Evangelium – Lk 4,24-30

*Slova svatého evangelia podle Lukáše*

Když přišel Ježíš do Nazareta, řekl lidu v synagoze: „Amen, pravím vám: Žádný prorok není vítaný ve svém domově. Říkám vám podle pravdy: Mnoho vdov bylo v izraelském národě za dnů Eliášových, kdy se nebe zavřelo na tři léta a šest měsíců a nastal velký hlad po celé zemi; ale k žádné z nich nebyl poslán Eliáš, jen k vdově do Sarepty v Sidónsku. A mnoho malomocných bylo v izraelském národě za proroka Elizea, ale nikdo z nich nebyl očištěn, jenom Náman ze Sýrie.“ Když to slyšeli, všichni v synagoze vzplanuli hněvem. Zvedli se, vyhnali ho ven z města a vedli až na sráz hory, na níž bylo vystavěno jejich město, aby ho srazili dolů. On však prošel jejich středem a ubíral se dál.

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-03-08.json](/api/v1/2021-03-08.json)
    * YAML - [2021-03-08.yaml](/api/v1/2021-03-08.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-03-08.json](/api/v1/olejnik/2021-03-08.json)
    * YAML - [olejnik/2021-03-08.yaml](/api/v1/olejnik/2021-03-08.yaml)

### Pravopis

!!! failure ""
    {--*Abana*--}: Rabana, Habána, Alana, Dbána<br>
    {--*damašské*--}: dam ašské, dam-ašské, Damašek, damašek, Damaškem, damaškem, dámské, dalmatské, lamanšské, danajské, šamanské, Damaška, Damašku, Damašky, Damašků, damašku, damašky, damašků, adamanské, jamajské, kazašské, lamačské, valašské<br>
    {--*Elizea*--}: Lížeš, Líza, Líze, Lize, Lízá, Líže, Nelížeš, Belize, Blížena, Blížená, Klizena, Klizená, Klížena, Klížená, Nelízá, Nelíže, Blížeš, Olížeš, Slížeš, Elisa, Elise, Klíže, Lizek, Lizka, Blízá, Blíže, Elita, Elitě, Hlíza, Hlíze, Lizem, Lízej, Lízla, Lízna, Olízá, Olíže, Plizé, Plíže, Slize, Slízá, Slíže, Nelízej, Nelízla, Nelízna, Želízek, Želízka, Elinek, Elinka, Elisem, Elišek, Eliška, Klížem, Blizen, Blizna, Blízej, Blízek, Blízka, Blízká, Blížen, Elipsa, Hlízek, Hlízka, Klizen, Kližek, Kližka, Klížen, Olízej, Olízla, Olízna, Slizem, Slizká, Slízej, Slízla, Slízna, Ulízla, Ulízna<br>
    {--*Elizeova*--}: Blížejova, Elisova, Elisová, Hlízova, Klížova, Klížová, Slizová, Heligeova, Heligeová, Blízkova, Eliškova, Elišková, Eliášova, Eliášová, Elipsová<br>
    {--*Elizeus*--}: Lížeš, Nelížeš, Blížeš, Olížeš, Slížeš<br>
    {--*Námana*--}: Namana, Najmana, Namázna, Namaň, Hamana, Lámaná, Lámána, Nadaná, Nadána, Namaká, Namane, Namanu, Namáhá, Námaha, Zamana, Šamana, Ňafaná, Ňafána<br>
    {--*Náman*--}: Namaň, Najman, Namana, Namane, Namanu, Haman, Lámán, Nadán, Namal, Namaž, Námah, Zamaň, Šaman, Ňafán<br>
    {--*Námanovy*--}: Najmanovy, Hamanovy, Šamanovy<br>
    {--*Parpar*--}: Par par, Par-par, Propař, Připař, Papat, Pařát, Párat, Papa, Papá, Prapor, Pápá, Pára, Párá, Paspart, Karpat, Patvar, Kapar, Parák, Páral, Farář, Napař, Pampa, Papal, Papáj, Papám, Papán, Papáš, Papír, Paraf, Paraď, Parma, Parna, Parná, Parta, Parád, Parám, Pařba, Pařák, Pápěr, Pária, Párák, Párám, Párán, Páráš, Zapař, Zápar, Gašpar, Harčár, Jarmar, Kašpar, Parkan, Parker, Parléř, Parnas, Barbar, Barvář, Bárkař, Farmář, Lampář, Pampám, Pardál, Parkán, Parmám, Partaj, Parter, Partám, Parťák, Pařbám, Pumpař, Purpur, Pálkař<br>
    {--*Samaří*--}: Samaria, Samarii, Samariu, Samarií, Samaro, Samaru, Samar, Samara, Samary, Samaře, Sadaři, Safari, Sumáři, Šafáři, Šamani, Šumaři<br>
    {--*Sarepty*--}: Karpety, Šarloty, Adepty, Aresty, Barety, Brepty, Karety, Samety, Skřety, Střepy, Střety, Škréty, Caretty, Skripty, Střepte, Zareptá, Šarlaty<br>
    {--*Sidónsku*--}: Siónskou, Siónský, Simonku, Silonku, Siónská, Siónské, Šimonku, Šimonků, Livonsku, Šimoníku, Šimoníků<br>
    {--*šiklů*--}: Šiklu, Šiklů, Šiklům, Šiklův, Šikolu, Šikolů, Šikulu, Šikulů, šikulu, šikulů, soklu, soklů, Šikly, šikli, šikly, Siku, Siků, silu, sklu, sílu, Šikl, Šílu, Šílů, šikl, šiku, šiků, Miklu, Miklů, Niklu, Niklů, Sikou, Ziklu, Ziklů, niklu, niklů, sídlu, Šikla, Šikle, Šídlu, Šídlů, šikla, šiklo, šiknu, šimlu, šimlů, šídlu, Čiklu, Čiklů<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-03-08/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-03-08/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-03-08/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-03-08/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "deep-purple")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
