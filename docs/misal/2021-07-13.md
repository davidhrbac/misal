---
title: '2021-07-13'
---
# Úterý po 15. neděli v mezidobí

## 

null

null

## Mezizpěv – Žl 69,3.14.30-31.33-34

Hledejte Pána, ubožáci, a vaše duše bude žít.

## Zpěv před evangeliem – srov. Žl 95,8ab

Aleluja. Nezatvrzujte dnes svá srdce, ale slyšte hlas Hospodinův. Aleluja.

## Evangelium – Mt 11,20-24

*Slova svatého evangelia podle Matouše*

Ježíš začal vytýkat městům, ve kterých se stalo nejvíc jeho zázraků, že se neobrátila: „Běda tobě, Chorazine, běda tobě, Betsaido! Kdyby se staly v Tyru a Sidónu takové zázraky jako u vás, už dávno by dělali pokání v žínici a v popelu. Ale to vám říkám: Tyru a Sidónu bude v soudný den lehčeji než vám. A ty, Kafarnaum, budeš snad vyvýšeno až do nebe? Až do pekla klesneš! Kdyby se v Sodomě staly ty zázraky, které se staly v tobě, zůstala by až do dneška. Ale říkám vám: Sodomské zemi bude v soudný den lehčeji než tobě.“

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-07-13.json](/api/v1/2021-07-13.json)
    * YAML - [2021-07-13.yaml](/api/v1/2021-07-13.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-07-13.json](/api/v1/olejnik/2021-07-13.json)
    * YAML - [olejnik/2021-07-13.yaml](/api/v1/olejnik/2021-07-13.yaml)

### Pravopis

!!! failure ""
    {--*Chorazine*--}: Chržíne, Chržíně, Harazine, Choratice<br>
    {--*Kafarnaum*--}: Katařanům, Kasárnám, Kasárnům, Kavárnám<br>
    {--*Levi*--}: Leví, Levín, Ledví, Levic, Levní, Levu, Levů, Leči, Lečí, Léči, Léčí, Lei, Lev, Leí, Lvi, Lví, Elévi, Sleví, Uleví, Leva, Leve, Levy, Levá, Levé, Levý, Jeví, Lepí, Letí, Levě, Leží, Loví, Neví<br>
    {--*Midjan*--}: Mejdan, Míchán, Milan, Milán, Jídán, Míjen, Míván, Pijan, Vídán, Abidjan, Medián, Milčan, Mičian, Fidlán<br>
    {--*robotování*--}: robotovali, robotovati, robot ování, robot-ování, roboto vání, roboto-vání, robotová ní, robotová-ní, robotizovaní, robotizováni, robotova, robotovi, robotová, robotoví, rotovaní, rotováni, rotování, rozbojovaní, rozbojováni, rozbojování, rozbočovaní, rozbočováni, rozbočování, robotovalo, robotovými, obodováni, obětovaní, obětováni, obětování, okótovaní, okótováni, okótování, robotoval, robotovat, probojovaní, probojováni, probojování, proboxovaní, proboxováni, proboxování, prokótovaní, prokótováni, prokótování, dobojovaní, dobojováni, dobojování, dokótovaní, dokótováni, dokótování, dorotovaní, dorotováni, dorotování, robotovala, robotovaly, sabotovaní, sabotováni, sabotování<br>
    {--*Sidónu*--}: Sifonu, Sifonů, Sifónu, Sifónů, Šifonu, Šifonů, Šifónu, Šifónů, Siónu, Siónů, Šídou, Simonu, Simonů, Silonu, Silonů, Šimonu, Šimonů, Šídovu<br>
    {--*Tyru*--}: Týřů, Tyršů, Týřům, Týřův, Tygru, Tygrů, Týři, Turu, Turů, Tůru, Túru, Týř, Třu, Tylu, Tylů, Tyrš, Týnu, Týnů, Týře, Byru, Lyru, Pýru, Pýrů, Sýru, Sýrů, Taru, Toru, Torů, Tyfu, Tyfů, Typu, Typů, Táru, Tóru, Týku, Týků, Týlu, Týlů, Týmu, Týmů, Týrá, Výru, Výrů<br>
    {--*žínici*--}: zimnici, zimnicí, žinčici, žíznící, zničí, znící, Finici, Zinini, sinici, sinicí, vinici, vinicí, viníci, vinící, zrnicí, zrnící, žijící, živici, živicí, živící, žíních, činící<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni-ex-2-1-15a.html](/api/v1/obs/2021-07-13/1-cteni-ex-2-1-15a.html)
    * [mezizpev.html](/api/v1/obs/2021-07-13/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-07-13/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-07-13/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "green")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
