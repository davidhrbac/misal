---
title: '2021-06-17'
---
# Čtvrtek po 11. neděli v mezidobí

## 1. čtení – 2 Kor 11,1-11

*Čtení z druhého listu svatého apoštola Pavla Korinťanům*

(Bratři!) Kéž byste ode mě snesli trochu nerozumu! Ano, sneste to ode mě! Mám vás totiž tak žárlivě rád jako Bůh. Já jsem přece připravil vaše zasnoubení jedinému muži a musím vás představit Kristu jako čistou pannu. Bojím se však, aby se vaše smýšlení nepokazilo, takže byste se uchýlili od upřímné a svaté oddanosti vůči Kristu. Vždyť tak i had svedl Evu svou lstivostí. Přijde k vám někdo a hlásá jiného Ježíše, než jsme hlásali my, přijímáte jiného Ducha, než jste přijali od nás, jiné evangelium, než jste kdysi od nás dostali - a vy to pěkně snesete! Já myslím, že nejsem v ničem pozadu za těmi nadapoštoly. I když v řečnění zběhlý nejsem, o poznání to neplatí. To jsme u vás jasně ukázali při každé příležitosti. Či jsem udělal chybu, když jsem se ponižoval, abyste vy byli povýšeni, že jsem vám hlásal Boží radostnou zvěst zadarmo? Jiné církevní obce jsem obíral a vzal jsem od nich na živobytí, abych mohl sloužit vám. A když jsem byl u vás a měl nedostatek, nikomu jsem nebyl na obtíž. Neboť to, čeho se mi nedostávalo, doplnili bratři, kteří přišli z Makedonie. A vůbec jsem se chránil a budu chránit, abych vám nebyl na obtíž. Dovolávám se Kristovy pravdy, která je ve mně: Nikdo mi v achajských krajích nezabrání, abych se tím chlubil! Proč? Že vás nemám rád? Bůh to ví, (že vás mám rád)!

## Mezizpěv – Žl 111,1-2.3-4.7-8

Skutky tvých rukou, Pane, jsou věrné a spravedlivé.

## Zpěv před evangeliem – Řím 8,15

Aleluja. Dostali jste ducha synovství, a proto můžeme volat: Abba, Otče! Aleluja.

## Evangelium – Mt 6,7-15

*Slova svatého evangelia podle Matouše*

Ježíš řekl svým učedníkům: "Když se modlíte, nebuďte přitom povídaví jako pohané. Ti si totiž myslí, že budou vyslyšeni pro množství slov. Nebuďte tedy jako oni. Vždyť váš Otec ví, co potřebujete, dříve než ho prosíte. Vy se tedy modlete takto: Otče náš, jenž jsi na nebesích, posvěť se jméno tvé. Přijď království tvé. Buď vůle tvá jako v nebi, tak i na zemi. Chléb náš vezdejší dej nám dnes. A odpusť nám naše viny, jako i my odpouštíme našim viníkům. A neuveď nás v pokušení, ale zbav nás od Zlého. Jestliže totiž odpustíte lidem jejich poklesky, odpustí také vám váš nebeský Otec; ale když lidem neodpustíte, ani váš Otec vám neodpustí vaše poklesky."

## Data

### Datové soubory

!!! info ""
    texty + žalm z Misálu

    * JSON - [2021-06-17.json](/api/v1/2021-06-17.json)
    * YAML - [2021-06-17.yaml](/api/v1/2021-06-17.yaml)

    žalm Olejník

    * JSON - [olejnik/2021-06-17.json](/api/v1/olejnik/2021-06-17.json)
    * YAML - [olejnik/2021-06-17.yaml](/api/v1/olejnik/2021-06-17.yaml)

### Pravopis

!!! failure ""
    {--*Abba*--}: Baba, Bába, Abeba, Abbé, Alba<br>
    {--*achajských*--}: Hajských, Zahajských, čabajských, abchazských, macajských, thajských, altajských<br>
    {--*nadapoštoly*--}: nad apoštoly, nad-apoštoly<br>

### Typografie

!!! bug ""
    N/A

### OBS soubory

!!! info ""
    Soubory určené pro použití v rámci OBS.

    * [1-cteni.html](/api/v1/obs/2021-06-17/1-cteni.html)
    * [mezizpev.html](/api/v1/obs/2021-06-17/mezizpev.html)
    * [evangelium.html](/api/v1/obs/2021-06-17/evangelium.html)
    * [olejnik/mezizpev.html](/api/v1/obs/2021-06-17/olejnik/mezizpev.html)


<script>
    window.addEventListener("load", (event) => {
      document.body.setAttribute("data-md-color-primary", "green")
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    });
</script>
