# Historie změn

## Misál - datový zdroj

### 0.3.1 <small>_PRE</small>

- Upraveny hlavičky šablon
- Přidáno site description
- Fixed #2: refactor šablon - používá se jedna hlavní
- OBS soubory přegenerovány z nové master šablony
- V menu pouze 8 dnů zpět
- Starší data přesunuta do Archivu
- Připraven stručný návod na OBS
- OBS refactored - zrušení tečky na konci nadpisu
- OBS šablony obsahují JS zobrazující počet znaků a umožňující zalamování text na podstránky.

### 0.2.1 <small>_22. února 2021</small>

- API: žalmy Olejníka dostupné v YAML
- Návody: API strom přidán
- Misál logicky upraven - přehozeno pořadí 2. čtení a mezizpěvu
- Návody: zapnuto zvýraznění kódu
- Přegenerovány OBS šablony
- Nadpisy žalmů pro OBS obsahují "Žalm" a zkrácené číslo žalmu
- Opraveno ID pro GA

### 0.1.1 <small>_21. února 2021</small>

- Fixed #1: Připraveno první vydání jako 0.1.1
- Prolinkováno na Gitlab repo
- Zapnuty permalinks, všechny nadpisy jsou nyní jako URL odkazy
- Každá stránka zobrazuje datum své poslední aktualizace
- Zapnuty GA na webu
- Připraveny OBS soubory pro začlenění do OBS
