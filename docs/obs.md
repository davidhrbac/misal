Pro potřeby online vysílání mší v naší farnosti používáme co nejvíce online zdrojů. Ke streamování používáme software [OBS Studio](https://obsproject.com/cs). Během vysílání pak zobrazujeme texty z Misálu, Kancionálu a příslušné texty žalmu.

## Misál

Pro jednotlivé části bohoslužby slova připravujeme šablony, které obsahují příslušné texty. Ty se následně v OBS importují jako online zdroje.

Před každým vysíláním pak stačí pouze změnit příslušné datum v API URL.

## Kancionál

Pobodně jako u mešních textů pracujeme s texty písní z Kancionálu. Využíváme web [kancional.cz](https://kancional.cz/) a importujeme píseň jako HTML zdroj. U tohoto zdroje pak pomocí CSS selektorů vypínáme a skrýváme části jako - hlavičku, patičku a jednotlivé sloky písně. Naše interní šablony textů z Misálu mají stejný typografický vzhled, máme tak zaručeno, že texty Kancionálu i Misálu vypadají v OBS stějně.

Před každým vysíláním pak stačí pouze změnit příslušné číslo písně v URL.

## Žalm

Primárně v naší farnosti používáme žalmy p. Olejníka. Textové podklady se mohou rozcházet s uvedenými v Misálu, proto držíme datové struktury a šablony i pro Olejníkovy žalmy.

Před každým vysíláním pak stačí pouze změnit příslušné datum v API URL.
