# Pořad Bílé soboty 2021-04-03

## Slavnost velikonoční svíce

* [01svice.html](01svice.html)
* [02chvalozpev.html](02chvalozpev.html)

## 4x čtení + mezizpěv

* [12-cteni.html](12-cteni.html)
* [12-mezizpev.html](12-mezizpev.html)
* [13-cteni.html](13-cteni.html)
* [13-mezizpev.html](13-mezizpev.html)
* [16-cteni.html](16-cteni.html)
* [16-mezizpev.html](16-mezizpev.html)
* [17-cteni.html](17-cteni.html)
* [17-mezizpev.html](17-mezizpev.html)

## Standardní postup

* [1-cteni.html](1-cteni.html)
* [mezizpev.html](mezizpev.html)
* [evangelium.html](evangelium.html)

## Obnova křestního vyznání

* [21-krest.html](21-krest.html)

## Antifona kropení

* [22-kropeni.html](22-kropeni.html)

## Antifona přijímání

* [23-prijimani.html](23-prijimani.html)
