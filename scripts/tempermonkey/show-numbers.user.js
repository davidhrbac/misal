// ==UserScript==
// @name         Show char numbers
// @namespace    https://gitlab.com/davidhrbac/misal/-/tree/master/scripts/tempermonkey
// @version      0.1.1
// @author       David Hrbáč
// @match        https://misal.hrbac.cz/api/v1/obs/**/*.html
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    String.prototype.replaceAt = function(index, replacement) {
        if (index >= this.length) {
            return this.valueOf();
        }

        return this.substring(0, index) + replacement + this.substring(index + 1);
    }

    const findIndices = (str, char) =>
        str.split('').reduce((indices, letter, index) => {
            letter === char && indices.push(index);
            return indices;
        }, [])

    var mytext = document.querySelector('#content > p');
    var str = mytext.innerHTML;
    var indices = findIndices(str, ".");
    indices = indices.concat(findIndices(str, "!"));
    indices = indices.concat(findIndices(str, "?"));
    indices = new Uint16Array(indices)
    indices.sort();
    // console.log(indices);
    for (var i = indices.length - 1; i >= 0; i--) {
        console.log(indices[i]);
        str = str.replaceAt(indices[i], str[indices[i]] + '<b style="color:Tomato;"><sup>' + (indices[i] + 1) + "</sup></b>");
    }

    mytext.innerHTML = str;

})();
