0.2.1 (2022-02-01)

- API: žalmy Olejníka dostupné v YAML
- Návody: API strom přidán
- Misál logicky upraven - přehozeno pořadí 2. čtení a mezizpěvu
- Návody: zapnuto zvýraznění kódu
- Přegenerovány OBS šablony
- Nadpisy žalmů pro OBS obsahují "Žalm" a zkrácené číslo žalmu
- Opraveno ID pro GA

0.1.1 (2021-02-01)

- Fixed #1: Připraveno první vydání jako 0.1.1
- Prolinkováno na Gitlab repo
- Zapnuty permalinks, všechny nadpisy jsou nyní jako URL odkazy
- Každá stránka zobrazuje datum své poslední aktualizace
- Zapnuty GA na webu
- Připraveny OBS soubory pro začlenění do OBS

