#/bin/bash
OUT="$(mktemp)"
trap 'rm -f -- "$OUT"' EXIT

#echo $OUT

for i in $(ls -r docs/misal/*.md)
do
  echo "    - $(basename ${i%.*}): $i" >> $OUT
done

sed -n '1,/start/p;/end/,$p' mkdocs.yml > mkdocs.yml.bak
sed "/days start/ r ${OUT}" mkdocs.yml.bak > mkdocs.yml
rm -f mkdocs.yml.bak
